import React, { useContext } from 'react';
import ContextProvider from './app/provider/provider'
import firestore from '@react-native-firebase/firestore';
import Estructura_Global from './app/Estructuras/Estructura_Global.json'
import Navigator from './app/navigator/navigation_container'; //Old Navigator
import moment from 'moment';
import {
  getListIngredientsFirebase,
  getListaRecetasFirestore,
} from './app/storage/storage';
import { getUserRef } from './app/storage/firebase';
import { AsyncStorage } from 'react-native';
import { I18nextProvider } from 'react-i18next';
import i18n from './app/lib/i18n';
import { Provider } from 'react-redux';
import store from './app/store'
import { Docs } from './app/local/constant';


export default (_: any) => {

  return (
    <ContextProvider>
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <Navigator />
        </I18nextProvider>
      </Provider>
    </ContextProvider>
  )
}