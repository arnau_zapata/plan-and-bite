const firebase = require("firebase");
// Required for side-effects
require("firebase/functions");

firebase.initializeApp({
    apiKey: 'AIzaSyCBunsFIR3GpT1D1BDX8TprPe3cqNeJ6EA',
    authDomain: 'plan-and-bite.firebaseapp.com',
    projectId: 'plan-and-bite',
    databaseURL: 'https://plan-and-bite.firebaseio.com',
});

// Initialize Cloud Functions through Firebase
var functions = firebase.functions();

const admin = require('firebase-admin');
const app = admin.initializeApp();
const db = admin.firestore(app);

module.exports = { functions, db, admin }
