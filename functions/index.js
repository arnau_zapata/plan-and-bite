const { getUser } = require("./firebase.js");


const functions = require('firebase-functions');

const STRIPE_PUBLISHABLE_KEY = "pk_live_51HgFjQL7t90Ie9qsGe4n2D6LP0h2K9ZnZ8gvdchGjd1zUhos8BaEg3V5FkdOqC16nXNTCFoOoK8XSJimkAfqfwkl00B2ReYLAe"

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.helloWorld = functions.https.onRequest((request, response) => {
    response.send("Hello from Firebase!");
});

exports.payWithStripe = functions.https.onRequest(async (request, response) => {
    try {
        const params = request.params;
        const stripe = require('stripe')(STRIPE_PUBLISHABLE_KEY);
        const charge = await stripe.charges.create({
            amount: params.amount,
            currency: params.currency,
            source: params.token.id,
            receipt_email: params.email
        });

        if (charge.status === 'succeeded' && charge.paid) {
            const email = params.email;
            const currency = "eur";
            const charge = await stripe.charges.create({
                amount: params.amount,
                currency: currency,
                source: params.token.id,
                receipt_email: email
            });
            response.send(charge);
            return
        }
        else {
            response.send({ status: 'failed', type: "fail" });

        }

    } catch (e) {
        console.log(e)
        response.send({ status: 'failed', type: "error", e: e });
    }

});


const doIteration = async (planning, structura_nevera, lista_recetas, numberSelectedPerson, year, mes, dia_init, averageNutricionalValues, user) => {
    let plato_seleccionado = seleccionar_Plato_Aleatorio(planning, year, mes + 1, dia_init);
    console.log("plato_seleccionado", plato_seleccionado)
    let receta_old = plato_seleccionado.receta;
    let old_value = await evaluate_planning(planning, plato_seleccionado, plato_seleccionado.receta, structura_nevera, averageNutricionalValues, user);
    await realizar_cambio(planning, structura_nevera, lista_recetas, plato_seleccionado, numberSelectedPerson);
    let new_value = await evaluate_planning(planning, plato_seleccionado, plato_seleccionado.receta, structura_nevera, averageNutricionalValues, user);
    if (old_value > new_value && receta_old !== -1) {
        revertir_cambio(plato_seleccionado, receta_old, planning, structura_nevera, numberSelectedPerson);
    }
}

// function range(start, end) {
//     return Array(end - start + 1).fill().map((_, idx) => start + idx)
// }

const { seleccionar_Plato_Aleatorio, evaluate_planning, realizar_cambio, revertir_cambio } = require("./functions/planningAutomatico")
exports.planningAutomatico = functions.https.onRequest(async (request, response) => {
    try {
        const params = request.params
        const planning = params.planning;
        const structura_nevera = params.structura_nevera;
        const lista_recetas = params.lista_recetas;
        const numberSelectedPerson = params.numberSelectedPerson;
        const averageNutricionalValues = params.averageNutricionalValues
        const email = params.email;
        const year = params.year;
        const mes = params.mes;
        const dia_init = params.dia_init;
        let user = await getUser(email);
        await Promise.all(
            Array(20).map(async (obj) => {
                await doIteration(planning, structura_nevera, lista_recetas, numberSelectedPerson, year, mes, dia_init, averageNutricionalValues, user)
                return 0
            })
        )
        response.send(planning)
    } catch (e) {
        response.send({ status: "failed", e })
    }
});

