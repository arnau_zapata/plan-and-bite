const { getRecetaFirebase } = require('../firebase.js')

function clone(obj) {
  if (null === obj || "object" !== typeof obj) return obj;
  var copy = obj.constructor();
  for (var attr in obj) {
    if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
  }
  return copy;
}

const isInteger = (variable) => {
  return !isNaN(variable);
}

const get_Receta = async (receta) => {
  if (!isInteger(receta) || !(typeof receta === 'string')) return receta;
  if (receta === "-1") return null;
  return await getRecetaFirebase(receta);
}

const get_Ingredientes_Receta = (receta) => {
  if (receta < 0) return [];
  if (isInteger(receta)) receta = get_Receta(receta);
  if (receta === null) return [];
  return receta.lista_ingredientes;
}

module.exports = {
  clone,
  get_Receta,
  get_Ingredientes_Receta
}
