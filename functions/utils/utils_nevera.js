const { clone, get_Ingredientes_Receta } = require('./utils')


const anadir_cantidad_ingrediente_a_nevera = (nevera, ingrediente, cantidad) => {
  let newIngrediente = nevera.filter((ingrediente_nevera) => (ingrediente_nevera.nombre === ingrediente.nombre))[0];
  if (newIngrediente !== undefined) {
    newIngrediente.cantidad += cantidad;
  } else {
    newIngrediente = clone(ingrediente);
    newIngrediente.cantidad = cantidad;
    nevera.push(newIngrediente)
  }

}

const restar_cantidad_ingrediente_a_nevera = (nevera, ingrediente, cantidad) => {
  let newIngrediente = nevera.filter((ingrediente_nevera) => (ingrediente_nevera.nombre === ingrediente.nombre))[0];
  if (newIngrediente === undefined) return -cantidad;
  newIngrediente.cantidad -= cantidad;

  for (var i = 0; i < nevera.length; i++) {
    if (nevera[i].cantidad <= 0) {
      nevera.splice(i, 1);
      i--;
    }
  }

  return newIngrediente.cantidad;
}

const eliminar_nevera_auxiliar = (nevera, ingrediente, quantity) => {
  quantity = quantity ? quantity : 1
  let nevera_auxiliar = nevera.lista_ingredientes_auxiliar;
  let nevera_compra = nevera.lista_ingredientes_lista_compra;

  let res_cantidad = restar_cantidad_ingrediente_a_nevera(nevera_auxiliar, ingrediente, ingrediente.cantidad * quantity);
  if (res_cantidad < 0) {
    anadir_cantidad_ingrediente_a_nevera(nevera_compra, ingrediente, res_cantidad * -1);
  }
  return ingrediente;
}


const anadir_nevera_auxiliar = (nevera, ingrediente, quantity) => {
  quantity = quantity ? quantity : 1
  let nevera_auxiliar = nevera.lista_ingredientes_auxiliar;
  let nevera_compra = nevera.lista_ingredientes_lista_compra;

  let res_cantidad = restar_cantidad_ingrediente_a_nevera(nevera_compra, ingrediente, ingrediente.cantidad * quantity);
  if (res_cantidad < 0) {
    anadir_cantidad_ingrediente_a_nevera(nevera_auxiliar, ingrediente, res_cantidad * -1);
  }
  return ingrediente;
}

const modificar_nevera_auxiliar = (old_receta, receta, structura_nevera, quantity) => {
  quantity = quantity ? quantity : 1
  let lista_ingredientes_Old_receta = get_Ingredientes_Receta(old_receta);
  let lista_ingredientes_receta = get_Ingredientes_Receta(receta);

  lista_ingredientes_receta.map((ingrediente) => {
    eliminar_nevera_auxiliar(structura_nevera, ingrediente, quantity);
    return ingrediente;
  });
  lista_ingredientes_Old_receta.map((ingrediente) => {
    anadir_nevera_auxiliar(structura_nevera, ingrediente, quantity);
    return ingrediente;
  });
}

const seleccionar_ingrediente = (lista_ingredientes, id_ingrediente) => {
  let selected_ingredient = lista_ingredientes.find((ingrediente) => ingrediente.id_ingrediente === id_ingrediente);
  return selected_ingredient;
}

const cantidad_ingredientes_en_nevera = (lista_ingredientes, nevera_auxiliar) => {
  let cantidad_ingredientes = 0;
  lista_ingredientes.forEach((ingrediente_nevera) => {
    if (seleccionar_ingrediente(nevera_auxiliar, ingrediente_nevera.id_ingrediente) !== undefined) cantidad_ingredientes++;
  });
  return cantidad_ingredientes;
}

module.exports = {
  modificar_nevera_auxiliar,
  cantidad_ingredientes_en_nevera
}