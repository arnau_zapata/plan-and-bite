const { functions, admin, db } = require("./config")


const getRecipeRef = (id) => {
    return db.collection("recetas").doc(id.toString());
}

const getUserRef = (email) => {
    return db.collection("customers").doc(email);
}

const getUser = async (email) => {
    const Ref = getUserRef(email);
    const userDataRef = await Ref.get();
    return userDataRef && userDataRef.data() ? userDataRef.data() : {}
}

const getRecetaFirebase = async (id) => {
    const ref = getRecipeRef(id);
    const dataRef = await ref.get();

}


module.exports = { getUser, getRecetaFirebase }