const moment = require('moment')
const { get_Receta } = require('../utils/utils')
const { modificar_nevera_auxiliar, cantidad_ingredientes_en_nevera } = require('../utils/utils_nevera')



const seleccionar_Plato_Aleatorio = (planning, year, mes, dia_init) => {
    let diferencia_dias_aleatoria = Math.floor(Math.random() * 7);
    let year_seleccionado = moment().year(year).month(mes - 1).date(dia_init).add(diferencia_dias_aleatoria, 'days').year()
    let month = moment().year(year).month(mes - 1).date(dia_init).add(diferencia_dias_aleatoria, 'days').month() + 1;
    let day = moment().year(year).month(mes - 1).date(dia_init).add(diferencia_dias_aleatoria, 'days').date()
    let comida = Math.floor(Math.random() * 3);
    let plato = Math.floor(Math.random() * 3);
    let planning_dia = planning.find((obj) => obj.year === year_seleccionado && obj.mes === month && obj.dia === day)
    let plato_seleccionado = planning_dia.data.find(obj => obj.comida === comida && obj.plato === plato)
    plato_seleccionado = plato_seleccionado ? plato_seleccionado : { year: year_seleccionado, mes: month, dia: day, comida, plato, receta: -1, quantity: 1 }

    return plato_seleccionado
}

const evaluate_planning = async (planning, plato_seleccionado, receta, nevera, valores_nutricionales_usuario, configuration) => {

    let evaluation = 0.0;
    let mes = plato_seleccionado.mes;
    let dia = plato_seleccionado.dia;
    const planning_dia = planning.find((obj) => obj.mes === mes && obj.dia === dia);
    let presupuesto_actual = planning_dia.cost_recipes;
    let valores_nutricionales = planning_dia ? planning_dia.valores_nutricionales : {};
    receta = await get_Receta(receta);
    evaluation += await evaluate_valor_nutricional(valores_nutricionales, valores_nutricionales_usuario);
    evaluation += evaluate_en_nevera(receta, nevera);
    if (configuration.limite_presupuesto) {
        evaluation += await evaluate_presupuesto(presupuesto_actual, configuration);
    }
    return evaluation;

}

const realizar_cambio = async (planning, structura_nevera, lista_recetas, plato_seleccionado, quantity) => {
    if (lista_recetas.length === 0) receta_seleccionada = -1;
    let str_comida = integer_to_comida(plato_seleccionado.comida);
    let aux_lista_recetas = filtrar_comida(str_comida, lista_recetas);
    aux_lista_recetas = filtrar_plato(plato_seleccionado.plato, aux_lista_recetas);

    let random_num = Math.floor(Math.random() * aux_lista_recetas.length)
    let val_aleat = aux_lista_recetas[random_num];
    let receta_seleccionada = -1;
    if (val_aleat !== undefined) {
        receta_seleccionada = val_aleat.id_receta;
    }
    await anadir_receta_a_planning(planning, structura_nevera, receta_seleccionada, plato_seleccionado.year, plato_seleccionado.mes, plato_seleccionado.dia, plato_seleccionado.comida, plato_seleccionado.plato, quantity);

}

const revertir_cambio = (plato_selected, receta_old, planning, structura_nevera, numberSelectedPerson) => {
    anadir_receta_a_planning(planning, structura_nevera, receta_old, plato_selected.year, plato_selected.mes, plato_selected.dia, plato_selected.comida, plato_selected.plato, numberSelectedPerson);
}

const anadir_receta_a_planning = async (planning, structura_nevera, receta, year, mes, dia, comida, plato, quantity) => {
    quantity = quantity ? quantity : 1;
    let planning_dia = planning.find((obj) => obj.year === year && obj.mes === mes && obj.dia === dia)
    let plato_seleccionado = planning_dia.data.find(obj => obj.comida === comida && obj.plato === plato)
    plato_seleccionado = plato_seleccionado ? plato_seleccionado : { year, mes, dia, comida, plato, receta: -1, quantity }
    let old_receta = anadir_receta(plato_seleccionado, receta, structura_nevera, quantity);
    if (planning_dia) {
        planning_dia.data.push(plato_seleccionado)
    } else {
        planning.push({ data: [plato_seleccionado], year, mes, dia })
    }

    await actualizar_valor_nutricional_menu_dia(planning, year, mes, dia, old_receta, receta);
    await actualizar_presupuesto(planning_dia, old_receta, receta);

    return [plato_seleccionado, old_receta];
}

const evaluate_valor_nutricional = async (lista_valores_nutricionales, valores_nutricionales_usuario) => {
    let evaluation = 1.0;
    if (!lista_valores_nutricionales) return evaluation
    Object.entries(valores_nutricionales_usuario).forEach((valor_nutricional_persona) => {
        let valor_nutricional_seleccionado = Object.entries(lista_valores_nutricionales).find((valor_nutricional) => valor_nutricional_persona[0] === valor_nutricional[0]);
        if (valor_nutricional_seleccionado === undefined) evaluation *= 1;
        else if (valor_nutricional_seleccionado[1] > valor_nutricional_persona[1]) evaluation *= parseFloat(valor_nutricional_persona[1]) / valor_nutricional_seleccionado[1];
        else evaluation *= parseFloat(valor_nutricional_seleccionado[1]) / valor_nutricional_persona[1];
    });
    return evaluation;
}

const evaluate_en_nevera = (receta, nevera) => {
    let evaluation = 1.0;
    if (receta !== undefined) {
        let cantidad_ingredientes = cantidad_ingredientes_en_nevera(receta.lista_ingredientes, nevera.lista_ingredientes_auxiliar);
        evaluation *= parseFloat(cantidad_ingredientes) / receta.lista_ingredientes.length;
    }
    return evaluation;
}


const evaluate_presupuesto = async (presupuesto_actual, configuration) => {
    let presupuesto_limite = configuration.presupuesto_establecido;
    let evaluation = presupuesto_actual / presupuesto_limite;
    if (evaluation >= 0.85 && evaluation <= 1.1) {
        return 4 * (0.85 - evaluation);
    }
    else if (evaluation > 1.1) {
        return (1 + (evaluation - 1.1) * (evaluation - 1.1)) * -1;
    }
    else {
        return 0;
    }
}

const anadir_receta = (plato_menu, receta, structura_nevera, quantity) => {
    quantity = quantity ? quantity : 1
    let old_receta = plato_menu && plato_menu.receta ? plato_menu.receta : -1;
    plato_menu.receta = receta;
    modificar_nevera_auxiliar(old_receta, receta, structura_nevera, quantity);
    return old_receta;
}

const filtrar_comida = (comida, lista_recetas) => {
    if (comida === "Desayuno") {
        let ret = lista_recetas.filter((receta) => receta.solo_para_comida === 0 || !receta.solo_para_comida)
        return ret;
    } else {
        let ret = lista_recetas.filter((receta) => receta.solo_para_comida !== 0)
        return ret;
    }
}

const filtrar_plato = (plato, lista_recetas) => {
    plato = plato + 1;
    if (plato === 1) {
        return lista_recetas.filter((receta) => receta.solo_para_plato === 1 || receta.solo_para_plato === 0 || !receta.solo_para_plato);
    } else if (plato === 2) {
        return lista_recetas.filter((receta) => receta.solo_para_plato === 2 || receta.solo_para_plato === 0 || !receta.solo_para_plato);

    } else if (plato === 3) {
        return lista_recetas.filter((receta) => receta.solo_para_plato === 3 || receta.solo_para_plato === 0 || !receta.solo_para_plato);
    }
    return lista_recetas
}

const actualizar_valor_nutricional_menu_dia = async (planning, year, mes, dia, old_receta, new_receta) => {
    new_receta = await get_Receta(new_receta);
    old_receta = await get_Receta(old_receta);

    let menu_dia = planning.find(obj => obj.year === year && obj.dia === dia && obj.mes === mes)
    if (!menu_dia.valores_nutricionales) menu_dia.valores_nutricionales = {}
    if (old_receta !== null && old_receta !== undefined) {
        Object.entries(old_receta.valores_nutricionales).forEach(([key, value]) => {
            let valor_nutricional_seleccionado = Object.entries(menu_dia.valores_nutricionales).find((valor_nutricional_menu_dia) => valor_nutricional_menu_dia[0] === key);
            if (valor_nutricional_seleccionado !== undefined) {
                menu_dia.valores_nutricionales[key] = value > 0
                    ? menu_dia.valores_nutricionales[key] - value
                    : 0;
            }
        });
    }
    if (new_receta !== null && new_receta !== undefined) {
        Object.entries(new_receta.valores_nutricionales).forEach(([key, value]) => {
            let valor_nutricional_seleccionado = Object.entries(menu_dia.valores_nutricionales).find((valor_nutricional_menu_dia) => valor_nutricional_menu_dia[0] === key);
            menu_dia.valores_nutricionales[key] = valor_nutricional_seleccionado
                ? menu_dia.valores_nutricionales[key] + value
                : value;
        });
    }
}

const actualizar_presupuesto = async (planning_dia, old_receta, receta) => {
    old_receta = await get_Receta(old_receta);
    receta = await get_Receta(receta);
    let old_presupuesto_receta;
    let new_presupuesto_receta;
    if (old_receta !== null) {
        old_presupuesto_receta = old_receta.cost;
    } else {
        old_presupuesto_receta = 0;
    }
    if (receta !== null) {
        new_presupuesto_receta = receta.cost;
    } else {
        new_presupuesto_receta = 0;
    }
    planning_dia.cost_recipes += new_presupuesto_receta;
    planning_dia.cost_recipes -= old_presupuesto_receta;
}

module.exports = {
    seleccionar_Plato_Aleatorio,
    evaluate_planning,
    realizar_cambio,
    revertir_cambio,
}