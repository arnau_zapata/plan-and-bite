'use strict';

var React = require('react-native');
import { Platform } from 'react-native';

var {
  StyleSheet,
} = React;

module.exports = StyleSheet.create({



container:{
  flex: 1,
  backgroundColor: '#fff',
  alignItems: 'center',
  justifyContent: 'center',
  textAlign:"center"
},

containerCalendario:{
  flex: 1,
  backgroundColor: "grey",
        //paddingTop: Platform.OS === 'android' ? 26 : 0 Esto solo sirve para dar un pequeño margen gris
},


appColor:{
  // backgroundColor:'#C4FCEF'
  backgroundColor:'white'
},

containerLogin:{
  flex: 1,
  //backgroundColor: "#5e88cc",
  textAlign: 'center',
  alignItems: 'center',
  paddingTop: 40,
  paddingHorizontal: 60,
  height: '100%'
},

containerPerfil:{
  flex: 1,
  //backgroundColor: "#5e88cc",
  textAlign: 'center',
  alignItems: 'center',
  paddingTop: 10,
  paddingHorizontal: 40,
},

containerRegistro:{
  flex: 1,
  paddingTop: 50,
  paddingHorizontal: 60,
},

center:{
  textAlign: 'center',
  alignItems: 'center',
},

basecontainer:{

    width: 100,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
},


//TAMAÑOS DE FUENTE
fuente_XL:{
  fontSize:50,
},
fuente_L:{
  fontSize:30,
},

fuente_M:{
  fontSize:20,
},

fuente_S:{
  fontSize:15,
},

fuente_12:{
  fontSize:12,
},

fuente_XS:{
  fontSize:10,
},



//TAMAÑO DE IMAGEN
img:{
  width: 20,
  height: 20,
  resizeMode: 'contain',
},

img_XS:{
  width: 20,
  height: 20,
  resizeMode: 'contain',
},

img_S:{
  width: 35,
  height: 35,
  resizeMode: 'contain',
},

img_M:{
  width: 50,
  height: 50,
  resizeMode: 'contain',
},

img_L:{
  width: 100,
  height: 100,
  resizeMode: 'contain',
},
img_XL:{
  width: 150,
  height: 150,
  resizeMode: 'contain',
},


form:{
  marginTop: 0,
  alignItems: "center",
  
},
formgroup:{
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  width: 100,
  textAlign: "center",
},

label:{
  fontSize: 20,
},
input: {
  marginTop: 5,
  marginBottom: 10,
  width: 200,
  height: 34,
  textAlign: "center",
  borderRadius: 4,
  borderColor: '#ccc',
  borderWidth: 1,
  fontSize: 16,
},

textReceta:{
  backgroundColor:"white",
  marginBottom: 20,
  // borderWidth:1,
},

textTabulation:{
  marginLeft:20
},


//TAMAÑO DE PADDING

padding_XXS:{
  padding:5,
},
basicPadding:{
  padding:10,
},

padding_M:{
  padding:25,
},
padding_XL:{
  padding:50,
},

vertical_padding_XXS:{
  paddingVertical:5,
},
vertical_basicPadding:{
  paddingVertical:10,
},

vertical_padding_M:{
  paddingVertical:25,
},
vertical_padding_XL:{
  paddingVertical:40,
},


horizontal_padding_XXS:{
  paddingHorizontal:5,
},
horizontal_basicPadding:{
  paddingHorizontal:10,
},

horizontal_padding_M:{
  paddingHorizontal:25,
},
horizontal_padding_XL:{
  paddingHorizontal:40,
},

//TEXT COLOR


text_color_gray:{
  color:'white',
  // color:'#a49d9a',
},

//BORDER RADIUS

border_Radius_S:{
  borderWidth:1,
  borderRadius:10,
},

//BACKGROUND COLOR
background_green:{
  backgroundColor:'green',
},
background_blue:{
  backgroundColor:'#33AAFF',
},
background_yellow:{
  backgroundColor:'yellow',
},
background_white:{
  backgroundColor:'white',
},

background_gray:{
  backgroundColor:'gray',
},
// background_green:{
//   padding:50,
// },
// background_green:{
//   padding:50,
// },


//TINTE

tinte_gray:{
  tintColor: 'gray',
  opacity: 0.8,
},
tinte_blue:{
  tintColor: 'blue',
  opacity: 0.8,
},


//ESPECIFIC
textCalendario:{
  fontSize:23,
  color:"blue",
  fontWeight:"bold",
  borderWidth:1,
  width:45,
},

planning_Calendario:{
  width:45,
  height:100,
  backgroundColor:"green",
  borderColor:"black",
  borderWidth:2,
},

magicIcon:{
  marginTop: 10,
  flexDirection: 'row', 
  alignItems: 'center', 
  borderRadius: 4, 
  borderColor: '#ccc', 
  borderWidth: 1, 
  width: 200, 
  height: 38, 
  alignContent: 'center'


},
perfilInput:{
  borderWidth:1,
  borderColor:'black',
  borderRadius:10,
  width:250,
  padding:5,
  marginBottom:20,
  marginRight:10,
},

magicIcon2LaVenganza:{
  marginTop: 10,
  flexDirection: 'row', 
  alignItems: 'center', 
  borderRadius: 4, 
  borderColor: "#a49d9a", 
  color:"#a49d9a",
  borderBottomWidth: 1, 
  width: 300, 
  height: 38, 
  alignContent: 'center'


},

//OTHER
row:{
  display: "flex",
  flexDirection: "row",
},

content:{
  display: "flex",
  flexDirection: "column",
},

image:{ 
  display: "flex",
  backgroundColor: 'red',
},
center:{
  justifyContent:'center',
  alignItems:'center',
}

});