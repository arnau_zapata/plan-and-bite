import { clone, get_Ingredientes_Receta, solicitar_servidor } from './utils'
import firestore from '@react-native-firebase/firestore';
import { eliminar_ingrediente_planificable_firestore, anadir_ingrediente_planificable_firestore } from '../storage/storage';
import { Ingrediente } from '../type/type';


export const anadir_cantidad_ingrediente_a_nevera = (nevera: any, ingrediente: any, cantidad: any) => {
  let newIngrediente = nevera.filter((ingrediente_nevera: any) => (ingrediente_nevera.nombre == ingrediente.nombre))[0];
  if (newIngrediente != undefined) {
    newIngrediente.cantidad += cantidad;
  } else {
    newIngrediente = clone(ingrediente);
    newIngrediente.cantidad = cantidad;
    nevera.push(newIngrediente)
  }

}

export const restar_cantidad_ingrediente_a_nevera = (nevera: any, ingrediente: any, cantidad: any) => {
  let newIngrediente = nevera.filter((ingrediente_nevera: any) => (ingrediente_nevera.nombre == ingrediente.nombre))[0];
  if (newIngrediente == undefined) return -cantidad;
  newIngrediente.cantidad -= cantidad;

  for (var i = 0; i < nevera.length; i++) {
    if (nevera[i].cantidad <= 0) {
      nevera.splice(i, 1);
      i--;
    }
  }

  return newIngrediente.cantidad;
}

export const eliminar_nevera_auxiliar = (nevera: any, ingrediente: any, quantity: any) => {
  quantity = quantity ? quantity : 1
  let nevera_auxiliar = nevera.lista_ingredientes_auxiliar;
  let nevera_compra = nevera.lista_ingredientes_lista_compra;

  let res_cantidad = restar_cantidad_ingrediente_a_nevera(nevera_auxiliar, ingrediente, ingrediente.cantidad * quantity);
  if (res_cantidad < 0) {
    anadir_cantidad_ingrediente_a_nevera(nevera_compra, ingrediente, res_cantidad * -1);
  }
  return ingrediente;
}


export const anadir_nevera_auxiliar = (nevera: any, ingrediente: any, quantity: any) => {
  quantity = quantity ? quantity : 1
  let nevera_auxiliar = nevera.lista_ingredientes_auxiliar;
  let nevera_compra = nevera.lista_ingredientes_lista_compra;

  let res_cantidad = restar_cantidad_ingrediente_a_nevera(nevera_compra, ingrediente, ingrediente.cantidad * quantity);
  if (res_cantidad < 0) {
    anadir_cantidad_ingrediente_a_nevera(nevera_auxiliar, ingrediente, res_cantidad * -1);
  }
  return ingrediente;
}

export const modificar_nevera_auxiliar = (old_receta: any, receta: any, structura_nevera: any, quantity: any, context: any) => {
  quantity = quantity ? quantity : 1
  let lista_ingredientes_Old_receta = get_Ingredientes_Receta(context, old_receta);
  let lista_ingredientes_receta = get_Ingredientes_Receta(context, receta);

  lista_ingredientes_receta.map((ingrediente: any) => {
    eliminar_nevera_auxiliar(structura_nevera, ingrediente, quantity);
    return ingrediente;
  });
  lista_ingredientes_Old_receta.map((ingrediente: any) => {
    anadir_nevera_auxiliar(structura_nevera, ingrediente, quantity);
    return ingrediente;
  });
}

export const seleccionar_ingrediente = (lista_ingredientes: any, id_ingrediente: any) => {
  let selected_ingredient = lista_ingredientes.find((ingrediente: any) => ingrediente.id_ingrediente == id_ingrediente);
  return selected_ingredient;
}

export const cantidad_ingredientes_en_nevera = (lista_ingredientes: any, nevera_auxiliar: any) => {
  let cantidad_ingredientes = 0;
  lista_ingredientes.map((ingrediente_nevera: any) => {
    if (seleccionar_ingrediente(nevera_auxiliar, ingrediente_nevera.id_ingrediente) != undefined) cantidad_ingredientes++;
  });
  return cantidad_ingredientes;
}

export const actualizar_ingredientes_planificables_firebase = async (old_receta: string | number, receta: string | number, context: any) => {
  if (old_receta === receta) {
    return
  }
  let lista_ingredientes_Old_receta = get_Ingredientes_Receta(context, old_receta);
  let lista_ingredientes_receta = get_Ingredientes_Receta(context, receta);

  await Promise.all(lista_ingredientes_receta.map(async (ingrediente: Ingrediente) => {
    await eliminar_ingrediente_planificable_firestore(ingrediente);
  }));
  await Promise.all(lista_ingredientes_Old_receta.map(async (ingrediente: Ingrediente) => {
    await anadir_ingrediente_planificable_firestore(ingrediente);
  }));

}

export const filtrar_nevera = (lista_recetas: any, nevera: any) => {
  let ret = lista_recetas.filter((receta: any) => {
    let lista_ingredientes = receta.lista_ingredientes;
    let lista_verificacion = lista_ingredientes.map((ingrediente: any) => {
      let ingrediente_seleccionado = nevera.filter((ingrediente_nevera: any) => ingrediente_nevera.nombre == ingrediente.nombre)[0];
      return ingrediente_seleccionado != undefined;
    });
    let all_en_nevera = true;
    lista_verificacion.map((obj_verificacion: any) => {
      if (obj_verificacion == false) all_en_nevera = false;
    });
    return all_en_nevera;
  });
  return ret;
}