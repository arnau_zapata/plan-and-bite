import moment from 'moment'
import axios from 'axios'
import { Context, } from '../type/type';
import { get_Ingrediente } from '../storage/storage';
import i18n from 'i18next';


export const solicitar_servidor = (solicitud: any, after_function: any) => {
  let finished = false;
  let ret;

  let query = 'http://planandbite.u3vrutvqsh.us-east-1.elasticbeanstalk.com/api/v1/' + solicitud;
  //let query='https://plan-n-bite-native.herokuapp.com/'+solicitud;
  fetch(query)
    .then((response) => response.json())
    .then((data) => {
      after_function(data);
      finished = true;
      return data;
    })
    .catch(function (error) {
      finished = true;

    });
}

export function clone(obj: any) {
  if (null == obj || "object" != typeof obj) return obj;
  var copy = obj.constructor();
  for (var attr in obj) {
    if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
  }
  return copy;
}

export const isInteger = (variable: any) => {
  return !isNaN(variable);
}

export const get_Receta = (context: Context, receta: any) => {
  if (!isInteger(receta) && typeof receta != typeof "") return receta;
  if (receta < 0) return null;
  return context.lista_recetas.find((obj: any) => obj.id_receta == receta);

}

export const flattenList =(list:any[])=>{
  return list.reduce((accum,obj)=>accum.concat(obj),[])
}

export const get_Ingredientes_Receta = (context: Context, receta: any) => {
  if (receta < 0) return [];
  if (isInteger(receta)) receta = get_Receta(context, receta);
  if (typeof receta == typeof "") receta = get_Receta(context, receta);
  if (receta == null) return [];
  return receta.lista_ingredientes;
}

export const objeto_aleatorio_array = (lista_recetas: any) => {
  if (lista_recetas.length == 0) return { id: -1 };
  var obj = lista_recetas[Math.floor(Math.random() * lista_recetas.length)];
  return obj;
}


export function getPosition_Estructure_in_Array(a: any, obj: any) {
  for (var i = 0; i < a.length; i++) {
    if (a[i].nombre === obj.nombre) {
      return i;
    }
  }
  return -1;
}

export function remove_all_objects_from_array(arr: any) {
  for (var i = 0; i < arr.length; i++) {
    arr.splice(i, 1);
    i--;
  }
}

export function weekOfMonth(year: any, mes: any, dia: any) {
  const aux = year + "-" + mes + "-" + dia;
  const input = moment(aux, "YYYY-MM-DD");
  const firstDayOfMonth = input.clone().startOf('month');
  const firstDayOfWeek = firstDayOfMonth.clone().startOf('week');

  const offset = firstDayOfMonth.diff(firstDayOfWeek, 'days');
  const ret = Math.ceil((input.date() + offset) / 7);
  return ret;
}

export function actualizar_valores_nutricionales_necesarios_persona(persona: any) {
  persona.valores_nutricionales = [];
  if (persona.sexo == "Hombre") {
    let valor_nutricional = { azucar: 10, fibra: 10, hidratos_carbono: 10, kcal: 2000, proteinas: 10 };

    persona.valores_nutricionales = valor_nutricional

  } else {
    let valor_nutricional = { azucar: 10, fibra: 10, hidratos_carbono: 10, kcal: 1500, proteinas: 10 };

    persona.valores_nutricionales = valor_nutricional;
  }
}

export function restarValoresNutricionales(valores_nutricionales: any, valoresNutricionalesResta: any, num: any) {
  if (num == undefined || num == 0) num = 1;
  Object.entries(valoresNutricionalesResta).map(([key, value]: any) => {
    const preCalculValoresNutricionales = valores_nutricionales[key] >= 0 ? valores_nutricionales[key] : 0;

    const preCalculNum = num - 1 > 0 ? num - 1 : 1;
    valores_nutricionales[key] = (preCalculValoresNutricionales * num - value) / preCalculNum;
  })
  return valores_nutricionales
}

export function sumarValoresNutricionales(valores_nutricionales: any, valoresNutricionalesSuma: any, num: any) {
  if (num == undefined || num == 0) num = 0;
  Object.entries(valoresNutricionalesSuma).map((valor_nutricional: any) => {
    let key = valor_nutricional[0];
    let value: number = valor_nutricional[1];
    const preCalculValoresNutricionales = valores_nutricionales[key] >= 0 ? valores_nutricionales[key] : 0;
    valores_nutricionales[key] = (preCalculValoresNutricionales * num + value) / (num + 1);
  })
  return valores_nutricionales
}

export const filterPlanningByDay = (planning: any, year: number, mes: number, dia: number) => {
  return planning.filter((obj: any) => obj.year === year && obj.mes === mes && obj.dia === dia)
}


export const findPlanningByPlato = (planning: any, year: number, mes: number, dia: number, comida: number, plato: number) => {
  return planning.find((obj: any) => obj.year === year && obj.mes === mes && obj.dia === dia && obj.comida === comida && obj.plato === plato)
}

export const apiCallGet = async (type: any, body: any) => {
  try {

    const data = await axios.get(`https://us-central1-plan-and-bite.cloudfunctions.net/${type}`, {
      params: body,
      headers: {
        'Content-Type': 'application/json',
      }
    })
    let ret = data.data;
    console.log(`${ret} API DATA`)
    return ret;
  } catch (e) {
    console.log(`${type} APICALL GET ERROR:`, e)
  }
}

export const apiCallDelete = async (type: any, body: any) => {
  try {
    console.log(type, "DELETE body:", body)
    console.log(type, "DELETE ulr:", `https://us-central1-plan-and-bite.cloudfunctions.net/${type}`)
    const data = await axios.delete(`https://us-central1-plan-and-bite.cloudfunctions.net/${type}`, {

      headers: {
        'Content-Type': 'application/json',
      },
      data: body
    })
    console.log(`${type} Delete DATA`, body)
  } catch (e) {
    console.log(`${type} APICALL Delete ERROR:`, e)
  }
}



export const apiCallPost = async (type: any, body: any) => {
  try {

    console.log(type, "POST body:", body)
    console.log(type, "POST ulr:", `https://us-central1-plan-and-bite.cloudfunctions.net/${type}`)
    const data = await axios.post(`https://us-central1-plan-and-bite.cloudfunctions.net/${type}`, body, {

      headers: {
        'Content-Type': 'application/json',
      }
    })
    console.log(`${type} post DATA`, body)
  } catch (e) {
    console.log(`${type} APICALL Post ERROR:`, e)
  }

}

export const apiCallPut = async (type: any, body: any) => {
  try {

    console.log(type, "PUT body:", body)
    console.log(type, "PUT ulr:", `https://us-central1-plan-and-bite.cloudfunctions.net/${type}`)
    const data = await axios.put(`https://us-central1-plan-and-bite.cloudfunctions.net/${type}`, body, {

      headers: {
        'Content-Type': 'application/json',
      }
    })
    console.log(`${type} Put DATA`, body)
  } catch (e) {
    console.log(`${type} APICALL Put ERROR:`, e)
  }
}



export const emptyDict = (dict: any) => {
  return Object.entries(dict).length == 0
}

const doMergeFunc = (mergeFunc: ((x: any) => any) | undefined, value: any) => {
  return mergeFunc ? mergeFunc(value) : value
}

export const mergeDict = (originalMap: any, otherMap: any, validateFunc: (x: any) => boolean, mergeFunc?: (x: any) => any) => {
  Object.entries(otherMap).forEach(([key, value]: any) => {
    if (Array.isArray(value)) {
      originalMap[key] = originalMap[key] ? originalMap[key] : []
      value.forEach((obj) => {
        const indexOriginalMap: number = originalMap[key].findIndex((obj2: any) => mergeFunc ? (doMergeFunc(mergeFunc, obj) == obj2) : obj == obj2);
        if (indexOriginalMap >= 0 && validateFunc(obj)) {
          originalMap[key][indexOriginalMap] = doMergeFunc(mergeFunc, obj)
        }
        else if (validateFunc(obj)) {
          originalMap[key].push(doMergeFunc(mergeFunc, obj))
        }
      })
    }
    else if (typeof value == typeof {} && (doMergeFunc(mergeFunc, value) == undefined || mergeFunc == undefined)) {
      originalMap[key] = originalMap[key] ? originalMap[key] : {}
      mergeDict(originalMap[key], value, validateFunc, mergeFunc)
      if (emptyDict(originalMap[key])) delete originalMap[key];
    }
    else if (validateFunc(value)) originalMap[key] = doMergeFunc(mergeFunc, value);
  })
}

export const unmergeDict = (originalMap: any, otherMap: any, validateFunc: (x: any) => boolean) => {
  console.log("unmergeDict", originalMap)
  Object.entries(otherMap).forEach(([key, value]: any) => {
    if (Array.isArray(value)) {
      value.forEach((obj) => {
        if (originalMap[key]) {
          const indexOriginalMap: number = originalMap[key].findIndex((obj2: any) => obj2 && (obj.data == obj2.data || obj2 == obj || obj2 == obj.data));
          if (indexOriginalMap >= 0 && validateFunc(obj)) {
            delete originalMap[key][indexOriginalMap];
          }
        }
      })
      if (originalMap[key]) {
        originalMap[key] = originalMap[key].filter((obj: any) => obj != undefined)
        if (originalMap[key].length == 0) delete originalMap[key];
      }
    }
    else if (!validateFunc(value) && typeof value == typeof {} && typeof originalMap[key] == typeof {}) {
      originalMap[key] = originalMap[key] ? originalMap[key] : {}
      unmergeDict(originalMap[key], value, validateFunc)
      if (emptyDict(originalMap[key])) delete originalMap[key];
    }
    else if (validateFunc(value)) delete originalMap[key];
  })
  console.log("originalMap", originalMap)
}

export const reduceDict = (originalMap: any, extractReduceFunc: (keyValue: any[]) => any[], accumFunc: (x: any, y: any) => any) => {
  const newDict: any = {}
  // console.log("reduceDict", originalMap)
  Object.entries(originalMap).forEach(([key, value]: any) => {
    const [newkey, newValue] = extractReduceFunc([key, value])
    if (newkey && newValue) newDict[newkey] = accumFunc(newDict[newkey], newValue) || newValue;
    else if (Array.isArray(value)) {
      value.forEach((aux: any) => {
        const [newKey, newValue] = extractReduceFunc(["", aux]);
        newDict[newKey] = accumFunc(newDict[newKey], newValue) || newValue
      })
    }
    else if (typeof value == typeof {}) {
      const aux = reduceDict(value, extractReduceFunc, accumFunc);
      Object.entries(aux).forEach(([newKey, newValue]: any) => {
        newDict[newKey] = accumFunc(newDict[newKey], newValue) || newValue
      })
    }
  })
  // console.log("newDict", newDict)
  return newDict
}

export const getValoresNutricionalesListaIngredientes = async (lista_ingredientes: any) => {
  const ret: any = {}
  await Promise.all(Object.entries(lista_ingredientes).map(async ([key, value]: any) => {
    const ingrediente: any = await get_Ingrediente(key);
    const cantidad = value ? value : 1;
    const valores_nutricionales = ingrediente.valores_nutricionales ? ingrediente.valores_nutricionales : {}
    !emptyDict(valores_nutricionales) && Object.entries(valores_nutricionales).forEach(([key, value]: any) => {
      ret[key] = (ret[key] ? ret[key] : 0) + (value * cantidad)
    })
  }));
  return ret;
}

export const getLanguage = () => {
  let languaje = i18n.language ||
    (typeof window !== 'undefined' && window.localStorage.i18nextLng) ||
    'es';
  languaje = languaje.split('-')[0]
  return languaje
};

export const translate = (untransatedVariable: any) => {
  const language = getLanguage();
  if (untransatedVariable == undefined) return undefined
  return untransatedVariable[language] ? untransatedVariable[language] : untransatedVariable;
}