
import moment from 'moment'

export const int_to_mes = (mes: any) => {
  if (mes == 0) return 'Ene';
  if (mes == 1) return 'Ene';
  if (mes == 2) return 'Feb';
  if (mes == 3) return 'Mar';
  if (mes == 4) return 'Abr';
  if (mes == 5) return 'May';
  if (mes == 6) return 'Jun';
  if (mes == 7) return 'Jul';
  if (mes == 8) return 'Ago';
  if (mes == 9) return 'Seb';
  if (mes == 10) return 'Oct';
  if (mes == 11) return 'Nov';
  if (mes == 12) return 'Dic';
}

export const pre_modificar_dias_calendario2 = (mes: any, dia_seleccionado: any, diferencia_dias: any) => {
  let new_dia = dia_seleccionado + diferencia_dias;
  let dias_mes_actual = moment().daysInMonth();
  let dias_mes_anterior = moment().subtract(1, 'month').daysInMonth();

  if (new_dia > dias_mes_actual) {
    new_dia = new_dia - dias_mes_actual;
    mes = mes + 1;
  }
  else if (new_dia < 1) {
    new_dia = dias_mes_anterior + new_dia;
    mes = mes - 1;
  }
  return [mes, new_dia]
}

export const int_to_dia_semana = (dia_semana_actual: any) => {
  if (dia_semana_actual == 0) return 'Lun';
  if (dia_semana_actual == 1) return 'Mar';
  if (dia_semana_actual == 2) return 'Mier';
  if (dia_semana_actual == 3) return 'Jue';
  if (dia_semana_actual == 4) return 'Vie';
  if (dia_semana_actual == 5) return 'Sab';
  if (dia_semana_actual == 6) return 'Dom';
}

export const get_Dia_Lunes = (mes: any, dia: any, dia_semana: any) => {
  let diferencia_dias = -dia_semana;
  return pre_modificar_dias_calendario2(mes, dia, diferencia_dias);
}

export const integer_to_comida = (comida: any) => {
  if (comida == 0) return 'Desayuno';
  if (comida == 1) return 'Comida';
  if (comida == 2) return 'Cena';
  if (comida == 3) return 'medienda';
}

export const comida_to_integer = (comida: any) => {
  if (comida == 'Desayuno') return 0;
  if (comida == 'Comida') return 1;
  if (comida == 'Cena') return 2;
  if (comida == 'medienda') return 3;
  return comida
}




