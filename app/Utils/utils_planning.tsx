
import Estructura_Global from '../Estructuras/Estructura_Global'
import { get_Receta, apiCallGet } from './utils'
import { modificar_nevera_auxiliar, cantidad_ingredientes_en_nevera, filtrar_nevera } from './utils_nevera'
import { integer_to_comida } from './utils_calendario'
import {
  actualizar_plato_firestore,
  getPersonSelectedValues,
  actualizarPlanningDia,
  setPendingPlanningFirebase,
  getConfiguration,
} from '../storage/storage'
import moment from 'moment'
import { Context, Plato, Receta } from '../type/type';

export const anadir_receta = (plato_menu: any, receta: any, structura_nevera: any, quantity: any, context: any) => {
  quantity = quantity ? quantity : 1
  let old_receta = plato_menu && plato_menu.receta ? plato_menu.receta : -1;
  plato_menu.receta = receta;
  modificar_nevera_auxiliar(old_receta, receta, structura_nevera, quantity, context);
  return old_receta;
}

const actualizar_valor_nutricional_menu_dia = (planning: any, year: any, mes: any, dia: any, old_receta: any, new_receta: any, context: any) => {
  new_receta = get_Receta(context, new_receta);
  old_receta = get_Receta(context, old_receta);

  let menu_dia = planning.find((obj: any) => obj.year == year && obj.dia == dia && obj.mes == mes)
  if (!menu_dia.valores_nutricionales) menu_dia.valores_nutricionales = {}
  if (old_receta != null && old_receta != undefined) {
    Object.entries(old_receta.valores_nutricionales).map(([key, value]: any) => {
      let valor_nutricional_seleccionado = Object.entries(menu_dia.valores_nutricionales).find((valor_nutricional_menu_dia) => valor_nutricional_menu_dia[0] == key);
      if (valor_nutricional_seleccionado != undefined) {
        menu_dia.valores_nutricionales[key] = value > 0
          ? menu_dia.valores_nutricionales[key] - value
          : 0;
      }
    });
  }
  if (new_receta != null && new_receta != undefined) {
    Object.entries(new_receta.valores_nutricionales).map(([key, value]) => {
      let valor_nutricional_seleccionado = Object.entries(menu_dia.valores_nutricionales).find((valor_nutricional_menu_dia) => valor_nutricional_menu_dia[0] == key);
      menu_dia.valores_nutricionales[key] = valor_nutricional_seleccionado
        ? menu_dia.valores_nutricionales[key] + value
        : value;
    });
  }
}


export const anadir_receta_a_planning = (planning: any, structura_nevera: any, receta: any, year: any, mes: any, dia: any, comida: any, plato: any, quantity: any, context: any) => {
  quantity = quantity ? quantity : 1;
  let planning_dia = planning.find((obj: any) => obj.year == year && obj.mes == mes && obj.dia == dia)
  let plato_seleccionado = planning_dia.data.find((obj: any) => obj.comida == comida && obj.plato == plato)
  plato_seleccionado = plato_seleccionado ? plato_seleccionado : { year, mes, dia, comida, plato, receta: -1, quantity }
  let old_receta = anadir_receta(plato_seleccionado, receta, structura_nevera, quantity, context);
  if (planning_dia) {
    planning_dia.data.push(plato_seleccionado)
  } else {
    planning.push({ data: [plato_seleccionado], year, mes, dia })
  }

  actualizar_valor_nutricional_menu_dia(planning, year, mes, dia, old_receta, receta, context);
  actualizar_presupuesto(planning_dia, old_receta, receta, context);

  return [plato_seleccionado, old_receta];
}

const actualizar_presupuesto = (planning_dia: any, old_receta: any, receta: any, context: any) => {
  old_receta = get_Receta(context, old_receta);
  receta = get_Receta(context, receta);
  let old_presupuesto_receta;
  let new_presupuesto_receta;
  if (old_receta != null) {
    old_presupuesto_receta = old_receta.cost;
  } else {
    old_presupuesto_receta = 0;
  }
  if (receta != null) {
    new_presupuesto_receta = receta.cost;
  } else {
    new_presupuesto_receta = 0;
  }
  planning_dia.cost_recipes += new_presupuesto_receta;
  planning_dia.cost_recipes -= old_presupuesto_receta;
}


export const menu_to_lista_Recetas = (menu: any, context: any) => {
  if (menu == undefined) return undefined;
  let lista_recetas = menu.lista_platos.filter((plato: any) => plato.receta >= 0);
  if (lista_recetas == undefined) return [];
  return lista_recetas.map((plato: any) => {
    return get_Receta(context, plato.receta);
  });
}

export const generarFiltros = async (alergiPersons: any, notEatPersons: any, context: Context) => {
  try {

    let configuracion = await getConfiguration();
    let lista_filtros = [];
    let lista_ingredientes_en_nevera = context.lista_ingredientes_nevera
    if (configuracion.solo_ingredientes_de_la_nevera) {
      lista_filtros.push((lista_recetas: any) => filtrar_nevera(lista_recetas, lista_ingredientes_en_nevera))
    }
    if (configuracion.limite_tiempo) {
      let limite_establecido = configuracion.tiempo_establecido;
      lista_filtros.push((lista_recetas: any) => {
        let ret = lista_recetas.filter((receta: any) => receta.tiempo_preparacion < limite_establecido || !receta.tiempo_preparacion);
        return ret;
      });
    }
    let notEatList = notEatPersons ? notEatPersons : [];
    if (notEatList.length > 0) {
      lista_filtros.push((lista_recetas: any) => {
        notEatList.map((notEat: any) => {
          lista_recetas = lista_recetas.filter((receta: any) => {
            let lista_ingredientes = receta.lista_ingredientes;
            return lista_ingredientes.filter((ingrediente: any) => ingrediente.tipo_ingrediente.includes(notEat)).length == 0;
          });
        });
        return lista_recetas;
      });
    }
    let alergiList = alergiPersons ? alergiPersons : [];
    if (alergiList.length > 0) {

      lista_filtros.push((lista_recetas: any) => {
        alergiList.map((alergia: any) => {
          lista_recetas = lista_recetas.filter((receta: any) => {
            let lista_ingredientes = receta.lista_ingredientes;
            return lista_ingredientes.filter((ingrediente: any) => ingrediente.nombre == alergia || ingrediente.tipo_ingrediente.includes(alergia)).length == 0;
          });
        });
        return lista_recetas;
      });
    }
    return lista_filtros;
  } catch (e) {
    console.log("error", e);
    return []
  }
}

export const usarFiltros = async (lista_recetas: any, context: any, alergiPersons?: any, notEatPersons?: any) => {
  console.log("usarFiltros")
  const lista_filtros = await generarFiltros(alergiPersons, notEatPersons, context);
  const result = lista_filtros.reduce((lista: any, filtro: any) => filtro(lista), lista_recetas);
  return result;
}


export const get_Num_plato_vacio = (lista_recetas: any) => {
  if (!lista_recetas.find((obj: any) => obj.plato == 0)) return 0;
  if (!lista_recetas.find((obj: any) => obj.plato == 1)) return 1;
  if (!lista_recetas.find((obj: any) => obj.plato == 2)) return 2;
  return -1;
}

const filtrar_plato = (plato: any, lista_recetas: any) => {
  plato = plato + 1;
  if (plato == 1) {
    return lista_recetas.filter((receta: any) => receta.solo_para_plato == 1 || receta.solo_para_plato == 0 || !receta.solo_para_plato);
  } else if (plato == 2) {
    return lista_recetas.filter((receta: any) => receta.solo_para_plato == 2 || receta.solo_para_plato == 0 || !receta.solo_para_plato);

  } else if (plato == 3) {
    return lista_recetas.filter((receta: any) => receta.solo_para_plato == 3 || receta.solo_para_plato == 0 || !receta.solo_para_plato);
  }
  return lista_recetas
}

export const filtrar_comida = (comida: any, lista_recetas: any) => {
  if (comida == "Desayuno") {
    let ret = lista_recetas.filter((receta: any) => receta.solo_para_comida == 0 || !receta.solo_para_comida)
    return ret;
  } else {
    let ret = lista_recetas.filter((receta: any) => receta.solo_para_comida != 0)
    return ret;
  }
  return lista_recetas;
}

const seleccionar_Plato_Aleatorio = (planning: any, year: any, mes: any, dia_init: any) => {
  let diferencia_dias_aleatoria = Math.floor(Math.random() * 7);
  let year_seleccionado = moment().year(year).month(mes - 1).date(dia_init).add(diferencia_dias_aleatoria, 'days').year()
  let month = moment().year(year).month(mes - 1).date(dia_init).add(diferencia_dias_aleatoria, 'days').month() + 1;
  let day = moment().year(year).month(mes - 1).date(dia_init).add(diferencia_dias_aleatoria, 'days').date()
  let comida = Math.floor(Math.random() * 3);
  let plato = Math.floor(Math.random() * 3);
  let planning_dia = planning.find((obj: any) => obj.year == year_seleccionado && obj.mes == month && obj.dia == day)
  let plato_seleccionado = planning_dia.data.find((obj: any) => obj.comida == comida && obj.plato == plato)
  plato_seleccionado = plato_seleccionado ? plato_seleccionado : { year: year_seleccionado, mes: month, dia: day, comida, plato, receta: -1, quantity: 1 }

  return plato_seleccionado
}

const realizar_cambio = (planning: any, structura_nevera: any, lista_recetas: any, plato_seleccionado: any, quantity: any, context: any) => {
  let receta_seleccionada = -1;
  if (lista_recetas.length == 0) receta_seleccionada = -1;
  let str_comida = integer_to_comida(plato_seleccionado.comida);
  let aux_lista_recetas = filtrar_comida(str_comida, lista_recetas);
  aux_lista_recetas = filtrar_plato(plato_seleccionado.plato, aux_lista_recetas);

  let random_num = Math.floor(Math.random() * aux_lista_recetas.length)
  let val_aleat = aux_lista_recetas[random_num];
  if (val_aleat != undefined) {
    receta_seleccionada = val_aleat.id_receta;
  }
  anadir_receta_a_planning(planning, structura_nevera, receta_seleccionada, plato_seleccionado.year, plato_seleccionado.mes, plato_seleccionado.dia, plato_seleccionado.comida, plato_seleccionado.plato, quantity, context);

}

const revertir_cambio = (plato_selected: any, receta_old: any, planning: any, structura_nevera: any, numberSelectedPerson: any, context: any) => {
  anadir_receta_a_planning(planning, structura_nevera, receta_old, plato_selected.year, plato_selected.mes, plato_selected.dia, plato_selected.comida, plato_selected.plato, numberSelectedPerson, context);
}


const evaluate_en_nevera = (receta: any, nevera: any) => {
  let evaluation = 1.0;
  if (receta != undefined) {
    let cantidad_ingredientes: any = cantidad_ingredientes_en_nevera(receta.lista_ingredientes, nevera.lista_ingredientes_auxiliar);
    evaluation *= parseFloat(cantidad_ingredientes) / receta.lista_ingredientes.length;
  }
  return evaluation;
}

const evaluate_valor_nutricional = async (lista_valores_nutricionales: any, valores_nutricionales_usuario: any) => {
  let evaluation = 1.0;
  if (!lista_valores_nutricionales) return evaluation
  Object.entries(valores_nutricionales_usuario).map((valor_nutricional_persona: any) => {
    let valor_nutricional_seleccionado: any = Object.entries(lista_valores_nutricionales).find((valor_nutricional) => valor_nutricional_persona[0] == valor_nutricional[0]);
    if (valor_nutricional_seleccionado == undefined) evaluation *= 1;
    else if (valor_nutricional_seleccionado[1] > valor_nutricional_persona[1]) evaluation *= parseFloat(valor_nutricional_persona[1]) / valor_nutricional_seleccionado[1];
    else evaluation *= parseFloat(valor_nutricional_seleccionado[1]) / valor_nutricional_persona[1];
  });

  return evaluation;
}




export const restar_valor_nutricional = async (receta: any, valores_nutricionales: any) => {
  if (!receta) return valores_nutricionales
  if (receta.valores_nutricionales) {
    Object.entries(receta.valores_nutricionales).map(([key, value]: any) => {

      const valueValoresNutricionales = (valores_nutricionales[key] > 0 ? valores_nutricionales[key] : 0)
      value = (value > 0 ? value : 0)
      const result = valueValoresNutricionales - value
      valores_nutricionales[key] = result > 0 ? result : 0;
    })

    return valores_nutricionales
  }
}

export const anadir_valor_nutricional = async (receta: any, valores_nutricionales: any) => {
  if (!receta) return valores_nutricionales
  if (receta.valores_nutricionales) {
    Object.entries(receta.valores_nutricionales).map((valor_nutricional) => {
      let key = valor_nutricional[0];
      let value = valor_nutricional[1];
      valores_nutricionales[key] = value + (valores_nutricionales[key] ? valores_nutricionales[key] : 0)
    })
    return valores_nutricionales
  }
  return valores_nutricionales
}



export const anadir_receta_a_firestore = async (receta: any, year: any, mes: any, dia: any, comida: any, plato: any, context: Context, quantity?: number) => {
  const oldReceipt: Plato = context.lista_planning.filter((obj) => obj.year === year && obj.mes === mes && obj.dia === dia && obj.comida === comida && obj.plato === plato)[0]
  quantity = quantity ? quantity : 1
  const newReceipt: Plato = {
    receta,
    year,
    mes,
    dia,
    comida,
    plato,
    quantity,
  }
  console.log("newReceipt", newReceipt)
  await actualizar_plato_firestore(oldReceipt, newReceipt, context, true)
  return oldReceipt ? oldReceipt.receta : -1;

}
export const eliminar_receta_a_firestore = async (year: any, mes: any, dia: any, comida: any, plato: any, context: Context) => {
  console.log("context.lista_planning", context.lista_planning)
  const oldReceipt: Plato = context.lista_planning.filter((obj) => obj.year === year && obj.mes === mes && obj.dia === dia && obj.comida === comida && obj.plato === plato)[0]
  await actualizar_plato_firestore(oldReceipt, undefined, context, true)
  return oldReceipt ? oldReceipt.receta : -1;
}
const evaluate_planning = async (planning: any, plato_seleccionado: any, receta: any, nevera: any, valores_nutricionales_usuario: any, context: any) => {
  let configuracion = await getConfiguration();
  let evaluation = 0.0;
  let mes = plato_seleccionado.mes;
  let dia = plato_seleccionado.dia;
  const planning_dia = planning.find((obj: any) => obj.mes == mes && obj.dia == dia);
  let presupuesto_actual = planning_dia.cost_recipes;
  let valores_nutricionales = planning_dia ? planning_dia.valores_nutricionales : {};
  receta = get_Receta(context, receta);
  evaluation += await evaluate_valor_nutricional(valores_nutricionales, valores_nutricionales_usuario);
  evaluation += evaluate_en_nevera(receta, nevera);
  if (configuracion.limite_presupuesto) {
    evaluation += await evaluate_presupuesto(presupuesto_actual);
  }
  return evaluation;

}

const evaluate_presupuesto = async (presupuesto_actual: any) => {
  let configuracion = await getConfiguration();
  let presupuesto_limite = configuracion.presupuesto_establecido;
  let evaluation = presupuesto_actual / presupuesto_limite;
  if (evaluation >= 0.85 && evaluation <= 1.1) {
    return 4 * (0.85 - evaluation);
  }
  else if (evaluation > 1.1) {
    return (1 + (evaluation - 1.1) * (evaluation - 1.1)) * -1;
  }
  else {
    return 0;
  }
  return 1 / evaluation;
}


const uploadPlanningToFirestore = async (planning: any, numberSelectedPerson: any, diaInit: any, diaEnd: any, context: any) => {
  const flateredPlanning = planning.reduce((accum: any, obj: any) => accum.concat((obj && obj.data) ? obj.data : []), [])
  if (flateredPlanning.length > 0) {
    await actualizarPlanningDia(flateredPlanning, numberSelectedPerson, diaInit, diaEnd, context)
  }
}


const ajustarKcal = async (nutricionalValues: any, user: any) => {
  if (user.peso_ideal == undefined || (user.peso_ideal > user.peso - 1 && user.peso_ideal < user.peso + 1)) { }
  else if (user.peso_ideal > user.peso) {
    nutricionalValues['kcal'] += 100;
  }
  else {
    nutricionalValues['kcal'] -= 100;
  }

}

const rellenar_con_IA = async (lista_recetas: any, year: any, mes: any, dia_init: any, featureVariasPersonas: any, context: Context) => {

  // calcular_presupuesto(mes, dia_init);
  // rellenar_aleatoriamente(lista_recetas,mes,dia_init);
  let planning = [];

  const diaInit = moment().year(year).month(mes).date(dia_init);
  const mom = moment().year(year).month(mes).date(dia_init);
  for (let i = 0; i < 7; i++) {
    let nyear = mom.year();
    let month = mom.month() + 1
    let day = mom.date();
    const menuDia = context.lista_planning.filter((obj) => obj.year === mom.year() && obj.mes === mom.month() + 1 && obj.dia === mom.date())
    const featuresMenuDia: any = context.lista_planning_features.find((obj) => obj.year === mom.year() && obj.mes === mom.month() + 1 && obj.dia === mom.date())
    planning.push({
      data: menuDia,
      year: nyear,
      mes: month,
      dia: day,
      valores_nutricionales: (featuresMenuDia && featuresMenuDia.valores_nutricionales) ? featuresMenuDia.valores_nutricionales : [],
      cost_recipes: (featuresMenuDia && featuresMenuDia.cost_recipes) ? featuresMenuDia.cost_recipes : 0
    })
    mom.add(1, 'days');
  }
  let structura_nevera = {
    lista_ingredientes_auxiliar: context.lista_ingredientes_planificables,
    lista_ingredientes_lista_compra: context.lista_ingredientes_nevera
  }
  let user = context.lista_personas[0];
  let { emails, averageNutricionalValues, numberSelectedPerson } = await getPersonSelectedValues(context);
  if (!featureVariasPersonas) {
    averageNutricionalValues = user.valores_nutricionales;
    await ajustarKcal(averageNutricionalValues, user)
    numberSelectedPerson = 1;
  }
  const body = {
    planning,
    structura_nevera,
    lista_recetas,
    numberSelectedPerson,
    averageNutricionalValues,
    email: Estructura_Global.email,
    year,
    mes,
    dia_init,
  }
  let resPlanning = await apiCallGet('planningAutomatico', body)
  if (resPlanning == undefined) {
    for (let i = 0; i < 1; i++) {
      let plato_seleccionado = seleccionar_Plato_Aleatorio(planning, year, mes + 1, dia_init);
      let receta_old = plato_seleccionado.receta;
      let old_value = await evaluate_planning(planning, plato_seleccionado, plato_seleccionado.receta, structura_nevera, averageNutricionalValues, context);
      realizar_cambio(planning, structura_nevera, lista_recetas, plato_seleccionado, numberSelectedPerson, context);
      let new_value = await evaluate_planning(planning, plato_seleccionado, plato_seleccionado.receta, structura_nevera, averageNutricionalValues, context);
      if (old_value > new_value && receta_old != -1) {
        revertir_cambio(plato_seleccionado, receta_old, planning, structura_nevera, numberSelectedPerson, context);
      }
    }
    resPlanning = planning;
  }

  if (resPlanning != undefined && numberSelectedPerson > emails.length) {
    await uploadPlanningToFirestore(resPlanning, numberSelectedPerson, diaInit, mom, context)
  }
  emails.map((email: any) => {
    setPendingPlanningFirebase(email, resPlanning.map((obj: any) => obj.data).flat());
  })
}



export const rellenar_calendario_con_IA = async (year: any, mes: any, dia_init: any, featureVariasPersonas: any, context: Context) => {
  let lista_recetas: any = context.lista_recetas;
  const { alergiPersons, notEatPersons } = await getPersonSelectedValues(context);
  console.log(lista_recetas.length)
  if (featureVariasPersonas) {
    lista_recetas = await usarFiltros(lista_recetas, context, alergiPersons, notEatPersons,);
  } else {
    lista_recetas = await usarFiltros(lista_recetas, context);
  }
  await rellenar_con_IA(lista_recetas, year, mes, dia_init, featureVariasPersonas, context);
}

