
import auth from '@react-native-firebase/auth'
import { setTokenAsyncStorage, setUIDAsyncStorage } from '../storage/storage';

export const signupFirebase = async (email: string, password: string) => {
    return await auth()
      .createUserWithEmailAndPassword(email, password)
      .then(async (ret: any) => {
        try {
          const authentication: any = auth();
          const token = await authentication.currentUser.getIdToken()
          const UID = await authentication.currentUser.uid
          await setTokenAsyncStorage(UID)
          await setUIDAsyncStorage(token)
          return ret;
        } catch (error) {
          return error.code
        }
      })
      .catch((error: any) => error.code)
  }
  export const loginFirebase = async (email: string, password: string) => {
    try {

      const authentication: any = auth();
      const result = await authentication.signInWithEmailAndPassword(email, password)
      console.log("result", result)
      const token = await authentication.currentUser.getIdToken()
      const UID = await authentication.currentUser.uid
      await setTokenAsyncStorage(UID)
      await setUIDAsyncStorage(token)

      return result
    } catch (error) {
      console.log(error)
      return error.code
    }
  }