import axios from "axios";
import moment from 'moment';
import { getListaRecetas } from '../../storage/firebase'

export const GET_ALL_RECIPES = 'GET ALL RECIPES';
export const CHANGE_TIME = '[RAAS] CHANGE TIME';

function isString(s) {
    return typeof (s) === 'string' || s instanceof String;
}


export async function getListaRecetasAction() {
    const data=await getListaRecetas()
    return (dispatch) => {
        return dispatch({
            type: GET_ALL_RECIPES,
            payload: data
        })
    }
}



export function checkEmail(bill, billingGroupList) {
    const isEmailSend = bill.mail_sent ? bill.mail_sent : false
    billingGroupList = billingGroupList.map(task => task.idBill == bill.idBill ? { ...bill, mail_sent: true } : task);
    return (dispatch) => {

        let dispatches = []
        if (!isEmailSend) {
            dispatches.push(dispatch(sendEmail(bill.num_invoice)))
        }

        dispatches.push(dispatch({
            type: SEND_EMAIL,
            payload: billingGroupList,
        }))

        dispatches.push(dispatch(showModalEmail(isEmailSend, bill.num_invoice)))

        return Promise.all(dispatches)
    }
}
