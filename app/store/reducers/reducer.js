import * as Actions from '../actions';
import moment from 'moment';


const initialState = {
    lista_ingredientes: [],
    lista_recetas: [],
    lista_ingredientes_nevera: [],
    lista_ingredientes_planificables: [],
    lista_ingredientes_lista_compra: [],
};
const userReducer = (state = initialState, action) => {

    switch (action.type) {
        case Actions.GET_ALL_RECIPES:
            return {
                ...state,
                lista_recetas: action.payload
            };
        default:
            return state
    }
};

export default userReducer;
