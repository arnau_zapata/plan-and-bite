import { combineReducers } from 'redux';
import data from './reducer';

const createReducer = asyncReducers =>
	combineReducers({
		data,
		...asyncReducers
	});


export default createReducer;
