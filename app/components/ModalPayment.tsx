import React, { useRef, useState, useContext, } from "react";
import { StyleSheet, Text, View, TextInput, Dimensions, Alert, Modal, TouchableOpacity } from "react-native";
import { Button } from "react-native-elements";
import { Dropdown } from 'react-native-material-dropdown';
import { ViewRow } from './commonStyledComponents'
import { doPayment } from "../storage/storage";
import ModalPlanNBite from "./ModalPlanNBite";



var s = require('../style');

type Props = {
    visible: boolean,
    closeModal: () => void,
    onPressAccept: () => void,
    amount: number | undefined,
    navigation: any,
    children: any
}



const ModalPayment = ({ visible, closeModal, onPressAccept, amount, navigation, children }: Props) => {
    return (
        <View>
            {amount && <ModalPlanNBite visible={visible} title={'Payment'} onPressCancel={closeModal}>
                {children}
                <Text>pagar {amount}€</Text>
                <ViewRow>
                    <Button title='Cancel' onPress={closeModal} />
                    <Button title='Accept' onPress={async () => {
                        const paid = await doPayment(amount)
                        if (paid) {
                            console.log("pago realizado")
                            closeModal()
                            onPressAccept()
                        } else {
                            closeModal();
                            navigation.navigate("Pagos")
                        }
                    }} />
                </ViewRow>
            </ModalPlanNBite>}
        </View>
    )
}

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        margin: 10
    },
    modalView: {
        backgroundColor: "white",
        borderBottomStartRadius: 20,
        borderBottomEndRadius: 20,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        padding: 10,
        width: '100%'
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});

export default ModalPayment;

