import React, { useRef, useState, useContext, } from "react";
import { StyleSheet, Text, View, TextInput, Dimensions, Alert, ScrollView, TouchableOpacity } from "react-native";



type Item = {
    id: number,
    value: string,
    name: string
}

type Props = {
    onItemSelect: (x: Item) => void,
    containerStyle: any,
    itemStyle: any,
    itemTextStyle: any,
    itemsContainerStyle: any,
    items: any,
}


export default (props: Props) => {
    const [value, setValue] = useState("")
    const [items, setItems] = useState(props.items)
    return (
        <View style={props.containerStyle}>
            <TextInput placeholder='insert' value={value} onChangeText={(text) => { setValue(text); setItems(props.items.filter((obj: Item) => obj.name.toLowerCase().includes(text.toLowerCase()))) }} />
            <ScrollView style={props.itemsContainerStyle}>
                {items.map((item: Item) => {
                    return (
                        <TouchableOpacity style={props.itemStyle} onPress={() => { setValue(item.name); props.onItemSelect(item) }}>
                            <Text>{item.name}</Text>
                        </TouchableOpacity>
                    )
                })}
            </ScrollView>
        </View>
    )
}


