import React, { useRef, useState, useContext, } from "react";
import { StyleSheet, Text, View, TextInput, Dimensions, Alert, Modal, TouchableOpacity } from "react-native";
import { Dropdown } from 'react-native-material-dropdown';
import SearchableDropdown from 'react-native-searchable-dropdown';



var s = require('../style');

type Props = {
    visible: boolean,
    title?: string,
    onPressCancel: ()=>void,
    children:any
}



const ModalPlanNBite = ({visible,title,onPressCancel,children}: Props) => {
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={visible}
            onRequestClose={() => {
                Alert.alert("Modal has been closed.");
            }}

        >
            <View style={{ width: '100%', height: '100%', backgroundColor: 'rgba(0,0,0,0.7)' }}>
                <View style={styles.centeredView}>
                    <View style={{ backgroundColor: 'green', flexDirection: 'row', alignSelf: 'stretch', justifyContent: 'space-between', padding: 10, borderTopStartRadius: 20, borderTopEndRadius: 20 }}>
                        <View />
                        <Text style={{ color: 'white' }}>{title}</Text>
                        <TouchableOpacity onPress={onPressCancel}>
                            <Text style={{ color: 'white' }}>X</Text>
                        </TouchableOpacity>
                    </View>
                    <View
                        style={styles.modalView}
                    >
                        {children}
                    </View>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        margin: 10
    },
    modalView: {
        backgroundColor: "white",
        borderBottomStartRadius: 20,
        borderBottomEndRadius: 20,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        padding:10,
        width:'100%'
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});

export default ModalPlanNBite;

