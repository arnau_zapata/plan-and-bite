
import React, { useState, useEffect } from "react";
import { Slider, Icon } from 'react-native-elements';
import { Text, View, Image, TouchableOpacity } from "react-native";


type Props = {
    index: number,
    setindex: (x: number) => void,
    tabButtonNames: string[],
}


export default (props: Props) => {


    return (
        <View style={{ flexDirection: "row", backgroundColor: 'white', borderBottomWidth: 1, borderTopWidth: 1 }}>
            {props.tabButtonNames.map((name, index) => (
                <TouchableOpacity style={[{ flex: 1, padding: 10, alignItems: 'center', backgroundColor: (props.index == index ? 'white' : 'rgba(0,0,0,0.2)'), paddingVertical: 20, borderLeftWidth: 1 }]} onPress={() => props.setindex(index)}>
                    {name && <Text style={{ color: 'black' }}>{name}</Text>}
                </TouchableOpacity>
            ))}

        </View>
    )
}


