import React, { useRef, useState, useContext, } from "react";
import { Text, View, } from "react-native";



type Props = {
    title: string,
    percentaje: number,
    style?: any
}
const red ='rgba(255,0,0,1)';
const orange ='rgba(255,117,0,1)';
const yellow ='rgba(255,230,0,1)';
const green ='rgba(0,255,0,1)';
const darkGreen ='rgba(0,117,0,1)';

export default ({ title, percentaje, style }: Props) => {
    const borderRadius = 50;
    const borderEndRadius = 10;
    let backgroundColor = "";
    if (percentaje < 15) backgroundColor = red
    else if (percentaje < 30) backgroundColor = orange
    else if (percentaje < 60) backgroundColor = yellow
    else if (percentaje < 80) backgroundColor = green
    else if (percentaje <= 100) backgroundColor = darkGreen
    else if (percentaje < 115) backgroundColor = green
    else if (percentaje < 130) backgroundColor = yellow
    else if (percentaje < 150) backgroundColor = orange
    else backgroundColor = red
    percentaje=(percentaje ? percentaje : 0)
    const widthProcess= (percentaje<=150?percentaje:150)
    console.log(title,percentaje,widthProcess)
    return (
        <View style={[style ? style : undefined, { justifyContent: 'flex-end' }]}>
            <Text style={{ marginLeft: 10, marginBottom: 2, fontSize: 12 }}>{title}</Text>
            <View style={{ flexDirection: "row", width: '100%', alignItems: "center" }}>
                <View style={{
                    width: '50%',
                    height: 30,
                    borderWidth: 1,
                    borderTopStartRadius: borderRadius,
                    borderBottomStartRadius: borderRadius,
                    zIndex: 2
                }}>
                    <View style={{
                        width: (widthProcess ? widthProcess : 0) + '%',
                        height: '100%',
                        borderTopStartRadius: borderRadius,
                        borderBottomStartRadius: borderRadius,
                        borderTopEndRadius: borderEndRadius,
                        borderBottomEndRadius: borderEndRadius,
                        backgroundColor: backgroundColor,
                    }} />
                </View>
                <View style={{
                    width: '25%',
                    height: 30,
                    borderWidth: 1,
                    borderTopEndRadius: borderEndRadius,
                    borderBottomEndRadius: borderEndRadius,
                    borderLeftWidth:1,
                    zIndex: 2
                }}/>
                <Text style={{ marginLeft: 5 }}>{percentaje}%</Text>
            </View>
        </View>
    )
}


