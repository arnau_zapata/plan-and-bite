import React, { useRef, useState, useContext, } from "react";
import { StyleSheet, Text, View, TextInput, Dimensions, Alert } from "react-native";
import { Dropdown } from 'react-native-material-dropdown';
import SearchableDropdown from 'react-native-searchable-dropdown';



var s = require('../style');

type Item = {
    id: number,
    value: string,
    name: string
}

type Props = {
    dropDownData?: string[],
    dropDownSearchableData?: Item[],
    placeholder?: string,
    setText: (x: any) => void,
    text?: string,
    error: string,
    focusNextField?: () => void,
    style?:any,
    password?:boolean,
    reference?: (x: any) => void
    keyboardType?: "default" | "email-address" | "numeric" | "phone-pad" | "number-pad" | "decimal-pad" | "visible-password" | "ascii-capable" | "numbers-and-punctuation" | "url" | "name-phone-pad" | "twitter" | "web-search" | undefined,
}


const TextInputPlanNBite = (props: Props) => {
    const [searchableData, setSearchableData] = useState(props.dropDownSearchableData ? props.dropDownSearchableData : [])

    return (
        <View style={props.style?props.style:{}}>
            {props.dropDownData !== undefined &&
                <Dropdown
                    label={props.placeholder}
                    data={props.dropDownData.map((value: string, index) => { return { value, label: value } })}
                    onChangeText={props.setText}
                    value={props.text?props.text:""}
                    baseColor='#fff'
                    textColor='#999'
                    
                />
            }
            {props.dropDownSearchableData !== undefined &&
                <SearchableDropdown
                    onItemSelect={(item: Item) => props.setText(item.id)}
                    containerStyle={{ padding: 5 }}
                    itemStyle={{
                        padding: 10,
                        marginTop: 2,
                        backgroundColor: 'white',
                        borderColor: 'white',
                        borderWidth: 1,
                        borderRadius: 5,
                    }}
                    itemTextStyle={{ color: 'black' }}
                    itemsContainerStyle={{ height: 220 }}
                    items={props.dropDownSearchableData}
                    resetValue={false}
                    ref={(input:any) => { props.reference && props.reference(input) }}
                    onSubmitEditing={props.focusNextField}
                    textInputProps={{
                        placeholder: props.placeholder,
                        underlineColorAndroid: 'transparent',
                        style: {
                            padding: 12,
                            borderWidth: 1,
                            backgroundColor: 'white',
                            borderColor: '#fff',
                            borderRadius: 10,
                        },
                        onTextChange: (text: string) => setSearchableData(props.dropDownSearchableData.filter((item: Item) => item.name.includes(text))),
                    }}
                    listProps={{
                        nestedScrollEnabled: true,
                    }}
                />
            }
            {!props.dropDownData && !props.dropDownSearchableData &&
                <View style={[s.magicIcon2LaVenganza, { width: 250, height: 60 }]}>
                    <TextInput
                        style={[s.fuente_M, { flex: 1, textAlign: "center", color: '#fff' }]}
                        placeholderTextColor="#fff"
                        value={props.text?props.text:""}
                        placeholder={props.placeholder}
                        secureTextEntry={props.password?props.password:false}
                        onChangeText={(text) => props.setText(text + "")}
                        keyboardType={props.keyboardType ? props.keyboardType : 'default'}
                        ref={(input) => { props.reference && props.reference(input) }}
                        onSubmitEditing={props.focusNextField}

                    />
                </View>
            }
            <Text style={{ textAlign: "center", color: 'red', fontSize: 18 }}> {props.error?props.error:""} </Text>
        </View>
    )
}

export default TextInputPlanNBite;

