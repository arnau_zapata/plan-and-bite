import styled from "styled-components/native";



export const View = styled.View`
    ${({ flex }) => flex != undefined && `flex: ${flex}`};
    ${({ row }) => row && `flexDirection: row;`}
    ${({ br }) => br && `borderRadius: 10px;`}
    ${({ borderVertical }) => borderVertical && `borderTopWidth: 1px; borderBottomWidth:1;`}
    ${({ borderColor }) => borderColor && `borderColor:${borderColor};`}
    ${({ m }) => m && `margin: ${m}px;`}
    ${({ mh }) => mh && `marginHorizontal: ${mh}px;`}
    ${({ mv }) => mv && `marginVertical: ${mv}px;`}
    ${({ mt }) => mt && `marginTop: ${mt}px;`}
    ${({ mb }) => mb && `marginBottom: ${mb}px;`}
    ${({ ml }) => ml && `marginLeft: ${ml}px;`}
    ${({ mr }) => mr && `marginRight: ${mr}px;`}
    ${({ p }) => p && `padding: ${p}px;`}
    ${({ ph }) => ph && `paddingHorizontal: ${ph}px;`}
    ${({ pv }) => pv && `paddingVertical: ${pv}px;`}
    ${({ pt }) => pt && `paddingTop: ${pt}px;`}
    ${({ pb }) => pb && `paddingBottom: ${pb}px;`}
    ${({ pl }) => pl && `paddingLeft: ${pl}px;`}
    ${({ pr }) => pr && `paddingRight: ${pr}px;`}
    ${({ bg }) => bg && `backgroundColor: ${bg};`}
    ${({ center }) => center && `justifyContent: center; alignItems: center;`}
    position: relative;
    ${({ justifyCenter }) => justifyCenter && `justifyContent: center;`}
    ${({ alignCenter }) => alignCenter && `alignItems: center;`}
    ${({ spaceBetween }) => spaceBetween && `justifyContent: space-between;`}
    ${({ width }) => width && `width: ${width};`}
    ${({ height }) => height && `height: ${height};`}
`;

export const Line = styled.View`
    borderWidth:${({ size }) => size ? size : 1}px;
    borderColor:${({ color }) => color ? color : 'black'};
`;


export const Scroll = styled.ScrollView`
    ${({ flex }) => flex != undefined && `flex: ${flex}`};
    ${({ row }) => row && `flexDirection: row;`}
    ${({ br }) => br && `borderRadius: 10px;`}
    ${({ borderVertical }) => borderVertical && `borderTopWidth: 1px; borderBottomWidth:1;`}
    ${({ borderColor }) => borderColor && `borderColor:${borderColor};`}
    ${({ m }) => m && `margin: ${m}px;`}
    ${({ mh }) => mh && `marginHorizontal: ${mh}px;`}
    ${({ mv }) => mv && `marginVertical: ${mv}px;`}
    ${({ mt }) => mt && `marginTop: ${mt}px;`}
    ${({ mb }) => mb && `marginBottom: ${mb}px;`}
    ${({ ml }) => ml && `marginLeft: ${ml}px;`}
    ${({ mr }) => mr && `marginRight: ${mr}px;`}
    ${({ p }) => p && `padding: ${p}px;`}
    ${({ ph }) => ph && `paddingHorizontal: ${ph}px;`}
    ${({ pv }) => pv && `paddingVertical: ${pv}px;`}
    ${({ pt }) => pt && `paddingTop: ${pt}px;`}
    ${({ pb }) => pb && `paddingBottom: ${pb}px;`}
    ${({ pl }) => pl && `paddingLeft: ${pl}px;`}
    ${({ pr }) => pr && `paddingRight: ${pr}px;`}
    ${({ bg }) => bg && `backgroundColor: ${bg};`}
    ${({ center }) => center && `justifyContent: center; alignItems: center;`}
    position: relative;
    ${({ justifyCenter }) => justifyCenter && `justifyContent: center;`}
    ${({ alignCenter }) => alignCenter && `alignItems: center;`}
    ${({ spaceBetween }) => spaceBetween && `justifyContent: space-between;`}
    ${({ width }) => width && `width: ${width};`}
    ${({ height }) => height && `height: ${height};`}
`;

export const Text = styled.Text`
    color: ${({ c }) => c || 'black'};
    fontSize: ${({ fs }) => fs || 14}px;
    ${({ fw }) => fw && `fontWeight: ${fw};`}
    fontFamily:${({ ff }) => ff ? ff : 'Anton-Regular'};
    ${({ center }) => center && `textAlign: center`}
    ${({ bold }) => bold && `fontWeight: bold`}
    ${({ m }) => m && `margin: ${m}px;`}
    ${({ mt }) => mt && `marginTop: ${mt}px;`}
    ${({ mb }) => mb && `marginBottom: ${mb}px;`}
    ${({ ml }) => ml && `marginLeft: ${ml}px;`}
    ${({ mr }) => mr && `marginRight: ${mr}px;`}
    ${({ absolute }) => absolute && `position: absolute;`}
    ${({ mh }) => mh && `marginHorizontal: ${mh}px;`}
    ${({ mv }) => mv && `marginVertical: ${mv}px;`}
    ${({ p }) => p && `padding: ${p}px;`}
    ${({ ph }) => ph && `paddingHorizontal: ${ph}px;`}
    ${({ pv }) => pv && `paddingVertical: ${pv}px;`}
    ${({ c, underline }) => underline && `
        textDecorationLine: underline;
        textDecorationStyle: solid;
        textDecorationColor: ${c || color.textDefault};
    `}
`;


export const ViewRow = styled.View`
    flexDirection: row;
    alignItems: center;
    ${({ justifyCenter }) => justifyCenter && `justifyContent: center;`}
    ${({ spaceBetween }) => spaceBetween && `justifyContent: space-between;`}
    ${({ justifyEnd }) => justifyEnd && `justifyContent: flex-end;`}
    ${({ mt }) => mt && `marginTop: ${mt}px;`}
    ${({ mb }) => mb && `marginBottom: ${mb}px;`}
    ${({ ml }) => ml && `marginLeft: ${ml}px;`}
    ${({ mh }) => mh && `marginHorizontal: ${mh}px;`}
    ${({ mv }) => mv && `marginVertical: ${mv}px;`}
    ${({ ph }) => ph && `paddingHorizontal: ${ph}px;`}
    ${({ pv }) => pv && `paddingVertical: ${pv}px;`}
    ${({ pt }) => pt && `paddingTop: ${pt}px;`}
    ${({ pb }) => pb && `paddingBottom: ${pb}px;`}
    ${({ pl }) => pl && `paddingLeft: ${pl}px;`}
    ${({ pr }) => pr && `paddingRight: ${pr}px;`}
    ${({ bg }) => bg && `backgroundColor: ${bg};`}
    ${({ br }) => br && `borderRadius: 10px;`}
    ${({ center }) => center && `justifyContent: center; alignItems: center;`}
    ${({ p }) => p && `padding: ${p}px;`}
    ${({ width }) => width && `width: ${width};`}
    ${({ height }) => height && `height: ${height};`}

`;

export const Touchable = styled.TouchableOpacity`
    ${({ f }) => f && `flex: ${f};`}
    ${({ bg }) => bg && `backgroundColor: ${bg};`}
    ${({ center }) => center && `justifyContent: center; alignItems: center;`}
    ${({ p }) => p && `padding: ${p}px;`}
    ${({ alignEnd }) => alignEnd && `alignItems: flex-end;`}
    ${({ mt }) => mt && `marginTop: ${mt}px;`}
    ${({ mb }) => mb && `marginBottom: ${mb}px;`}
    ${({ ml }) => ml && `marginLeft: ${ml}px;`}
    ${({ mh }) => mh && `marginHorizontal: ${mh}px;`}
    ${({ mv }) => mv && `marginVertical: ${mv}px;`}
    ${({ mr }) => mr && `marginRight: ${mr}px;`}
    ${({ br }) => br && `borderRadius: 10px;`}
    ${({ alignCenter }) => alignCenter && `alignItems: center;`}
    ${({ justifyCenter }) => justifyCenter && `justifyContent: center;`}
    ${({ spaceBetween }) => spaceBetween && `justifyContent: space-between;`}
    ${({ justifyEnd }) => justifyEnd && `justifyContent: flex-end;`}
    
`;