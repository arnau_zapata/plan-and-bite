import React from 'react'
import { StyleSheet,  ActivityIndicator, View } from 'react-native'


const styles = StyleSheet.create({
    loading: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 5,
        height: 25
      }
})
interface Props{
    style?:any,
    color?:any
}

class Loading extends React.Component<Props>{
    render(){
        return(
            <View style={[styles.loading,this.props.style]}>
            <ActivityIndicator size="small" color={this.props.color?this.props.color:"rgba(100, 165, 96, 1)"} />
            </View>
        )
    }
}

export default Loading