import firestore from '@react-native-firebase/firestore';
import { Receta, Ingrediente, ValorNutricional, Place, Language, CoordinatesPlace, Coordinate } from '../type/type';
import moment from 'moment'
import RnHash, { JSHash, CONSTANTS } from "react-native-hash";
const secretKey = "ca5a3b95efc9c6c2a4174715a758f1eb7e7d313e67437402f5ebf347a8225833";
import Estructura_Global from '../Estructuras/Estructura_Global.json'
import { getEmailUserFirebase, get_Ingrediente } from './storage';
import { Docs, formatMoment } from '../local/constant';
import { emptyDict, flattenList, getLanguage, getValoresNutricionalesListaIngredientes, mergeDict, reduceDict, unmergeDict } from '../Utils/utils';

export const IsEmailAndPasswordValid = async (email: string, password: string) => {
  try {
    email = email.toLocaleLowerCase();
    email = email.replace(new RegExp(" ", 'g'), "")
    let hashPassword = await JSHash(JSON.stringify({ email, password, secretKey }), CONSTANTS.HashAlgorithms.keccak)
    let userDataRef = await firestore().collection("customers").doc(email).get();
    let data = userDataRef.data()
    console.log(hashPassword)
    if (data) {
      return hashPassword === data.password;
    } else {
      return false
    }
  } catch (e) {
    console.log(e)
  }
  return false;

}



//*********** GET REF **************** */


export const getPremiumFeaturesUser = () => {
  return firestore().collection("premium").doc(Estructura_Global.email);
}

export const getUserRef = (email?: string) => {
  return firestore().collection("customers").doc(email ? email : Estructura_Global.email);

}

export const getModificationRef = () => {
  return otherDataRef()
    .doc("modifications")
}
export const getPlanningRef = (year: number, mes: number, email?: string) => {
  return getUserRef(email)
    .collection("planning").doc(year.toString())
    .collection("month").doc(mes.toString())
}
export const getPlanningDayRef = (year: number, mes: number, dia: number, email?: string) => {
  return getPlanningRef(year, mes, email)
    .collection("day").doc(dia.toString())

}

export const otherDataRef = () => {
  return getUserRef()
    .collection("other_data")
}

export const getCurrentPlaceRef = () => {
  return otherDataRef()
    .doc("current_place")
}

export const getIngredientesPlanificablesRef = () => {
  return getUserRef()
    .collection("ingredientes_planificables")
}

export const getIngredientesListaCompraRef = () => {
  return getUserRef()
    .collection("ingredientes_lista_compra")
}

export const getIngredientesNeveraRef = () => {
  return getUserRef()
    .collection("ingredientes_nevera")
}


export const getPersonasRef = () => {
  return getUserRef()
    .collection("personas")
}
export const getPremiumFeaturesPricesRef = () => {
  return firestore().collection("premium").doc("price_features");
}

export const getPersonaRef = async (email?: string) => {
  const personaColectionRef = await getUserRef()
    .collection("personas");
  const personaColectionData = await personaColectionRef.get();
  if (email == undefined) return personaColectionRef.doc(personaColectionData.docs[0].id);
  else {
    const index = personaColectionData.docs.findIndex((obj) => obj.data().email == email);
    return personaColectionRef.doc(personaColectionData.docs[index].id);
  }
}

export const getNotificationsColectionRef = async (email?: string) => {
  const selected_email = email ? email : Estructura_Global.email;
  return firestore().collection("customers").doc(selected_email).collection("notifications");
}

export const getIngredientesRef = () => {
  return firestore().collection("ingredientes");
}

export const getPendingBuyingRef = async (actual_day: string) => {
  return firestore().collection("purchases").doc("pending").collection("users").doc(actual_day + "_" + Estructura_Global.email);
}

export const getAdminRef = async () => {
  return firestore().collection("admin");
}

export const getCostIngredientRef = async (id: string, typePlace: string, current_place: string) => {
  return firestore().collection("ingredientes").doc(id).collection("cost").doc(typePlace).collection("places").doc(current_place);
}

export const getNutricionistasColectionRef = () => {
  return firestore().collection("nutricionista");
}

export const getCollectionUploadDataRef = () => {
  return firestore().collection("upload_data");
}


export const getUploadDataRef = (dataType: string, name: string) => {
  return getCollectionUploadDataRef().doc(dataType).collection("upload_data").doc(name);
}

export const getVerifiedDataRef = (dataType: string, name: string) => {
  return getCollectionUploadDataRef().doc(dataType).collection("verified_data").doc(name);
}

export const getUnvalidatedDataRef = (dataType: string, name: string) => {
  return getUnvalidatedDataCollectionRef(dataType).doc(name);
}


export const getUnvalidatedDataCollectionRef = (dataType: string) => {
  return getCollectionUploadDataRef().doc(dataType).collection("unvalidated_data");
}

export const getLeyendUploadDataRef = () => {
  return getCollectionUploadDataRef().doc("leyend");
}

export const getPlaceRef = (type: string, place: string) => {
  return getTypePlaceCollectionRef(type).doc(place);
}

export const getTypePlaceCollectionRef = (type: string) => {
  return firestore().collection("places").doc(type).collection("places");
}

export const clientNutricionistRef = async (nutricionist_email: string, clientEmail: string) => {
  return getNutricionistasColectionRef().doc(nutricionist_email).collection("clients").doc(clientEmail);
}

export const getUploadDataTypeRef = (typeData: string, hash: string) => {
  return getCollectionUploadDataRef().doc(typeData).collection("data").doc(hash)
}

export const getCollectionLocalRecipesRef = () => {
  return getUserRef().collection("recipes");
}

export const getLocalRecipesRef = (id: string) => {
  return getCollectionLocalRecipesRef().doc(id);
}

export const getCardDataRef = async (cardData?: any) => {
  if (cardData == undefined) {

    const ref = await getUserRef().collection("payments");
    const dataRef = await ref.get();
    let id = dataRef && dataRef.docs && dataRef.docs[0] && dataRef.docs[0].id
      ? dataRef.docs[0].id.toString()
      : undefined

    if (id == undefined) return undefined
    else return ref.doc(id);
  } else {
    const hashCardData = await hash(cardData)
    return getUserRef().collection("payments").doc(hashCardData);
  }
}

export const getPaymentRef = (current_time: string) => {
  return firestore().collection("payments").doc(Estructura_Global.email).collection("payments").doc(current_time);
}

export const getPersonalizedUrlRef = async (url: string) => {
  const path = url.split('/');
  let ref: any = firestore();
  for (let i = 0; i < path.length; i++) {
    if (i % 2 == 0) ref = await ref.collection(path[i]);
    else ref = await ref.doc(path[i])
  }

  return ref;
}


//*********** GET DATA **************** */

export const getPlanningMenu = async (year: number, mes: number, dia: number, comida: number) => {
  let menuRef = await getPlanningRef(year, mes)
    .collection("day").doc(dia.toString())
    .collection("comida").doc(comida.toString())
    .collection("plato")
    .get();
  if (menuRef.docs) {
    let data = menuRef.docs.map((doc) => doc.data())
    return data;
  }
  else {
    return [];
  }
}

export const getPlanningMenuDay = async (year: number, mes: number, dia: number) => {
  let menu0 = await getPlanningMenu(year, mes, dia, 0)
  let menu1 = await getPlanningMenu(year, mes, dia, 1)
  let menu2 = await getPlanningMenu(year, mes, dia, 2)
  let planning = menu0.concat(menu1).concat(menu2)
  return planning
}

export const getUser = async () => {
  let userDataRef = await getUserRef().get();
  return userDataRef && userDataRef.data() ? userDataRef.data() : {}
}

export const setUser = async (data: any) => {
  await getUserRef().set(data, { merge: true });
}


export const getRecipe = async (year: number, mes: number, dia: number, comida: number, plato: number) => {
  let platoRef = await getPlanningRef(year, mes)
    .collection("day").doc(dia.toString())
    .collection("comida").doc(comida.toString())
    .collection("plato").doc(plato.toString())
    .get();
  return platoRef.data() ? platoRef.data() : {}
}

export const setRecipe = async (receta: number, year: number, mes: number, dia: number, comida: number, plato: number, quantity: number) => {
  quantity = quantity ? quantity : 1

  await getPlanningRef(year, mes)
    .collection("day").doc(dia.toString())
    .collection("comida").doc(comida.toString())
    .collection("plato").doc(plato.toString())
    .set({ receta, year, mes, dia, comida, plato, quantity })
  return { receta, year, mes, dia, comida, plato }
}

export const removeRecipe = async (receta: number, year: number, mes: number, dia: number, comida: number, plato: number) => {

  await getPlanningRef(year, mes)
    .collection("day").doc(dia.toString())
    .collection("comida").doc(comida.toString())
    .collection("plato").doc(plato.toString())
    .delete()
}

export const getCostRecipesPlanningMenuDay = async (year: number, month: number, day: number) => {
  let planningDay: any = await getPlanningDayRef(year, month, day).get();
  let cost_recipes = planningDay && planningDay.data() && planningDay.data().cost_recipes
    ? planningDay.data().cost_recipes
    : 0
  return cost_recipes;
}

export const setPlanningDiaCostRecipes = async (year: number, month: number, day: number, cost: number) => {
  if (cost > 0) await getPlanningDayRef(year, month, day).set({ cost_recipes: cost }, { merge: true })
  else await getPlanningDayRef(year, month, day).set({ cost_recipes: firestore.FieldValue.delete() }, { merge: true })
}

export const deleteRecipe = async (year: number, mes: number, dia: number, comida: number, plato: number) => {

  try {
    getPlanningRef(year, mes)
      .collection("day").doc(dia.toString())
      .collection("comida").doc(comida.toString())
      .collection("plato").doc(plato.toString())
      .delete()
  } catch (e) {
    console.log("error", e)
  }
}

export const setNutricionalValue = async (year: number, mes: number, dia: number, valores_nutricionales: any) => {
  await getPlanningDayRef(year, mes, dia).set({ valores_nutricionales: valores_nutricionales }, { merge: true })
}


export const getNumNutricionalSemanalValues = async (year: number, month: number, week: number) => {
  let planningDayMonthRef: any = await getPlanningRef(year, month)
    .get();
  return planningDayMonthRef.data() && planningDayMonthRef.data().num_valores_nutricionales_semana ? planningDayMonthRef.data().num_valores_nutricionales_semana[week - 1] : 0

}
export const getNumNutricionalMensuaValues = async (year: number, month: number) => {
  let planningDayMonthRef: any = await getPlanningRef(year, month)
    .get();
  return planningDayMonthRef.data() && planningDayMonthRef.data().num_valores_nutricionales_mes ? planningDayMonthRef.data().num_valores_nutricionales_mes : 0

}

export const setValoresNutricionalesSemana = async (year: number, month: number, week: number, valores_nutricionales: ValorNutricional) => {
  const planningDataRef = await getPlanningRef(year, month).get();
  const planningData: any = planningDataRef.data();
  const valoresNutricionalesSemana = planningData && planningData.valores_nutricionales_semana ? planningData.valores_nutricionales_semana : [{}, {}, {}, {}, {}, {}];
  valoresNutricionalesSemana[week - 1] = valores_nutricionales;
  await getPlanningRef(year, month).set({ valores_nutricionales_semana: valoresNutricionalesSemana }, { merge: true })

}
export const setValoresNutricionalesMes = async (year: number, month: number, valores_nutricionales: ValorNutricional) => {
  await getPlanningRef(year, month).set({ valores_nutricionales_mes: valores_nutricionales }, { merge: true })

}

export const setNumNutricionalSemanalValues = async (year: number, month: number, week: number, num_valores_nutricionales: number) => {
  const planningDataRef = await getPlanningRef(year, month).get();
  const planningData: any = planningDataRef.data();
  const valoresNutricionalesSemana = planningData && planningData.num_valores_nutricionales_semana ? planningData.num_valores_nutricionales_semana : [0, 0, 0, 0, 0, 0];
  valoresNutricionalesSemana[week - 1] = num_valores_nutricionales;
  await getPlanningRef(year, month).set({ num_valores_nutricionales_semana: valoresNutricionalesSemana }, { merge: true })

}
export const setNumNutricionalMensuaValues = async (year: number, month: number, num_valores_nutricionales: number) => {
  await getPlanningRef(year, month).set({ num_valores_nutricionales_mes: num_valores_nutricionales }, { merge: true })

}



export const getNutricionalValue = async (year: number, month: number, day: number, email?: string) => {
  let planningDayDataRef: any = await getPlanningDayRef(year, month, day, email)
    .get();
  return planningDayDataRef.data() && planningDayDataRef.data().valores_nutricionales ? planningDayDataRef.data().valores_nutricionales : {}

}
export const getNutricionalSemanalValue = async (year: number, month: number, week: number, email?: string) => {
  let planningDayMonthRef: any = await getPlanningRef(year, month, email)
    .get();
  return planningDayMonthRef.data() && planningDayMonthRef.data().valores_nutricionales_semana ? planningDayMonthRef.data().valores_nutricionales_semana[week - 1] : {}

}
export const getNutricionalMonthValue = async (year: number, month: number, email?: string) => {
  let planningDayMonthRef: any = await getPlanningRef(year, month, email)
    .get();
  return planningDayMonthRef.data() && planningDayMonthRef.data().valores_nutricionales_mes ? planningDayMonthRef.data().valores_nutricionales_mes : {}

}

export const getValoresNutricionalesRecomendados = async () => {
  const personaPrincipalRef: any = await getPersonaRef()
  const personaPrincipalDataRef: any = await personaPrincipalRef.get();
  return personaPrincipalDataRef.data() && personaPrincipalDataRef.data().valores_nutricionales ? personaPrincipalDataRef.data().valores_nutricionales : {}
}

export const get_ingredientes_planificables = async () => {
  let collectionDataRef = await getIngredientesPlanificablesRef().get()
  return collectionDataRef.docs.map((obj) => obj.data())
}

export const get_ingredientes_ListaCompra = async () => {
  let collectionDataRef = await getIngredientesListaCompraRef().get()
  return collectionDataRef.docs.map((obj) => obj.data())
}

export const eliminar_ingrediente_planificable = async (ingrediente: any) => {
  let nevera_auxiliar = await getIngredientesPlanificablesRef()
  let nevera_compra = await getIngredientesListaCompraRef()

  let res_cantidad: any = await restar_cantidad_ingrediente_planificable(nevera_auxiliar, ingrediente, ingrediente.cantidad);
  console.log("res_cantidad", res_cantidad)
  if (res_cantidad < 0) {
    await anadir_cantidad_ingrediente_planificable(nevera_compra, ingrediente, res_cantidad * -1);
  }
  return ingrediente;
}

export const anadir_ingrediente_planificable = async (ingrediente: any) => {
  let nevera_auxiliar = await getIngredientesPlanificablesRef()
  let nevera_compra = await getIngredientesListaCompraRef()

  let res_cantidad: any = await restar_cantidad_ingrediente_planificable(nevera_compra, ingrediente, ingrediente.cantidad);
  if (res_cantidad < 0) {
    await anadir_cantidad_ingrediente_planificable(nevera_auxiliar, ingrediente, res_cantidad * -1);
  }
  return ingrediente;
}

export const restar_cantidad_ingrediente_planificable = async (nevera: any, ingrediente: Ingrediente, cantidad: number) => {

  let ingredienteRef = await nevera.doc(ingrediente.id_ingrediente.toString());
  let ingredienteDataRef = await ingredienteRef.get();
  let ingredienteData = ingredienteDataRef.data()
  const ingredienteCantidad = ingredienteData && ingredienteData.cantidad >= 0 ? ingredienteData.cantidad : 0
  if (ingredienteData) {
    let newCantidad = ingredienteCantidad - cantidad;
    console.log("restar_cantidad_ingrediente_planificable", ingrediente.id_ingrediente, ingredienteCantidad, cantidad, newCantidad)
    if (newCantidad <= 0) {
      await ingredienteRef.delete();
      return newCantidad
    } else {
      await ingredienteRef.update({ cantidad: newCantidad })
      return 0;
    }
  }
  else {
    return -cantidad;
  }
}

export const getPlanningFeatureDay = async (year: any, month: any, day: any) => {
  const docDataRef = await getPlanningDayRef(year, month, day).get()
  return docDataRef.data() ? docDataRef.data() : {}
}


export const setPlanningFeatureDay = async (feature: any, year: any, month: any, day: any) => {
  await getPlanningDayRef(year, month, day).set(feature)
}

export const anadir_cantidad_ingrediente_planificable = async (nevera: any, ingrediente: any, cantidad: number) => {
  let ingredienteRef = await nevera.doc(ingrediente.id_ingrediente.toString());
  let ingredienteDataRef = await ingredienteRef.get();
  let ingredienteData = ingredienteDataRef.data()
  const ingredienteCantidad = ingredienteData && ingredienteData.cantidad >= 0 ? ingredienteData.cantidad : 0
  if (ingredienteData) {
    let newCantidad = ingredienteCantidad + cantidad;
    console.log("anadir_cantidad_ingrediente_planificable", ingrediente.id_ingrediente, ingredienteCantidad, cantidad, newCantidad)
    await ingredienteRef.update({ cantidad: newCantidad })
  }
  else {
    const dataIngrediente: any = await get_Ingrediente(ingrediente.id_ingrediente)
    if (dataIngrediente.days_to_expire) {
      ingrediente.expiration_date = moment().add(dataIngrediente.days_to_expire, 'days').format('YYYY-MM-DD')
    }
    await ingredienteRef.set({ ...ingrediente, cantidad }, { merge: true })
  }

}


export const vaciarListaCompra = async () => {
  await getIngredientesListaCompraRef().get().then(async (collectionDataRef) => {
    await Promise.all(collectionDataRef.docs.map((dataRef) => getIngredientesListaCompraRef().doc(dataRef.id.toString()).delete()))
  })
}

export const getListaRecetas = async (addLocal?: boolean) => {
  let collectionRecetasRef = await firestore().collection("recetas");
  let collectionRecetasDataRef = await collectionRecetasRef.get();
  let currentPlace: any = await getCurrentPlace();
  currentPlace = (currentPlace && currentPlace.current_place) ? currentPlace.current_place : currentPlace;
  let listRecipe = await Promise.all(collectionRecetasDataRef.docs.map(async (obj) => {
    return { ...obj.data() }
  }))
  if (addLocal) {
    let local_recipes = await getLocalRecipes();
    local_recipes = local_recipes.map((recipes) => {
      return { ...recipes, local: true }
    })
    listRecipe = [...listRecipe, ...local_recipes];
  }
  listRecipe = await Promise.all(listRecipe.map(async (obj) => {
    let totalCost = 0;
    await Promise.all(obj.lista_ingredientes.map(async (ingrediente: Ingrediente) => {
      const id = ingrediente.id_ingrediente.toString()
      const cost = await getCostIngredient(id, currentPlace)
      totalCost += cost * ingrediente.cantidad
    }))
    totalCost = Math.floor(totalCost * 100) / 100
    return { ...obj, cost: totalCost }
  }))
  return listRecipe;

}

const sortBytime = (x: any, y: any) => {
  if (!x) return -1;
  else if (!y) return 1
  else if (x.time < y.time) return -1;
  else return 1;
}

export const getPersonas = async () => {
  let personasRef = getPersonasRef()
  let personasDataRef = await personasRef.get()
  return personasDataRef.docs.map(doc => doc.data()).sort(sortBytime)
}


export const setCurrentPlace = async (dataFirebase: any) => {
  const docRef = await getCurrentPlaceRef()
  docRef.set(dataFirebase, { merge: true })
}

export const addPerson = async (attr: any, index: number) => {
  let personasRef = getPersonasRef();
  let personasDataRef = await personasRef.get();
  if (index === undefined) await personasRef.doc(attr.time).set(attr, { merge: true });
  else if (personasDataRef.docs[index] === undefined) await personasRef.doc(attr.time).set(attr, { merge: true });
  else {
    delete attr.time;
    await personasRef.doc(personasDataRef.docs[index].id.toString()).set(attr, { merge: true })
  }
}
export const removePerson = async (time: string) => {
  let personasRef = getPersonasRef();
  await personasRef.doc(time).delete();

}


export const addImportedPerson = async (email: any, index: number) => {
  let time = moment().format("YYYY-MM-DD HH:mm:ss");
  let personasRef = getPersonasRef();
  let personasDataRef = await personasRef.get();
  if (index === undefined) {
    await personasRef.doc(time).set({ email, time, imported: true });
    return { email, time, imported: true }
  }
  else if (personasDataRef.docs[index] === undefined) {
    await personasRef.doc(time).set({ email, time, imported: true });
    return { email, time, imported: true }
  }
  else {
    const newTime = personasDataRef.docs[index].id.toString()
    await personasRef.doc(newTime).set({ email, time: newTime, imported: true })
    return { email, time: newTime, imported: true }
  }
}


export const importPerson = async (email: string) => {
  let personaCollectionRef = await firestore().collection("customers").doc(email).collection("personas");
  let personaCollectionDataRef = await personaCollectionRef.get();
  let personaDataRef = personaCollectionDataRef.docs[0]
    ? await personaCollectionRef.doc(personaCollectionDataRef.docs[0].id.toString()).get()
    : undefined;
  return personaDataRef ? personaDataRef.data() : {}
}


export const setLastTimeUsed = async (time: string) => {
  const userRef = await getUserRef();
  userRef.set({ last_time_used: time }, { merge: true })
}


export const getLastTimeUsed = async () => {
  const userRef = await getUserRef();
  const userDataRef: any = await userRef.get();
  return userDataRef.data().last_time_used ? userDataRef.data().last_time_used : undefined
}

export const setCheckdateModification = async (key: string, time: string) => {
  const userRef = await getModificationRef();
  const userDataRef: any = await userRef.get();
  let modifications = userDataRef.data() ? userDataRef.data() : {};
  modifications[key] = time;
  userRef.set(modifications, { merge: true })
}

export const getPesoDia = async (email?: string) => {
  const personaPrincipalRef = await getPersonaRef(email);
  const personaPrincipalDataRef: any = await personaPrincipalRef.get();
  return personaPrincipalDataRef ? personaPrincipalDataRef.data().evolucion_peso_dia : undefined;
}
export const getPesoSemana = async (email?: string) => {
  const personaPrincipalRef = await getPersonaRef(email);
  const personaPrincipalDataRef: any = await personaPrincipalRef.get();
  return personaPrincipalDataRef ? personaPrincipalDataRef.data().evolucion_peso_semana : undefined;
}

export const getPesoMes = async (email?: string) => {
  const personaPrincipalRef = await getPersonaRef(email);
  const personaPrincipalDataRef: any = await personaPrincipalRef.get();
  return personaPrincipalDataRef ? personaPrincipalDataRef.data().evolucion_peso_mes : undefined;
}

export const setPesoDia = async (peso: number) => {
  const personaPrincipalRef = await getPersonaRef();

  const personaPrincipalDataRef: any = await personaPrincipalRef.get();
  let evolucion_peso_dia = personaPrincipalDataRef.data().evolucion_peso_dia ? personaPrincipalDataRef.data().evolucion_peso_dia : {};
  evolucion_peso_dia[moment().format('YYYY-MM-DD')] = peso;
  if (Object.keys(evolucion_peso_dia).length > 7) {
    delete evolucion_peso_dia[Object.keys(evolucion_peso_dia).reduce((accum, obj) => accum < obj ? accum : obj, "zzz")]
  }
  personaPrincipalRef.update({ evolucion_peso_dia })
}
export const setPesoSemana = async (peso: number) => {
  const personaPrincipalRef = await getPersonaRef();
  const personaPrincipalDataRef: any = await personaPrincipalRef.get();
  let evolucion_peso_semana = personaPrincipalDataRef.data().evolucion_peso_semana ? personaPrincipalDataRef.data().evolucion_peso_semana : {};
  const week = moment().day();
  const endWeek = week == 0 ? 0 : 7;
  evolucion_peso_semana[moment().day(endWeek).format('YYYY-MM-DD')] = peso;
  if (Object.keys(evolucion_peso_semana).length > 5) {
    delete evolucion_peso_semana[Object.keys(evolucion_peso_semana).reduce((accum, obj) => accum < obj ? accum : obj, "zzz")]
  }
  personaPrincipalRef.update({ evolucion_peso_semana })
}

export const setPesoMes = async (peso: number) => {
  const personaPrincipalRef = await getPersonaRef();
  const personaPrincipalDataRef: any = await personaPrincipalRef.get();
  let evolucion_peso_mes = personaPrincipalDataRef.data().evolucion_peso_mes ? personaPrincipalDataRef.data().evolucion_peso_mes : {};
  evolucion_peso_mes[moment().endOf('month').format('YYYY-MM-DD')] = peso;
  if (Object.keys(evolucion_peso_mes).length > 5) {
    delete evolucion_peso_mes[Object.keys(evolucion_peso_mes).reduce((accum, obj) => accum < obj ? accum : obj, "zzz")]
  }
  personaPrincipalRef.update({ evolucion_peso_mes })
}

const getValoresNutricionales = async (peso: number, peso_ideal: number, gender: string, altura: string, edad: string, nivel_ejercicio: string, reguladorKcal: number, evolucion_peso_semana: any) => {
  let kcal = 0;
  if (reguladorKcal == undefined) reguladorKcal = 1;
  if (gender == undefined) kcal = 2000;
  else if (altura == undefined || edad == undefined || nivel_ejercicio == undefined) kcal = (gender == 'Mujer' ? 2000 : 2500);
  else {
    let multiplicador_ejercicio = 1;
    if (nivel_ejercicio == 'Poco') multiplicador_ejercicio = 1.2;
    else if (nivel_ejercicio == 'Normal') multiplicador_ejercicio = 1.55;
    else if (nivel_ejercicio == 'Mucho') multiplicador_ejercicio = 1.75;
    kcal = ((10 * peso) + (6.25 * parseInt(altura)) - (5 * parseInt(edad)) + (gender == 'Mujer' ? -164 : 5)) * multiplicador_ejercicio;
  }
  const week = moment().day();
  const endWeek = week == 0 ? 0 : 7;
  const momentEndWeek = moment().day(endWeek);

  if (evolucion_peso_semana[momentEndWeek.format('YYYY-MM-DD')] == undefined) {
    let peso_prediccion = peso;
    let tendenciaAlcista;
    if (peso < peso_ideal) { peso_prediccion += 0.5; tendenciaAlcista = true }
    else if (peso > peso_ideal) { peso_prediccion -= 0.5; tendenciaAlcista = false }
    const pesoSemanaAnterior = evolucion_peso_semana[momentEndWeek.subtract(7, 'days').format('YYYY-MM-DD')]
    if (pesoSemanaAnterior != undefined) {//70 72 true
      const diffPeso = peso - pesoSemanaAnterior;
      if (diffPeso > 1 || diffPeso > -1) { //-2 true
        console.log("kcal,reguladorKcal", kcal, reguladorKcal)
        reguladorKcal = reguladorKcal * (tendenciaAlcista ? 0.95 : 1.05);
        reguladorKcal = Math.floor(reguladorKcal * 1000) / 1000
        kcal *= reguladorKcal;
        console.log("kcal,reguladorKcal", kcal, reguladorKcal)
      }
    }
  }
  kcal = Math.floor(kcal)
  return [
    {
      azucar: 90,
      fibra: 10,
      hidratos_carbono: 260,
      kcal: kcal,
      proteinas: 50,
      grasas: 70,
    },
    reguladorKcal
  ]
}

export const updateValoresNutricionalesRecomendados = async (peso: number) => {
  const personaPrincipalRef: any = await getPersonaRef()
  const personaPrincipalDataRef: any = await personaPrincipalRef.get();
  let { peso_ideal, gender, altura, edad, nivel_ejercicio, reguladorKcal, evolucion_peso_semana } = personaPrincipalDataRef.data();
  const evolucion_peso = evolucion_peso_semana ? evolucion_peso_semana : {}
  let [valores_nutricionales, regulador] = await getValoresNutricionales(peso, peso_ideal, gender, altura, edad, nivel_ejercicio, reguladorKcal, evolucion_peso);
  await personaPrincipalRef.update({ valores_nutricionales, peso, reguladorKcal: regulador })

}

export const getBitecoins = async () => {
  const PremiumFeaturesRef: any = await getUserRef()
  const PremiumFeaturesDataRef: any = await PremiumFeaturesRef.get();
  const data = PremiumFeaturesDataRef.data()
  if (data.bitecoins == undefined) PremiumFeaturesRef.update({ bitecoins: 0 })
  return data.bitecoins ? data.bitecoins : 0;
}

export const get_pending_Bitecoins = async () => {
  const PremiumFeaturesRef: any = await getUserRef()
  const PremiumFeaturesDataRef: any = await PremiumFeaturesRef.get();
  const data = PremiumFeaturesDataRef.data()
  if (data.pending_bitecoins == undefined) PremiumFeaturesRef.update({ pending_bitecoins: 0 })
  return data.pending_bitecoins ? data.pending_bitecoins : 0;
}



export const get_Premium_Features = async () => {
  try {
    const PremiumFeaturesRef: any = await getPremiumFeaturesUser()
    const PremiumFeaturesDataRef: any = await PremiumFeaturesRef.get();
    const features = PremiumFeaturesDataRef.data()
    return features ? features : {};
  } catch {
    return {}
  }
}

export const set_Premium_Features = async (features: any) => {
  Object.entries(features).map(([key, value]) => {
    if (value == "") delete features[key]
  })
  const PremiumFeaturesRef: any = await getPremiumFeaturesUser()
  if (Object.entries(features).length == 0) await PremiumFeaturesRef.delete();
  else await PremiumFeaturesRef.set(features)


}

export const set_Bitecoins = async (bitecoins: any) => {
  const userRef: any = await getUserRef()
  await userRef.set({ bitecoins }, { merge: true })
}



export const get_Premium_Features_Prices = async () => {
  const PremiumFeaturesPricesRef: any = await getPremiumFeaturesPricesRef()
  const PremiumFeaturesPricesDataRef: any = await PremiumFeaturesPricesRef.get();
  const featuresPrices = PremiumFeaturesPricesDataRef.data()
  return featuresPrices;
}

export const setPendingBuying = async (actual_day: string, ingredients: any, cost: number) => {
  const PendingBuyingRef: any = await getPendingBuyingRef(actual_day);
  await PendingBuyingRef.set({ ingredients, cost }, { merge: true })
}

export const getCurrentPlace = async () => {
  const Ref: any = await getCurrentPlaceRef();
  const DataRef: any = await Ref.get();
  const data: Place = DataRef.data();
  return data;
}

const getCostIngredientByTypePlace = async (id: string, doc: string, place: string) => {
  if (place == undefined) {
    return undefined;
  }
  const Ref: any = await getCostIngredientRef(id, doc, place);
  const DataRef: any = await Ref.get();
  const data = (DataRef.data() && DataRef.data().cost) ? DataRef.data().cost : undefined;
  return data;
}

export const getCostIngredient = async (id: string, current_place: Place) => {
  id = id.toString();
  let ret;
  if (current_place == undefined) return 0;
  ret = await getCostIngredientByTypePlace(id, Docs.neighbourhood, current_place.neighbourhood)
  if (ret != undefined) return ret;

  ret = await getCostIngredientByTypePlace(id, Docs.district, current_place.district)
  if (ret != undefined) return ret;
  ret = await getCostIngredientByTypePlace(id, Docs.city, current_place.city)
  if (ret != undefined) return ret;
  ret = await getCostIngredientByTypePlace(id, Docs.province, current_place.province)
  if (ret != undefined) return ret;
  ret = await getCostIngredientByTypePlace(id, Docs.region, current_place.region)
  if (ret != undefined) return ret;
  ret = await getCostIngredientByTypePlace(id, Docs.country, current_place.country)
  if (ret != undefined) return ret;
  else {
    console.log("error getCostIngredient, place not found")
    return 0;
  }
}

export const setPendingPlanning = async (email: string, planning: any) => {
  const time = moment().format('YYYY-MM-DD HH:mm:ss');

  const ColectionRef: any = await getNotificationsColectionRef(email);
  const Ref: any = await ColectionRef.doc(time);
  await Ref.set({ planning, time, type: 'planning', email });
}


export const get_Notifications = async () => {
  const collectionRef = await getNotificationsColectionRef();
  const DataRef = await collectionRef.get();
  const data = DataRef.docs ? DataRef.docs.map(obj => obj.data()) : undefined;
  return data;
}


export const deleteNotification = async (time: string) => {
  const ColectionRef = await getNotificationsColectionRef();
  const Ref = await ColectionRef.doc(time);
  await Ref.delete();
}

export const get_Nutricionistas = async () => {
  const ColectionRef = await getNutricionistasColectionRef();
  const DataRef = await ColectionRef.get();
  const data = DataRef.docs ? DataRef.docs.map((obj: any) => obj.data()) : [];
  return data;
}

export const setCostIngredientPlace = async (id: string, typePlace: string, place: string, cost: number) => {
  const DocRef = await getCostIngredientRef(id, typePlace, place);
  const DataRef = await DocRef.set({ id, typePlace, place, cost });
}

const setPlaceByTypePlace = async (type: string, place: string, placeName: string, language: string, placeInception: Place, coordinates: Coordinate) => {
  const docRef = await getPlaceRef(type, place);
  const dataRef = await docRef.get()
  const dataPlace: any = dataRef.data()
  const aux: any = {};
  aux[language] = placeName;
  let environment = dataPlace ? dataPlace.environment : undefined;
  if (environment == undefined) {
    environment = {
      neighbourhood: [placeInception.neighbourhood],
      district: [placeInception.district],
      city: [placeInception.city],
      province: [placeInception.province],
      region: [placeInception.region],
      country: [placeInception.country],
    }
  }
  else {
    if (!environment.neighbourhood.find((obj: string) => obj == placeInception.neighbourhood)) environment.neighbourhood.push(placeInception.neighbourhood)
    if (!environment.district.find((obj: string) => obj == placeInception.district)) environment.district.push(placeInception.district)
    if (!environment.city.find((obj: string) => obj == placeInception.city)) environment.city.push(placeInception.city)
    if (!environment.province.find((obj: string) => obj == placeInception.province)) environment.province.push(placeInception.province)
    if (!environment.region.find((obj: string) => obj == placeInception.region)) environment.region.push(placeInception.region)
    if (!environment.country.find((obj: string) => obj == placeInception.country)) environment.country.push(placeInception.country)
  }
  const data: any = {
    id: place,
    place,
    placeName: aux,
    environment,
  }
  if (coordinates) data.coordinates = coordinates

  const DataRef = await docRef.set(data, { merge: true });
}

const hash = async (data: any) => {
  return await JSHash(JSON.stringify(data), CONSTANTS.HashAlgorithms.keccak)
}

const hashCoordinate = async (coordinates: Coordinate) => {
  return hash({ x: coordinates.latitude, y: coordinates.longitude })
}

export const setPlace = async (placeName: Place, language: Language, coordinatesPlace: CoordinatesPlace) => {

  const place: Place = {
    neighbourhood: await hashCoordinate(coordinatesPlace.neighbourhood),
    district: await hashCoordinate(coordinatesPlace.district),
    city: await hashCoordinate(coordinatesPlace.city),
    province: await hashCoordinate(coordinatesPlace.province),
    region: await hashCoordinate(coordinatesPlace.region),
    country: await hashCoordinate(coordinatesPlace.country),
  }

  await setPlaceByTypePlace(Docs.neighbourhood, place.neighbourhood, placeName.neighbourhood, language, place, coordinatesPlace.neighbourhood)
  await setPlaceByTypePlace(Docs.district, place.district, placeName.district, language, place, coordinatesPlace.district)
  await setPlaceByTypePlace(Docs.city, place.city, placeName.city, language, place, coordinatesPlace.city)
  await setPlaceByTypePlace(Docs.province, place.province, placeName.province, language, place, coordinatesPlace.province)
  await setPlaceByTypePlace(Docs.region, place.region, placeName.region, language, place, coordinatesPlace.region)
  await setPlaceByTypePlace(Docs.country, place.country, placeName.country, language, place, coordinatesPlace.country)

}

export const getPlaces = async (type: string) => {
  const collectionRef = await getTypePlaceCollectionRef(type);
  const DataRef = await collectionRef.get();
  const data = DataRef.docs ? DataRef.docs.map((obj: any) => obj.data()) : [];
  return data;
}

export const addClientNutricionist = async (nutricionist_email: string) => {
  const clientEmail = Estructura_Global.email;
  const DocRef = await clientNutricionistRef(nutricionist_email, clientEmail)
  DocRef.set({
    email: clientEmail,
    expiration_date: moment().add(31, 'days').format(formatMoment)
  })
}

export const setCreditCardData = async (cardData: any) => {
  const DocRef = await getCardDataRef(cardData)
  if (DocRef == undefined) return undefined
  DocRef.set(cardData)

}

export const getCreditCardData = async () => {
  const DocRef = await getCardDataRef()
  if (DocRef == undefined) return undefined
  else {
    const dataRef = await DocRef.get()
    return dataRef.data()
  }
}

export const addInfoPayment = (amount: number) => {
  const time = moment().format(formatMoment)
  const docRef = getPaymentRef(time);
  docRef.set({
    email: Estructura_Global.email,
    time,
    amount
  })
}

const getPlanningMonth = async (year: number, month: number) => {
  month = month + 1;
  const planing = await getPlanningRef(year, month).collection("day");
  const planingDataRef = await planing.get();
  let data = await Promise.all(planingDataRef.docs.map(async(doc:any)=>{
    return await getPlanningMenuDay(year,month,parseInt(doc.id));
  }));
  data = flattenList(data);
  console.log("data",data)
  return data;
}

export const getPlanning = async () => {
  const monthAgo = moment().subtract(30, "days");
  const nextMonth = moment().add(30, "days");
  const planningMonthAgo = await getPlanningMonth(monthAgo.year(), monthAgo.month());
  const planningMonth = await getPlanningMonth(moment().year(), moment().month());
  const planningMonthLater = await getPlanningMonth(nextMonth.year(), nextMonth.month());

  // while (current_moment.date() != nextMonth.date() || current_moment.month() + 1 != nextMonth.month() + 1) {
  //   const planning = await getPlanningMenuDay(current_moment.year(), current_moment.month() + 1, current_moment.date());
  //   aux = [...aux, ...planning]
  //   current_moment.add(1, 'days');
  // }
  return [...planningMonthAgo, ...planningMonth, ...planningMonthLater]
}

const getPlanningFeaturesDay = async (year: number, mes: number, day: number) => {
  let docDataRef = await getPlanningDayRef(year, mes, day)
    .get();
  return docDataRef.data() ? docDataRef.data() : undefined
}

export const getPlanningFeatures = async () => {
  const monthAgo = moment().subtract(30, "days");
  const nextMonth = moment().add(30, "days");
  const current_moment = monthAgo;
  let aux: any[] = []
  while (current_moment.date() != nextMonth.date() || current_moment.month() != nextMonth.month()) {
    const year = current_moment.year();
    const mes = current_moment.month() + 1;
    const dia = current_moment.date();
    const planning = await getPlanningFeaturesDay(year, mes, dia);
    if (planning) aux = [...aux, { ...planning, year, mes, dia }]
    current_moment.add(1, 'days');
  }
  return aux

}

export const getLeyendUploadData = async (typeData: string) => {
  const docDataRef = await getLeyendUploadDataRef().get();
  const data: any = docDataRef.data() ? docDataRef.data() : {};
  return data[typeData] ? data[typeData] : []
}

export const setUnverifiedData = async (typeData: string, id: string, data: any, totalBitecoins: number) => {
  await getUnvalidatedDataRef(typeData, id.toLowerCase()).set(data)
  const docRef = await getUploadDataRef(typeData, id.toLowerCase());
  const docUploadDataRef = await docRef.get();
  const dataUpload: any = docUploadDataRef.data() ? docUploadDataRef.data() : {}
  mergeDict(dataUpload, data, (obj) => true, (obj) => obj.data)
  docRef.set(dataUpload, { merge: true })
  // await getUploadedDataRef(typeData, id.toLowerCase()).set(data, { merge: true })

  const docDataRef: any = await getUserRef().get()
  const pendingBitecoins = docDataRef.data().pending_bitecoins ? docDataRef.data().pending_bitecoins : 0;
  await getUserRef().update({ pending_bitecoins: pendingBitecoins + totalBitecoins })
}

export const getVerifiedData = async (dataType: string, name: string) => {
  const docRef = getVerifiedDataRef(dataType, name);
  const docDataRef = await docRef.get();
  const data = docDataRef.data() ? docDataRef.data() : {}
  return data;
}

export const getData = async (dataType: string, name: string) => {
  const docRef = getUnvalidatedDataRef(dataType, name);
  const docDataRef = await docRef.get();
  const data = docDataRef.data() ? docDataRef.data() : {}
  return data;
}


export const getUploadData = async (dataType: string, name: string) => {
  const docRef = await getUploadDataRef(dataType, name);
  const docDataRef = await docRef.get();
  const data = docDataRef.data() ? docDataRef.data() : {}
  return data;
}

export const getIsAdmin = async () => {
  const colectionRef = await getAdminRef();
  const email = await getEmailUserFirebase();
  const dataRef = await colectionRef.doc(email).get();
  return dataRef.data() != undefined
}

export const getCollectionData = async (typeData: string) => {
  const collectionDataRef = await getUnvalidatedDataCollectionRef(typeData).get();
  const data = collectionDataRef.docs ? collectionDataRef.docs.map((obj: any) => obj.data()) : [];
  return data;

}
export const addUploadedData = async (typeData: string, id: string, senderData: any) => {
  const docRef = await getUploadDataRef(typeData, id);
  const docDataRef = await docRef.get();
  const docData: any = (docDataRef && docDataRef.data()) ? docDataRef.data() : {};
  // mergeDict(docData, senderData, (obj) => obj.validated, (obj) => obj.data)
  unmergeDict(docData, senderData, (obj) => obj.validated == false)
  if (!emptyDict(docData)) await docRef.set(docData);
  else await docRef.delete();

}



export const validateData = async (typeData: string, id: string, senderData: any) => {
  const docRef = await getVerifiedDataRef(typeData, id);
  const docDataRef = await docRef.get();
  const docData: any = (docDataRef && docDataRef.data()) ? docDataRef.data() : {};
  mergeDict(docData, senderData, (obj) => obj.validated, (obj) => obj.data)
  if (!emptyDict(docData)) await docRef.set(docData);
  else await docRef.delete();
}
export const removeUnvalidatedData = async (typeData: string, id: string, senderData: any) => {
  const docRef = await getUnvalidatedDataRef(typeData, id);
  const docDataRef = await docRef.get();
  const docData: any = (docDataRef && docDataRef.data()) ? docDataRef.data() : {};
  unmergeDict(docData, senderData, (obj) => obj.data != undefined)
  if (!emptyDict(docData)) await docRef.set(docData)
  else await docRef.delete()

}
export const updateValidateBitecoins = async (senderData: any) => {
  const addBitecoinsDict = reduceDict(senderData, ([key, value]) => [value.uploader, (value.bitecoins && value.validated) ? value.bitecoins : 0], (x, y) => x + y)
  await Promise.all(Object.entries(addBitecoinsDict).map(async ([key, value]: any) => {
    const docRef = await getUserRef(key);
    const docDataRef = await docRef.get();
    const docData = docDataRef.data();
    const bitecoins = (docData && docData.bitecoins) ? docData.bitecoins : 0;
    const pending_bitecoins = (docData && docData.pending_bitecoins) ? docData.pending_bitecoins : 0;
    await docRef.update({ bitecoins: bitecoins + value, pending_bitecoins: (pending_bitecoins - value >= 0) ? pending_bitecoins - value : 0 })
  }))
  const removeBitecoinsDict = reduceDict(senderData, ([key, value]) => [value.uploader, (value.bitecoins && value.validated == false) ? value.bitecoins : 0], (x, y) => x + y)
  await Promise.all(Object.entries(removeBitecoinsDict).map(async ([key, value]: any) => {
    const docRef = await getUserRef(key);
    const docDataRef = await docRef.get();
    const docData = docDataRef.data();
    const pending_bitecoins = (docData && docData.pending_bitecoins) ? docData.pending_bitecoins : 0;
    await docRef.update({ pending_bitecoins: (pending_bitecoins - value >= 0) ? pending_bitecoins - value : 0 })
  }))
  console.log("addBitecoinsDict", addBitecoinsDict)
  console.log("removeBitecoinsDict", removeBitecoinsDict)
}


const getTranslatedData = (data: any) => {
  const language = getLanguage();
  console.log("language", language)
  console.log("untranslatedLanguaje", data)
  console.log("type data", typeof data == typeof {}, Array.isArray(data))
  if (Array.isArray(data)) {
    let ret: any = []
    typeof data == typeof [] && data.forEach((value: any) => {
      const aux: any = {}
      Object.entries(value).forEach(([key, value]: any) => {
        aux[key] = value[language] ? value[language] : value
      })
      ret.push(aux)
    })
    console.log("translatedLanguaje", ret)
    return ret
  }
  else if (typeof data == typeof {}) {
    const ret: any = {}
    typeof data == typeof {} && Object.entries(data).forEach(([key, value]: any) => {
      ret[key] = value[language] ? value[language] : value
    })
    console.log("translatedLanguaje", ret)
    return ret;

  }
}

export const getPersonalizedUrlData = async (url: string, withTranslation: boolean, onlyName: boolean) => {
  const docRef = await getPersonalizedUrlRef(url);
  const docDataRef = await docRef.get();
  let data = {};
  if (!docDataRef) data = {};
  if (docDataRef.docs) data = docDataRef.docs.map((doc: any) => onlyName ? { name: doc.data().nombre, id: doc.data().id } : doc.data());
  else if (docDataRef.data()) data = docDataRef.data();
  else data = {}
  if (withTranslation) data = getTranslatedData(data)
  return data;
}


export const getLocalRecipes = async () => {
  const collectionDataRef = await getCollectionLocalRecipesRef().get();
  const data = collectionDataRef.docs ? collectionDataRef.docs.map((obj: any) => obj.data()) : [];
  return data;
}

const toLocalRecipeData = async (senderData: any) => {
  const nombre: string = (senderData.name || senderData.nombre) ? (senderData.name ? senderData.name : senderData.nombre) : "Receta"
  const preparation: string = (senderData.preparation || senderData.preparacion) ? (senderData.preparation ? senderData.preparation : senderData.preparacion) : ""
  const valores_nutricionales = !emptyDict(senderData.lista_ingredientes) ? await getValoresNutricionalesListaIngredientes(senderData.lista_ingredientes) : {}
  const lista_ingredientes = await Promise.all(Object.entries(senderData.lista_ingredientes).map(async ([key, value]: any) => {
    const ingrediente = await get_Ingrediente(key);
    return { ...ingrediente, cantidad: value }
  }));
  const ret = {
    nombre,
    preparacion: preparation,
    lista_ingredientes,
    imagen: "/imagen_recetas/default.jpg",
    id_receta: nombre.toLowerCase(),
    valores_nutricionales
  }
  return ret;
}

export const addLocalRecipe = async (senderData: any, id: any, totalBitecoins: any) => {
  const docDataRef = await getLocalRecipesRef(id);
  senderData = await toLocalRecipeData(senderData);
  await docDataRef.set({ ...senderData, id, totalBitecoins })
}

export const getLocalRecipe = async (docName: string) => {
  const collectionRef = await getCollectionLocalRecipesRef();
  const docDataRef = await collectionRef.doc(docName).get()
  const data = docDataRef.data() ? docDataRef.data() : {};
  return data;
}

export const getListIngredients = async () => {
  const collectionDataRef = await getIngredientesRef().get();
  const data = collectionDataRef.docs ? collectionDataRef.docs.map((obj: any) => obj.data()) : [];
  return data;
}