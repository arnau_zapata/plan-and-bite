import firestore from '@react-native-firebase/firestore';
import Estructura_Global from '../Estructuras/Estructura_Global.json'
import RnHash, { JSHash, CONSTANTS } from "react-native-hash";
import * as firebase from "@react-native-firebase/storage";
import {
    getUserRef,
    getIngredientesNeveraRef,
    getPlanningFeatures,
    getPlanningMenuDay,
    getRecipe,
    setRecipe,
    deleteRecipe,
    getUser,
    setUser,
    getNutricionalValue,
    setNutricionalValue,
    eliminar_ingrediente_planificable,
    anadir_cantidad_ingrediente_planificable,
    restar_cantidad_ingrediente_planificable,
    anadir_ingrediente_planificable,
    vaciarListaCompra,
    getListaRecetas,
    setPlanningDiaCostRecipes,
    getCostRecipesPlanningMenuDay,
    getPersonas,
    addPerson,
    importPerson,
    addImportedPerson,
    removePerson,
    setLastTimeUsed,
    getLastTimeUsed,
    setCheckdateModification,
    getPesoDia,
    getPesoSemana,
    getPesoMes,
    getNutricionalSemanalValue,
    getNutricionalMonthValue,
    getValoresNutricionalesRecomendados,
    setValoresNutricionalesSemana,
    setValoresNutricionalesMes,
    getNumNutricionalSemanalValues,
    getNumNutricionalMensuaValues,
    setNumNutricionalSemanalValues,
    setNumNutricionalMensuaValues,
    setPesoDia,
    setPesoSemana,
    setPesoMes,
    updateValoresNutricionalesRecomendados,
    get_Premium_Features,
    set_Premium_Features,
    getBitecoins,
    set_Bitecoins,
    get_Premium_Features_Prices,
    setPendingBuying,
    getCurrentPlace,
    getCostIngredient,
    setPendingPlanning,
    get_Notifications,
    deleteNotification,
    get_Nutricionistas,
    setCostIngredientPlace,
    setPlace,
    getPlaces,
    addClientNutricionist,
    setUnverifiedData,
    getLeyendUploadData,
    addInfoPayment,
    getPlanning,
    removeRecipe,
    getPlanningFeatureDay,
    setPlanningFeatureDay,
    setCurrentPlace,
    getVerifiedData,
    getData,
    getIsAdmin,
    getCollectionData,
    addUploadedData,
    validateData,
    removeUnvalidatedData,
    updateValidateBitecoins,
    getUploadDataRef,
    getUploadData,
    getPersonalizedUrlData,
    getLocalRecipes,
    addLocalRecipe,
    getListIngredients,
    get_pending_Bitecoins
} from './firebase'
import { AsyncStorage } from 'react-native';
import { apiCallGet, apiCallPost, filterPlanningByDay, findPlanningByPlato, get_Receta, restarValoresNutricionales, sumarValoresNutricionales, weekOfMonth } from '../Utils/utils'
import moment from 'moment'
import { actualizar_ingredientes_planificables_firebase } from '../Utils/utils_nevera'
import { Receta, Ingrediente, Persona, Plato, ValorNutricional, Place, Language, CoordinatesPlace, Context } from '../type/type'
import { formatMoment } from '../local/constant';
import { anadir_valor_nutricional, restar_valor_nutricional } from '../Utils/utils_planning';
import ValidateData from '../screens/ValidateData/ValidateData';


const STRIPE_PUBLISHABLE_KEY = "pk_live_51HgFjQL7t90Ie9qsGe4n2D6LP0h2K9ZnZ8gvdchGjd1zUhos8BaEg3V5FkdOqC16nXNTCFoOoK8XSJimkAfqfwkl00B2ReYLAe"
//********* GET SET ASYNCSTORAGE ************** */

const getObjectAsyncStorage = async (key: string) => {
    let mom = moment();
    let asyncStorageObject: any = await AsyncStorage.getItem(key);
    if (asyncStorageObject) {
        asyncStorageObject = JSON.parse(asyncStorageObject);
        if (asyncStorageObject.expirationDate !== undefined && mom.format('YYYY-MM-DD') > asyncStorageObject.expirationDate) {
            asyncStorageObject.expirationDate = moment().add(7, 'days').format('YYYY-MM-DD');
            await AsyncStorage.setItem(key, JSON.stringify(asyncStorageObject));
            return undefined
        }
        let checkdate: string = asyncStorageObject.checkdate;
        if (!checkdate || checkdate < moment().format(moment.HTML5_FMT.DATETIME_LOCAL_SECONDS)) {
            checkdate = moment().add(2, 'hours').format(moment.HTML5_FMT.DATETIME_LOCAL_SECONDS)
            let user: any = await getUser();
            if (user.modifications && user.modifications[key] > asyncStorageObject.last_time_used) return undefined;
        }
        return asyncStorageObject.data;
    }
}

const deleteObjectAsyncStorage = async (key: string) => {
    await AsyncStorage.removeItem(key);
}


export const setObjectAsyncStorage = async (key: string, data: any, numDays?: number) => {
    let obj = {
        data,
        last_time_used: moment().format(moment.HTML5_FMT.DATETIME_LOCAL_SECONDS),
        checkdate: moment().add(2, 'hours').format(moment.HTML5_FMT.DATETIME_LOCAL_SECONDS),
        expirationDate: moment().add(numDays ? numDays : 7, 'days').format(moment.HTML5_FMT.DATETIME_LOCAL_SECONDS),
    }
    await AsyncStorage.setItem(key, JSON.stringify(obj));

    setCheckdateModification(key, moment().format(moment.HTML5_FMT.DATETIME_LOCAL_SECONDS))
    console.log("set " + key + " to AsyncStorage");
}

const removeReceiptToPlanning = (key: string, context: Context, removeObjects: Plato[]) => {
    let planning: any = []
    if (key == 'planning') {
        planning = context.lista_planning
        removeObjects.forEach((removeObject) => {
            planning = planning.filter((obj: any) =>
                obj.dia != removeObject.dia ||
                obj.mes != removeObject.mes ||
                obj.year != removeObject.year ||
                obj.comida != removeObject.comida ||
                obj.plato != removeObject.plato
            )
        })
    }
    else if (key == 'planning-features') {
        planning = context.lista_planning_features
        removeObjects.forEach((removeObject) => {
            planning = planning.filter((obj: any) =>
                obj.dia != removeObject.dia ||
                obj.mes != removeObject.mes ||
                obj.year != removeObject.year
            )
        })
    }
    return planning
}


const removePersonaToListPersonas = (context: Context, removeObjects: Persona[]) => {
    let personas: any = context.lista_personas

    removeObjects.forEach((removeObject) => {
        personas = personas.filter((obj: Persona) => obj.time != removeObject.time)
    })
    return personas
}

const removeIngredientToListaIngredientes = (key: string, context: Context, removeObjects: Ingrediente[]) => {
    let lista_ingredientes: any = [];
    if (key == 'ingredientes_nevera') lista_ingredientes = context.lista_ingredientes_nevera
    else if (key == 'ingredientes_planificables') lista_ingredientes = context.lista_ingredientes_planificables
    else if (key == 'ingredientes_lista_compra') lista_ingredientes = context.lista_ingredientes_lista_compra
    removeObjects.forEach((removeObject) => {
        lista_ingredientes = lista_ingredientes.filter((obj: Ingrediente) =>
            obj.id_ingrediente != removeObject.id_ingrediente
        )
    })
    return lista_ingredientes
}

const removeContextPersonas = (context: Context, removeObject: any[]) => {
    const key = "personas"
    const planning = removePersonaToListPersonas(context, removeObject);
    context.set_lista_personas(context, planning)
    setObjectAsyncStorage(key, planning)
}
const removeContextPlanning = (context: Context, lista_recetas: Plato[], featuresMenuDia: any[], ingredientes_planificables: Ingrediente[], ingredientes_lista_compra: Ingrediente[]) => {

    let key = "planning"
    let planning = removeReceiptToPlanning(key, context, lista_recetas);
    planning = [...planning]

    setObjectAsyncStorage(key, planning)

    key = "planning-features"
    const featuresWithoutObject = removeReceiptToPlanning(key, context, featuresMenuDia);
    const features = [...featuresWithoutObject, ...featuresMenuDia]
    setObjectAsyncStorage(key, features)

    context.set_lista_planning(context, planning, features, ingredientes_planificables, ingredientes_lista_compra)


}

export const setUnverifiedDataFirebase = async (typeData: string, id: string, data: any, totalBitecoins: number) => {
    id = id.toLowerCase();
    await setUnverifiedData(typeData, id, data, totalBitecoins)

}

export const getUploadDataFirebase = async (typeData: string, id: string) => {
    id = id.toLowerCase();
    return await getUploadData(typeData, id)

}

export const getLeyendUploadDataFirebase = async (typeData: string) => {
    return await getLeyendUploadData(typeData)
}

const updateContextPlanning = (context: Context, lista_recetas: Plato[], featuresMenuDia: any[], ingredientes_planificables: Ingrediente[], ingredientes_lista_compra: Ingrediente[]) => {
    let key = "planning"
    const planningWithoutObject = removeReceiptToPlanning(key, context, lista_recetas);
    const planning = [...planningWithoutObject, ...lista_recetas]
    setObjectAsyncStorage(key, planning)

    key = "planning-features"
    const featuresWithoutObject = removeReceiptToPlanning(key, context, featuresMenuDia);
    const features = [...featuresWithoutObject, ...featuresMenuDia]
    setObjectAsyncStorage(key, features)

    setObjectAsyncStorage("ingredientes_planificables", ingredientes_planificables)

    setObjectAsyncStorage("ingredientes_lista_compra", ingredientes_lista_compra)

    context.set_lista_planning(context, planning, features, ingredientes_planificables, ingredientes_lista_compra)

}

const updateContextNevera = async (context: Context, newObject: any[]) => {
    let key = "ingredientes_nevera"
    let neveraWithoutObject = removeIngredientToListaIngredientes(key, context, newObject)
    const nevera = [...neveraWithoutObject, ...newObject]
    await setObjectAsyncStorage(key, nevera)

    key = "ingredientes_planificables"
    const ingredientes_planificables: any = await get_Lista_Ingredientes_Firebase("ingredientes_planificables");
    await setObjectAsyncStorage(key, ingredientes_planificables)

    key = "ingredientes_lista_compra"
    const ingredientes_lista_compra: any = await get_Lista_Ingredientes_Firebase("ingredientes_lista_compra");
    await setObjectAsyncStorage(key, ingredientes_lista_compra)

    context.set_lista_ingredientes_nevera(context, nevera, ingredientes_planificables, ingredientes_lista_compra)

}
const updateContextListaPlanificablesyCompra = (context: Context, newObject: Ingrediente[]) => {
    let key = "ingredientes_planificables"
    let neveraWithoutObject = removeIngredientToListaIngredientes(key, context, newObject)
    const ingredientes_planificables = [...neveraWithoutObject, ...newObject]
    setObjectAsyncStorage(key, ingredientes_planificables)

    key = "ingredientes_lista_compra"
    neveraWithoutObject = removeIngredientToListaIngredientes(key, context, newObject)
    const ingredientes_lista_compra = [...neveraWithoutObject, ...newObject];
    setObjectAsyncStorage(key, ingredientes_lista_compra)

    context.set_lista_ingredientes_planificablesyCompra(context, ingredientes_planificables, ingredientes_lista_compra)
}

const removeContextListaPlanificablesyCompra = (context: Context, newObject: Ingrediente[]) => {
    let key = "ingredientes_planificables"
    let ingredientes_planificables = removeIngredientToListaIngredientes(key, context, newObject)
    setObjectAsyncStorage(key, ingredientes_planificables)

    key = "ingredientes_lista_compra"
    const ingredientes_lista_compra = removeIngredientToListaIngredientes(key, context, newObject)
    setObjectAsyncStorage(key, ingredientes_lista_compra)

    context.set_lista_ingredientes_planificablesyCompra(context, ingredientes_planificables, ingredientes_lista_compra)
}

const updateContextPlanningFeatures = (context: Context, newObject: any[]) => {
    const key = "planning-features"
    const planningWithoutObject = removeReceiptToPlanning(key, context, newObject)
    const planning = [...planningWithoutObject, ...newObject]
    context.set_lista_planning_features(context, planning)
    setObjectAsyncStorage(key, planning)
}

const updateContextPersonas = (context: Context, newObject: Persona[]) => {
    const key = "personas"
    const personasWithoutObject = removePersonaToListPersonas(context, newObject)
    const personas = [...personasWithoutObject, ...newObject]
    context.set_lista_personas(context, personas)
    setObjectAsyncStorage(key, personas)
}


const removeContextNevera = async (context: Context, newObject: Ingrediente[]) => {
    let key = "ingredientes_nevera"
    const nevera = removeIngredientToListaIngredientes(key, context, newObject)
    await setObjectAsyncStorage(key, nevera)

    key = "ingredientes_planificables"
    const ingredientes_planificables: any = await get_Lista_Ingredientes_Firebase("ingredientes_planificables");
    await setObjectAsyncStorage(key, ingredientes_planificables)

    key = "ingredientes_lista_compra"
    const ingredientes_lista_compra: any = await get_Lista_Ingredientes_Firebase("ingredientes_lista_compra");
    await setObjectAsyncStorage(key, ingredientes_lista_compra)


    console.log("ingredientes_planificables", ingredientes_planificables)

    console.log("ingredientes_lista_compra", ingredientes_lista_compra)

    context.set_lista_ingredientes_nevera(context, nevera, ingredientes_planificables, ingredientes_lista_compra)

}



export const getEmailUserFirebase = async () => {
    let key = `email`;
    let data = await getObjectAsyncStorage(key)
    return data;
}

export const setEmailUserFirebase = async (email: string) => {
    let key = `email`;
    setObjectAsyncStorage(key, email, 30)
}

export const deleteEmailUserFirebase = async () => {
    let key = `email`;
    let data = await deleteObjectAsyncStorage(key)
    return data;
}
export const setTokenAsyncStorage = (accessToken: string) => {
    AsyncStorage.setItem('accessToken', accessToken)
}
export const setUIDAsyncStorage = (UID: string) => {
    AsyncStorage.setItem('UID', UID)
}
//********* GET INGREDIENTS AND RECIPES ************** */

export const getListIngredientsFirebase = async () => {
    let key = "lista-ingredientes";
    let listaIngredientes = await getObjectAsyncStorage(key)
    if (!listaIngredientes) {
        let listaIngredientes = await getListIngredients();
        setObjectAsyncStorage(key, listaIngredientes)
    }
    return listaIngredientes;
}


export const get_Ingrediente = async (id: number) => {
    let list_ingredientes = await getListIngredientsFirebase();
    const ingrediente: Ingrediente = list_ingredientes.find((obj: any) => obj.id_ingrediente == id)
    return ingrediente ? ingrediente : {}
}

//********* GET INGREDIENT LIST ************** */
export const get_Lista_Ingredientes_Firebase = async (key: string) => {
    try {
        const userRef = getUserRef();
        const neveraDataRef = await userRef.collection(key).get();
        const neveraDataCollection = neveraDataRef.docs;
        if (neveraDataCollection) {
            let ret = neveraDataCollection.map((obj) => obj.data())
            ret = await Promise.all(ret.map(async (obj) => {
                return { ...await get_Ingrediente(obj.id_ingrediente), cantidad: obj.cantidad, id_ingrediente: obj.id_ingrediente, expiration_date: obj.expiration_date }
            }))
            return ret;
        }
        else {
            return []
        }
    } catch (e) {
        console.log("error", e)
    }


}


export const get_Ingredientes_Nevera = async () => {
    let key = "ingredientes_nevera";
    let listaIngredientes = await getObjectAsyncStorage(key)
    if (!listaIngredientes) {
        listaIngredientes = await get_Lista_Ingredientes_Firebase(key);
        setObjectAsyncStorage(key, listaIngredientes)
    }
    return listaIngredientes;
}



export const get_Ingredientes_Planificables = async () => {
    let key = "ingredientes_planificables";
    let listaIngredientes = await getObjectAsyncStorage(key)
    if (!listaIngredientes) {
        listaIngredientes = await get_Lista_Ingredientes_Firebase(key);
        setObjectAsyncStorage(key, listaIngredientes)
    }
    return listaIngredientes;
}

export const get_lista_compra = async () => {
    let key = "ingredientes_lista_compra";
    let listaIngredientes = await getObjectAsyncStorage(key)
    if (!listaIngredientes) {
        listaIngredientes = await get_Lista_Ingredientes_Firebase(key);
        setObjectAsyncStorage(key, listaIngredientes)
    }
    return listaIngredientes;
}


//********* GET SET REMOVE PLANNING ************** */
export const getPlanningFeaturesFirebase = async () => {
    let key = `planning-features`;
    let data = await getObjectAsyncStorage(key)
    if (!data) {
        data = await getPlanningFeatures();
        setObjectAsyncStorage(key, data)
    }
    return data;

}

export const getPlanningFirebase = async () => {
    let key = `planning`;
    let data = await getObjectAsyncStorage(key)
    if (!data) {
        data = await getPlanning();
        setObjectAsyncStorage(key, data)
    }
    return data;

}

export const seleccionar_Menu_dia = async (year: number, mes: number, dia: number) => {
    const data = await getPlanningMenuDay(year, mes, dia);
    return data;
}


export const getConfiguration = async () => {
    let key = `configuration`;
    let data = await getObjectAsyncStorage(key)
    if (!data || Object.keys(data).length == 0) {
        data = {
            vegetariano: false,
            limite_presupuesto: false,
            limite_tiempo: false,
        };
        setObjectAsyncStorage(key, data)
    }
    return data;
}

export const getUserFirebase = async () => {
    let key = `user`;
    let data = await getObjectAsyncStorage(key)
    if (!data || Object.keys(data).length == 0) {
        data = await getUser();
        setObjectAsyncStorage(key, data)
    }
    return data;
}

export const getCurrentPlaceFirebase = async () => {
    let key = `current_place`;
    let data = undefined//await getObjectAsyncStorage(key)
    if (!data) {
        data = await getCurrentPlace();
        setObjectAsyncStorage(key, data)
    }
    return data;
}

export const setCurrentPlaceFirebase = async (dataFirebase: any) => {
    await setCurrentPlace(dataFirebase)
    const data = await getListaRecetas();
    setObjectAsyncStorage("current_place", data)
}

export const getListaRecetasFirestore = async () => {
    let key = `lista_recetas`;
    let data = await getObjectAsyncStorage(key)
    if (!data) {
        data = await getListaRecetas(true);
        setObjectAsyncStorage(key, data)
    }
    return data;
}


export const getValoresNutricionalesMenuDia = async (year: number, month: number, day: number, email?: string) => {
    let key = `valores-nutricionales-${year}-${month}-${day}`;
    let data = !email
        ? await getObjectAsyncStorage(key)
        : undefined
    if (!data) {
        data = await getNutricionalValue(year, month, day, email);
        if (!email) setObjectAsyncStorage(key, data)
    }
    return data;
}

export const getValoresNutricionalesSemmana = async (year: number, month: number, semana: number, email?: string) => {
    let key = `valores-nutricionales-semana-${year}-${month}-${semana}`;
    let data = !email
        ? await getObjectAsyncStorage(key)
        : undefined
    if (!data) {
        data = await getNutricionalSemanalValue(year, month, semana, email);
        if (!email) setObjectAsyncStorage(key, data)
    }
    return data;
}


export const getValoresNutricionalesMes = async (year: number, month: number, email?: string) => {
    let key = `valores-nutricionales-${year}-${month}`;
    let data = !email
        ? await getObjectAsyncStorage(key)
        : undefined
    if (!data) {
        data = await getNutricionalMonthValue(year, month, email);
        if (!email) setObjectAsyncStorage(key, data)
    }
    return data;
}

export const getValoresNutricionalesRecomendadosFirebase = async () => {
    let key = `valores-nutricionales-recomendados`;
    let data = await getObjectAsyncStorage(key)
    if (!data) {
        data = await getValoresNutricionalesRecomendados();
        setObjectAsyncStorage(key, data)
    }
    return data;
}
export const actualizar_plato_firestore = async (oldPlato: Plato, newPlato: Plato | undefined, context: Context, upload: boolean) => {
    const year = newPlato ? newPlato.year : oldPlato.year;
    const mes = newPlato ? newPlato.mes : oldPlato.mes;
    const dia = newPlato ? newPlato.dia : oldPlato.dia;

    const recetaNewPlato = newPlato ? newPlato.receta : -1;
    const recetaOldPlato = oldPlato ? oldPlato.receta : -1;
    if (newPlato) await setRecipe(newPlato.receta, newPlato.year, newPlato.mes, newPlato.dia, newPlato.comida, newPlato.plato, newPlato.quantity);
    else {
        await removeRecipe(recetaOldPlato, year, mes, dia, oldPlato.comida, oldPlato.plato)
    }

    await actualizar_ingredientes_planificables_firebase(recetaOldPlato, recetaNewPlato, context);

    const cost_recipes = await actualizar_coste(recetaOldPlato, recetaNewPlato, year, mes, dia, context);
    const oldReceipt = get_Receta(context, recetaOldPlato)
    const newReceipt = get_Receta(context, recetaNewPlato)
    const featuresMenuDia = filterPlanningByDay(context.lista_planning_features, year, mes, dia)[0]
    let valores_nutricionales = featuresMenuDia ? featuresMenuDia.valores_nutricionales : {}
    valores_nutricionales = await restar_valor_nutricional(oldReceipt, valores_nutricionales);
    valores_nutricionales = await anadir_valor_nutricional(newReceipt, valores_nutricionales);
    const features = { cost_recipes, valores_nutricionales, year, mes, dia }

    await setPlanningFeatureDay(features, year, mes, dia);
    if (!upload) return {}

    const ingredientes_planificables: any = await get_Lista_Ingredientes_Firebase("ingredientes_planificables");
    const ingredientes_lista_compra: any = await get_Lista_Ingredientes_Firebase("ingredientes_lista_compra");
    if (newPlato) {
        await updateContextPlanning(
            context,
            [newPlato],
            [{ cost_recipes, valores_nutricionales }],
            ingredientes_planificables,
            ingredientes_lista_compra
        )

    } else {
        await removeContextPlanning(
            context,
            [oldPlato],
            [{ cost_recipes, valores_nutricionales }],
            ingredientes_planificables,
            ingredientes_lista_compra
        )
    }
}

export const setUserConfigurationFirebase = async (newConfiguration: any) => {
    let data: any = await getConfiguration();
    Object.entries(newConfiguration).map(([key, value]) => {
        data[key] = value
    })

    let key = `configuration`;
    setObjectAsyncStorage(key, data, 10000)

}


export const clearAllData = () => {
    AsyncStorage.getAllKeys()
        .then(keys => AsyncStorage.multiRemove(keys))
}

export const actualizar_coste = async (id_old_receta: number, id_receta: number, year: number, month: number, day: number, context: Context) => {
    const old_receta = get_Receta(context, id_old_receta);
    const receta = get_Receta(context, id_receta);

    let costNewRecipe = receta && receta.cost ? receta.cost : 0;
    let costOldRecipe = old_receta && old_receta.cost ? old_receta.cost : 0;
    let diference_prices = costNewRecipe - costOldRecipe;


    const featuresMenuDia = context.lista_planning_features.find((obj) => obj.year === year && obj.mes === month && obj.dia === day);
    const cost_recipes = (featuresMenuDia && featuresMenuDia.cost_recipes && featuresMenuDia.cost_recipes > 0) ? featuresMenuDia.cost_recipes : 0
    const result = cost_recipes + diference_prices
    return result > 0 ? result : 0
}



export const setPlanningDiaValoresNutricionales = async (year: number, mes: number, dia: number, valores_nutricionales: any, context: any) => {
    let data = await getNutricionalValue(year, mes, dia);
    const semana = weekOfMonth(year, mes, dia);
    let dataSemana = await getNutricionalSemanalValue(year, mes, semana);
    let dataMes = await getValoresNutricionalesMes(year, mes);
    let numSemanal: number = await getNumNutricionalSemanalValues(year, mes, semana)
    let numMensual: number = await getNumNutricionalMensuaValues(year, mes);

    if (data != undefined && Object.values(data).length > 0 && !Object.values(data).every((obj: any) => obj <= 0)) {
        dataSemana = restarValoresNutricionales(dataSemana, data, numSemanal);
        numSemanal--;
        dataMes = restarValoresNutricionales(dataMes, data, numMensual);
        numMensual--;
    }

    await setNutricionalValue(year, mes, dia, valores_nutricionales)
    data = await getNutricionalValue(year, mes, dia)

    if (data != undefined && Object.values(data).length > 0 && !Object.values(data).every((obj: any) => obj <= 0)) {

        dataSemana = sumarValoresNutricionales(dataSemana, data, numSemanal)
        numSemanal++;


        dataMes = sumarValoresNutricionales(dataMes, data, numMensual)
        numMensual++;

    }
    setValoresNutricionalesSemanaFirebase(year, mes, semana, dataSemana);
    setValoresNutricionalesMesFirebase(year, mes, dataMes)
    setNumNutricionalSemanalValues(year, mes, semana, numSemanal)
    setNumNutricionalMensuaValues(year, mes, numMensual)

    const featuresMenuDia = context.lista_planning_features.find((obj: any) => obj.year === year && obj.mes === mes && obj.dia === dia);

    updateContextPlanningFeatures(context, [{ ...featuresMenuDia, valores_nutricionales: data }])

}
export const setValoresNutricionalesSemanaFirebase = async (year: number, month: number, week: number, valoresNutricionales: ValorNutricional) => {
    await setValoresNutricionalesSemana(year, month, week, valoresNutricionales);

    let key = `valores-nutricionales-semana-${year}-${month}-${week}`;
    let data = await getNutricionalSemanalValue(year, month, week)
    setObjectAsyncStorage(key, data)
}

export const setValoresNutricionalesMesFirebase = async (year: number, month: number, valoresNutricionales: ValorNutricional) => {
    await setValoresNutricionalesMes(year, month, valoresNutricionales);

    let key = `valores-nutricionales-${year}-${month}`;
    let data = await getNutricionalMonthValue(year, month)
    setObjectAsyncStorage(key, data)
}



export const setPlanningDayCostRecipes = async (year: number, month: number, day: number, cost_recipes: number, context: Context) => {
    await setPlanningDiaCostRecipes(year, month, day, cost_recipes);

    const featuresMenuDia = context.lista_planning_features.find((obj: any) => obj.year === year && obj.mes === month && obj.dia === day);
    await setPlanningDiaCostRecipes(year, month, day, cost_recipes);
    updateContextPlanningFeatures(context, [{ ...featuresMenuDia, cost_recipes }])
}




export const eliminar_ingrediente_planificable_firestore = async (ingrediente: any) => {
    await eliminar_ingrediente_planificable(ingrediente);
}

export const anadir_ingrediente_planificable_firestore = async (ingrediente: any) => {
    await anadir_ingrediente_planificable(ingrediente);
}

export const update_Ingrediente_Nevera = async (ingrediente: Ingrediente, diffCantidad: number, context: any) => {
    let nevera = await getIngredientesNeveraRef()
    let outIngrediente: any = { id_ingrediente: ingrediente.id_ingrediente, cantidad: ingrediente.cantidad }
    if (diffCantidad >= 0) {
        await anadir_cantidad_ingrediente_planificable(nevera, outIngrediente, diffCantidad);
        await anadir_ingrediente_planificable({ ...ingrediente, cantidad: diffCantidad });
    } else {
        await restar_cantidad_ingrediente_planificable(nevera, outIngrediente, -diffCantidad)
        await eliminar_ingrediente_planificable({ ...ingrediente, cantidad: -diffCantidad });
    }
    console.log("ingrediente.cantidad + diffCantidad", ingrediente.cantidad, diffCantidad, ingrediente.cantidad + diffCantidad)
    if (ingrediente.cantidad > 0) updateContextNevera(context, [ingrediente])
    else removeContextNevera(context, [ingrediente])

}

export const vaciarListaCompraFirebase = async () => {
    let data = await get_lista_compra()
    const neveraRef = getIngredientesNeveraRef();
    data.map((ingrediente: any) => anadir_cantidad_ingrediente_planificable(neveraRef, { id_ingrediente: ingrediente.id_ingrediente, cantidad: ingrediente.cantidad }, 0))

    await vaciarListaCompra();

    let key = "ingredientes_nevera";
    data = await get_Lista_Ingredientes_Firebase(key)
    setObjectAsyncStorage(key, data)

    key = "ingredientes_lista_compra";
    data = await get_Lista_Ingredientes_Firebase(key)
    setObjectAsyncStorage(key, data)
}


export const comandaCompraFirebase = async (cost: number) => {
    let data = await get_lista_compra()

    const actual_day = moment().format("YYYY-MM-DD");

    await setPendingBuying(actual_day, data, cost)

    let key = "ingredientes_lista_compra";
    data = await get_Lista_Ingredientes_Firebase(key)
    setObjectAsyncStorage(key, data)
}

export const get_List_Recetas_Planning = async (dia_init: string, dia_fin: string) => {
    let last_time_used = moment(dia_init, "YYYY-MM-DD");
    let now = moment(dia_fin, "YYYY-MM-DD");
    let new_planning: any[] = [];
    while (last_time_used.format("YYYY-MM-DD") <= now.format("YYYY-MM-DD")) {
        let year = last_time_used.year();
        let mes = last_time_used.month() + 1;
        let dia = last_time_used.date();
        let planning_dia: any[] = await seleccionar_Menu_dia(year, mes, dia);
        new_planning = new_planning.concat(planning_dia)
        last_time_used.add(1, 'days');
    }
    return new_planning
}

export const actualizar_nevera = async (context: Context) => {
    let key = "last_time_used";
    let last_time_used = await getLastTimeUsed();
    let now = moment();
    console.log("last_time_used", last_time_used)

    await setLastTimeUsed(now.format("YYYY-MM-DD"))
    if (last_time_used == now.format("YYYY-MM-DD")) return;

    let nevera = getIngredientesNeveraRef();

    let planning = await get_List_Recetas_Planning(last_time_used, moment().subtract(1, 'days').format("YYYY-MM-DD"))

    if (planning.length == 0) return;

    planning = planning.map((receta: any) => get_Receta(context, receta.receta))
    let listIngredients = await accumulate_Cuantity_List_Ingredients(planning, context)
    await Promise.all(listIngredients.map((ingredient: any) => restar_cantidad_ingrediente_planificable(nevera, ingredient, ingredient.cantidad)))

    key = "ingredientes_nevera"
    let dataNevera = await get_Lista_Ingredientes_Firebase(key)
    await setObjectAsyncStorage(key, dataNevera)

}

export const accumulate_Cuantity_List_Ingredients = (lista_recetas: Receta[], context: Context) => {
    let listaIngredientes: any = lista_recetas.map((receta: any) => {
        receta = receta.lista_ingredientes ? receta : get_Receta(context, receta.receta);
        let ret = receta ? receta.lista_ingredientes : []
        return ret ? ret : []
    });
    listaIngredientes = listaIngredientes.reduce((accum: any, ingrediente: any) => ingrediente.concat(accum), [])
    listaIngredientes = listaIngredientes.map((ingrediente: any) => { return { id_ingrediente: ingrediente.id_ingrediente, cantidad: ingrediente.cantidad } })
    let newListaIngredientes = []
    while (listaIngredientes.length > 0) {
        let ingrediente = listaIngredientes[0];
        let newCantidad = listaIngredientes.reduce((accum: any, ingredientelista: any) => ingredientelista.id_ingrediente == ingrediente.id_ingrediente ? ingredientelista.cantidad + accum : accum, 0)
        newListaIngredientes.push({ id_ingrediente: ingrediente.id_ingrediente, cantidad: newCantidad })
        listaIngredientes = listaIngredientes.filter((ingredienteLista: any) => ingredienteLista.id_ingrediente != ingrediente.id_ingrediente)
    }
    return newListaIngredientes;
}

export const importPersonFirebase = async (email: string) => {
    email = email.toLocaleLowerCase();
    email = email.replace(new RegExp(" ", 'g'), "")
    return await importPerson(email)
}


export const getListPersonas = async () => {
    let key = `personas`;
    let data = await getObjectAsyncStorage(key)
    if (!data) {
        data = await getPersonas()
        data = await Promise.all(data.map(async (obj: any) => {
            if (obj.email && obj.imported) return { ...obj, ...(await importPerson(obj.email)), email: obj.email }
            else return obj;
        }));
        setObjectAsyncStorage(key, data)
    }
    return data;
}

export const addNewUser = async ( email: string) => {
    email = email.toLocaleLowerCase();
    email = email.replace(new RegExp(" ", 'g'), "")
    Estructura_Global.email = email;
    setUser({ email });
    return true

}

export const addPersonFirebase = async (attr: any, index: number, context: any) => {
    let time = moment().format("YYYY-MM-DD HH:mm:ss");
    await addPerson({ ...attr, time }, index);
    const key = `personas`;
    const data = await getPersonas()
    setObjectAsyncStorage(key, data);

    updateContextPersonas(context, [{ ...attr, time }])
}

export const removePersonFirebase = async (persona: Persona, context: any) => {
    await removePerson(persona.time);

    removeContextPersonas(context, [persona])
}

export const addImportedPersonFirebase = async (email: string, index: number, context: any) => {
    email = email.toLocaleLowerCase();
    email = email.replace(new RegExp(" ", 'g'), "")

    const persona: any = await addImportedPerson(email, index);

    updateContextPersonas(context, [persona])
}

export const updateSelectedPerson = async (index: number, selected: boolean, context: Context) => {

    const listaPersonas = context.lista_personas
    const persona = listaPersonas[index]
    if (persona) {
        persona.selected = selected;
    }
    updateContextPersonas(context, [persona])
}

export const getPersonSelectedValues = async (context: Context) => {
    const listaPersonas = context.lista_personas;
    let selectedUsers = listaPersonas.filter((person: Persona) => person.selected);

    let numberSelectedPerson = selectedUsers.length;
    let averageNutricionalValues: any = selectedUsers.filter((person) => person.valores_nutricionales).reduce((accum: any, person: Persona) => {
        Object.entries(person.valores_nutricionales).map(([key, value]: any[]) => {
            let averageValue = value / numberSelectedPerson;
            accum[key] = accum[key] ? accum[key] + averageValue : averageValue;
        })
        return accum
    }, {});
    let emails = selectedUsers
        .filter((person: Persona) => person.time != listaPersonas[0].time && person.email != undefined)
        .map((person: Persona) => person.email);

    let lista_alergias = listaPersonas.reduce((accum: string[], person: any) => accum.concat(person.alergias ? person.alergias : []), []);
    let lista_noCome = listaPersonas.reduce((accum: string[], person: Persona) => accum.concat(person.noCome ? person.noCome : []), []);
    return { emails, averageNutricionalValues, numberSelectedPerson, alergiPersons: lista_alergias, notEatPersons: lista_noCome }

}

const getPlanningFeatureDayFirebase = async (year: number, month: number, day: number) => {
    return await getPlanningFeatureDay(year, month, day)
}

export const actualizarPlanningDia = async (planning_dia: Plato[], quantity: number, diaInit: any, diaEnd: any, context: Context) => {
    console.log("planning_semana", planning_dia)
    await Promise.all(planning_dia.map(async (plato: Plato) => {
        const oldReceipt = findPlanningByPlato(context.lista_planning, plato.year, plato.mes, plato.dia, plato.comida, plato.plato)
        await actualizar_plato_firestore(oldReceipt, { ...plato, quantity }, context, false)
    }))
    const ingredientes_planificables: any = await get_Lista_Ingredientes_Firebase("ingredientes_planificables");
    const ingredientes_lista_compra: any = await get_Lista_Ingredientes_Firebase("ingredientes_lista_compra");

    let features: any[] = [];
    while (diaInit.format("YYYY-MM-DD") < diaEnd.format("YYYY-MM-DD")) {
        const year = diaInit.year()
        const mes = diaInit.month() + 1
        const dia = diaInit.date()
        if (filterPlanningByDay(planning_dia, year, mes, dia).length > 0) {
            const data = await getPlanningFeatureDayFirebase(year, mes, dia);
            features.push({ ...data, year, mes, dia })
        }
        diaInit.add(1, "days")
    }
    await updateContextPlanning(
        context,
        planning_dia,
        features,
        ingredientes_planificables,
        ingredientes_lista_compra
    )

}

export const get_Premium_Features_Firebase = async () => {
    let key = `premium`;
    let data = await getObjectAsyncStorage(key)
    if (data == undefined) {
        data = await get_Premium_Features()
        setObjectAsyncStorage(key, data)
    }
    return data;
}

export const set_Premium_Features_Firebase = async (features: any) => {
    let key = `premium`;
    let data: any = await getObjectAsyncStorage(key)
    if (data == undefined) {
        data = await get_Premium_Features();
        setObjectAsyncStorage(key, data, 1)
    }
    Object.entries(features).map(([keyFeature, valueFeature]) => {
        data[keyFeature] = valueFeature
    })
    set_Premium_Features(data);
    setObjectAsyncStorage(key, data)
}

export const getPesoDiaFirebase = async (email?: string) => {
    let key = `peso_dia_` + (email ? email : Estructura_Global.email);
    let data = await getObjectAsyncStorage(key)
    if (data == undefined) {
        data = await getPesoDia(email);
        setObjectAsyncStorage(key, data, 1)
    }
    return data;
}

export const getPesoSemanaFirebase = async (email?: string) => {
    let key = `peso_semana` + (email ? email : Estructura_Global.email);
    let data = await getObjectAsyncStorage(key)
    if (data == undefined) {
        data = await getPesoSemana(email);
        setObjectAsyncStorage(key, data, 1)
    }
    return data;
}


export const getPesoMesFirebase = async (email?: string) => {
    let key = `peso_mes` + (email ? email : Estructura_Global.email);
    let data = await getObjectAsyncStorage(key)
    if (data == undefined) {
        data = await getPesoMes(email);
        setObjectAsyncStorage(key, data)
    }
    return data;
}

export const updatePesoFirebase = async (peso: number, context: Context) => {

    await updateValoresNutricionalesRecomendados(peso);
    await setPesoDia(peso);
    await setPesoSemana(peso);
    await setPesoMes(peso)

    let key = `peso_dia`;
    let data = await getPesoDia()
    setObjectAsyncStorage(key, data);

    key = `peso_semana`;
    data = await getPesoSemana()

    setObjectAsyncStorage(key, data);

    key = `peso_mes`;
    data = await getPesoMes()
    setObjectAsyncStorage(key, data);

    key = `valores-nutricionales-recomendados`;
    data = await getValoresNutricionalesRecomendados()
    setObjectAsyncStorage(key, data);

    key = `personas`;
    data = await getPersonas();
    setObjectAsyncStorage(key, data);
    context.set_lista_personas(context, data)


}
export const get_Bitecoins_Firebase = async () => {
    const data = await getBitecoins();
    return data;
}


export const get_pending_Bitecoins_Firebase = async () => {
    const data = await get_pending_Bitecoins();
    return data;
}

export const substract_Bitecoins_Firebase = async (numberCoins: any) => {
    const coins = await get_Bitecoins_Firebase();
    await set_Bitecoins(coins - numberCoins > 0 ? coins - numberCoins : 0);
}


export const add_Bitecoins_Firebase = async (numberCoins: any) => {
    const coins = await get_Bitecoins_Firebase();
    await set_Bitecoins(coins + numberCoins);
}


export const get_Premium_Features_Prices_Firebase = async () => {
    let data = await get_Premium_Features_Prices()
    return data;
}

const getCost = async (listaCompra: any[]) => {
    const data: any = await getCurrentPlace();
    const current_place = data.current_place;
    let cost = 0;
    const hash = await JSHash(JSON.stringify(listaCompra), CONSTANTS.HashAlgorithms.keccak)
    await Promise.all(listaCompra.map(async (ingredient: Ingrediente) => {
        const conversedQuantity = ingredient.tipo_cantidad == 'g' || ingredient.tipo_cantidad == 'ml'
            ? ingredient.cantidad
            : ingredient.cantidad * ingredient.conversion_ud_g
        cost += conversedQuantity * await getCostIngredient(ingredient.id_ingrediente.toString(), current_place)
    }))
    return { cost, hash }
}

export const getCostIngredientsList = async () => {
    const listaCompra = await get_lista_compra();
    let hashPassword = await JSHash(JSON.stringify(listaCompra), CONSTANTS.HashAlgorithms.keccak)

    let key = `cost-ingredientes-lista-compra`;
    let data = await getObjectAsyncStorage(key)
    if (data == undefined || data.hash != hashPassword) {
        data = await getCost(listaCompra);
        //setObjectAsyncStorage(key, data)
    }
    return data.cost;
}

export const setCostIngredientPlaceFirebase = async (id: string, typePlace: string, place: string, cost: number) => {
    await setCostIngredientPlace(id, typePlace, place, cost)
}

export const setPendingPlanningFirebase = async (email: string, planning: any) => {
    await setPendingPlanning(email, planning)
}

export const setPlaceFirebase = async (placeName: Place, language: Language, coordinatesPlace: CoordinatesPlace) => {
    await setPlace(placeName, language, coordinatesPlace)
}



export const get_Notifications_Firebase = async () => {
    let key = `notifications`;
    let data = await getObjectAsyncStorage(key)
    if (data == undefined) {
        data = await get_Notifications();
        setObjectAsyncStorage(key, data)
    }
    return data;
}
export const deleteNotificationFirebase = async (time: string) => {
    await deleteNotification(time)

    let key = `notifications`;
    const data = await get_Notifications();
    setObjectAsyncStorage(key, data)
}

export const getNutricionistasFirebase = async () => {
    let key = `nutricionistas`;
    let data = await getObjectAsyncStorage(key)
    if (data == undefined) {
        data = await get_Nutricionistas();
        setObjectAsyncStorage(key, data)
    }
    return data;
}

export const getPlacesFirebase = async (type: string) => {
    return getPlaces(type)
}
export const set_Monthly_Premium_Firebase = async (withNutricionista?: boolean) => {
    const nextMonth = moment().add(31, 'days').format(formatMoment)
    const features: any = {
        objetivo_peso: nextMonth,
        seguimiento: nextMonth,
        varias_personas: nextMonth,
    }
    if (withNutricionista) {
        features.nutricionista = nextMonth;
    }
    let key = `premium`;
    let data: any = await getObjectAsyncStorage(key)
    if (data == undefined) {
        data = await get_Premium_Features();
        setObjectAsyncStorage(key, data, 1)
    }
    Object.entries(features).map(([keyFeature, valueFeature]) => {
        data[keyFeature] = valueFeature
    })
    set_Premium_Features(data);
    setObjectAsyncStorage(key, data)

}
export const set_Nutricionist_Firebase = (nutricionist_email: string) => {
    set_Premium_Features_Firebase({ nutricionist_email })
    addClientNutricionist(nutricionist_email);
}


export const getCreditCardToken = (cardData: any) => {

    const card: any = {
        'card[name]': cardData.values.name,
        'card[number]': cardData.values.number.replace(/ /g, ''),
        'card[exp_month]': cardData.values.expiry.split('/')[0],
        'card[exp_year]': cardData.values.expiry.split('/')[1],
        'card[cvc]': cardData.values.cvc
    };
    return fetch('https://api.stripe.com/v1/tokens', {
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
            Authorization: `Bearer ${STRIPE_PUBLISHABLE_KEY}`
        },
        method: 'post',
        body: Object.keys(card)
            .map(key => key + '=' + card[key])
            .join('&')
    })
        .then(response => response.json());
};

export const getCreditCardDataFirebase = async () => {
    const key = 'credit_card'
    let data = await getObjectAsyncStorage(key)
    return data;
}

export const setCreditCardDataFirebase = async (cardData: any) => {
    const key = 'credit_card'
    let data = await setObjectAsyncStorage(key, cardData, 10000)
}

export const doPayment = async (amount: number) => {
    const cardData = await getCreditCardDataFirebase()
    if (cardData == undefined) return false;
    let token: any = {};
    try {
        token = await getCreditCardToken(cardData);
        console.log("token", token)
        if (token.error) {
            return false;
        }
        const charge: any = await apiCallGet("payWithStripe", {
            amount,
            currency: "eur",
            token,
            email: Estructura_Global.email,
        })
        console.log("charge", charge)
        if (charge.status === 'succeeded' && charge.paid) {
            addInfoPayment(amount);
            return true;
        }
        else {
            return false
        }
    } catch (e) {
        console.log("doPayment", e)
        return false;
    }
}


export const getVerifiedDataFirebase = async (typeData: string, name: string) => {
    return await getVerifiedData(typeData, name);
}

export const getDataFirebase = async (typeData: string, name: string) => {
    return await getData(typeData, name);
}

export const getIsAdminFirebase = async () => {
    return await getIsAdmin();
}

export const getCollectionDataFirebase = async (typeData: string) => {
    return await getCollectionData(typeData)
}


export const validateDataFirebase = async (typeData: string, id: string, senderData: any) => {
    console.log("senderData", senderData)
    id = id.toLocaleLowerCase();
    console.log("addUploadedData************************")
    await addUploadedData(typeData, id, senderData);
    console.log("validateData************************")
    await validateData(typeData, id, senderData);
    console.log("removeUnvalidatedData************************")
    await removeUnvalidatedData(typeData, id, senderData);
    console.log("updateValidateBitecoins************************")
    await updateValidateBitecoins(senderData);
}

export const getPersonalizedUrlDataFirebase = async (typeData: string, withTranslation: boolean, onlyName: boolean) => {
    return await getPersonalizedUrlData(typeData, withTranslation, onlyName)
}


export const getLocalRecipesFirebase = async () => {
    let key = `local_recipes`;
    let data = await getObjectAsyncStorage(key)
    if (data == undefined) {
        data = await getLocalRecipes()
        setObjectAsyncStorage(key, data)
    }
    return data;
}

export const addLocalRecipeFirebase = async (senderData: any, id: string, totalBitecoins: any) => {
    id = id.toLocaleLowerCase();
    await addLocalRecipe(senderData, id, totalBitecoins)

    let key = `local_recipes`;
    const data = await getLocalRecipes()
    setObjectAsyncStorage(key, data)
}

export const getImageRecipe = async (receta: Receta) => {
    try {
        const data = await firebase.firebase.storage().ref().child(receta.imagen).getDownloadURL()
        if (data == undefined) throw '';
        console.log("data", data)
        return data;
    } catch (error) {
        console.log("getImageRecipe", error, "result:", await firebase.firebase.storage().ref().child('/imagen_recetas/default.jpg').getDownloadURL())
        return await firebase.firebase.storage().ref().child('/imagen_recetas/default.jpg').getDownloadURL()
    }
}

export const getImageIngredient = async (ingredient: Ingrediente) => {
    try {
        const data = await firebase.firebase.storage().ref().child(ingredient.image ? ingredient.image : "").getDownloadURL()
        if (data == undefined) throw '';
        return data;
    } catch (error) {
        console.log("getImageRecipe", error, "result:", await firebase.firebase.storage().ref().child('/imagen_recetas/default.jpg').getDownloadURL())
        return await firebase.firebase.storage().ref().child('/imagen_recetas/default.jpg').getDownloadURL()
    }
}