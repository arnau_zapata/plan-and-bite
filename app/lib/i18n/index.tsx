import i18n from 'i18next'
import { initReactI18next } from 'react-i18next';
import * as RNLocalize from "react-native-localize";
import spanishResources from '../../locales/es.json';
import englishResources from '../../locales/en.json';

const defaultNs = "translation"
const fallbackLng = 'es'

const localizeLanguage = () => {
    const locales = RNLocalize.getLocales();
    if (Array.isArray(locales)) {        
        return locales[0].languageTag;
    }
    return fallbackLng
}

i18n
    .use(initReactI18next)    
    .init({
        debug: false,        
        lng: localizeLanguage(),
        fallbackLng: fallbackLng,  
        whitelist:['es', 'en'],     
        defaultNS: defaultNs,
        interpolation: {
            escapeValue: false,
        },
        react: {
            wait: true,
        },
    })

i18n.addResourceBundle('es', defaultNs, spanishResources)
i18n.addResourceBundle('en', defaultNs, englishResources)

export default i18n
