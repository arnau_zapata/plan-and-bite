//import AppNavigator from './AppNavigator';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import BottomNavigator, { AccessStack, TabsBottom , IndependentScreens,} from './bottom_navigator';

export default createAppContainer(createSwitchNavigator(
    {
        
        //BottomNavigator,
        TabsBottom, 
        AccessStack,
        IndependentScreens,
        
    },{
        initialRouteName: 'AccessStack',
    }
))