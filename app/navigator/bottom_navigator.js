import React from 'react';



//Screens


import AccessScren from '../screens/Access';
import LoginScreen from '../screens/login';
import RegistroScreen from '../screens/Registro'
import RecetaScreen from '../screens/Receta';
import CalendarioScreen from '../screens/Calendario';
import MiNeveraScreen from '../screens/Nevera';
import LaCompraScreen from '../screens/ListaCompra'
import LogOutScreen from '../screens/LogOut';
import AnadirRecetaScreen from '../screens/AnadirReceta';
import PlanningManualScreen from '../screens/Calendario';
import NeveraAddScreen from '../screens/Nevera/components/NeveraAdd';
import NeveraEditScreen from '../screens/Nevera/components/NeveraEdit';
import BuyPremiumScreen from '../screens/BuyPremium';
import BuyBiteCoinsScreen from '../screens/BuyBiteCoins';
import PremiumScreen from '../screens/Premium';
import FreeBitecoinsScreen from '../screens/Premium/components/FreeBitecoins';
import PerfilAnadirPersonaScreen from '../screens/Perfil';
import SeguimientoScreen from '../screens/Seguimiento';
import CuentaScreen from '../screens/Cuenta';
import ListaUsuariosScreen from '../screens/Cuenta/components/ListaUsuarios';
import NotificacionesScreen from '../screens/Notificaciones';
import NutricionistasScreen from '../screens/Nutricionistas';
import LoadingScreen from '../screens/Loading';
import pagosScreen from '../screens/Pagos';
import AddDataScreen from '../screens/AddData';
import ValidateDataScreen from '../screens/ValidateData';
import ConfiguracionScreen from '../screens/Configuracion';
import { createStackNavigator } from 'react-navigation-stack';

// =================== Bottom Tab Navigator ===================
import { createBottomTabNavigator } from 'react-navigation-tabs';

// =================== Top Tab Navigator ===================
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';


//Icons
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';

//Custom Icons
import ToolBarIcon from '../assets/ToolBarCalendario_02.png';


// =================== Navigation stacks ===================

//2. Calendario Stack
const CalendarioStack = createStackNavigator({
    Calendario: {
        screen: CalendarioScreen,
    }
},//{headerLayoutPreset: 'center'}
    { headerMode: 'none' }
);

//3. MiNevera Stack
export const MiNeveraStack = createStackNavigator({
    MiNevera: {
        screen: MiNeveraScreen,
    },
},

    //{headerLayoutPreset: 'center'},
    { headerMode: 'none' });


//4. LaCompra Stack
const LaCompraStack = createStackNavigator({
    LaCompra: {
        screen: LaCompraScreen,
        navigationOptions: {
            /*tabBarLabel: 'Semana',
            title: 'Home',*/
        }
    }
}, { headerMode: 'none' },
    //{headerLayoutPreset: 'center'}
);

//5. LaCompra Stack
const LogOutStack = createStackNavigator({
    LogOut: {
        screen: LogOutScreen,
        navigationOptions: {
            tabBarLabel: 'Log Out',
            title: 'Log Out',
            //header: null
        }
    }
}, { headerMode: 'none' });

//Independent Buttons
export const AnadirRecetaStack = createStackNavigator({
    AnadirReceta: {
        screen: AnadirRecetaScreen,
        navigationOptions: {
            tabBarLabel: 'Receta',
            title: 'Receta',

        }
    }
});


export const NeveraAddStack = createStackNavigator({ NeverAdd: { screen: NeveraAddScreen } }, { title: 'Añadir' });
export const NeveraEditStack = createStackNavigator({ NeveraEdit: { screen: NeveraEditScreen } });
export const PlanningManualStack = createStackNavigator({ PlanningManual: { screen: PlanningManualScreen } });
export const RecetaStack = createStackNavigator({ Receta: { screen: RecetaScreen } });
export const PremiumStack = createStackNavigator({ Premium: { screen: BuyPremiumScreen } }, { headerMode: 'none' });
export const CuentaStack = createStackNavigator({ Cuenta: { screen: CuentaScreen } }, { headerMode: 'none' });
export const PerfilAnadirPersonaStack = createStackNavigator({ PerfilAnadirPersona: { screen: PerfilAnadirPersonaScreen } }, { headerMode: 'none' });

//exports a stack that includes 2 screens - (Login and Registro in this case)
export const AccessStack = createStackNavigator({
    Access: { screen: AccessScren },
    Login: { screen: LoginScreen },
    Registro: { screen: RegistroScreen }
}, { headerMode: 'none' });

//const nav = createBottomTabNavigator(
export default createBottomTabNavigator(
    {
        Calendario: CalendarioStack,
        Nevera: MiNeveraStack,
        Semana: LaCompraStack,
        LogOut: LogOutStack,

    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                const { routeName } = navigation.state;
                let iconName =
                    routeName == 'Calendario' ? 'ios-home' :
                        routeName == 'Nevera' ? 'ios-settings' :
                            routeName == 'Semana' ? 'ios-settings' :
                                routeName == 'LogOut' ? 'ios-log-out' : null;


                // You can return any component that you like here!
                return <Icon name={iconName} size={25} color={tintColor} />;
            },

        }),

        tabBarOptions: {
            activeTintColor: 'orange',
            inactiveTintColor: 'white',
            style: {
                backgroundColor: 'black'
            },
            initialRouteName: 'Login'
        },
    }
)

//export default createAppContainer(nav);

/* ===================  Working Bottom Tab Navigator Test by Robert Font ===================

export const nav = createMaterialBottomTabNavigator({
    Home: {
        screen: HomeScreen,
    },
    Settings: { screen: SettingsScreen }
}, {
    initialRouteName: 'Home',
    order: ['Home', 'Settings'],
    activeColor: 'orange',
}) 
export default createAppContainer(nav); */



// ==================================== TEST =========================
export const TabsBottom = createBottomTabNavigator(
    {
        Calendario: CalendarioStack,
        Nevera: MiNeveraStack,
        Semana: LaCompraStack,
        Cuenta: CuentaStack,

    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                const { routeName } = navigation.state;
                let iconName =
                    routeName == 'Calendario' ? 'calendar-check' :
                        routeName == 'Nevera' ? 'fridge' :
                            routeName == 'Semana' ? 'view-week' :
                                routeName == 'Cuenta' ? 'user-circle' : null;
                routeName == 'LogOut' ? 'logout' : null;


                // You can return any component that you like here!
                return <Icon2 name={iconName} size={24} color={tintColor} />;
            },


        }),

        tabBarOptions: {
            activeTintColor: '#6495ed',
            inactiveTintColor: 'black',
            style: {
                backgroundColor: 'White',
                innerHeight: 26,
                outerHeight: 20
            },
            initialRouteName: 'Calendario',

        },
    }
)

export const IndependentScreens = createStackNavigator({
    AddData: { screen: AddDataScreen },
    Loading: { screen: LoadingScreen },
    FreeBitecoins: { screen: FreeBitecoinsScreen },
    ValidateData: { screen: ValidateDataScreen },
    Configuracion: { screen: ConfiguracionScreen },
    AnadirReceta: { screen: AnadirRecetaScreen },
    NeveraAdd: { screen: NeveraAddScreen },
    NeveraEdit: { screen: NeveraEditScreen },
    PlanningManual: { screen: PlanningManualScreen },
    Seguimiento: { screen: SeguimientoScreen },
    Receta: { screen: RecetaScreen },
    BuyBiteCoins: { screen: BuyBiteCoinsScreen },
    Notificaciones: { screen: NotificacionesScreen },
    Nutricionistas: { screen: NutricionistasScreen },
    BuyPremium: { screen: BuyPremiumScreen },
    Premium: { screen: PremiumScreen },
    Perfil: { screen: PerfilAnadirPersonaScreen },
    Pagos: { screen: pagosScreen },
    TabsBottom: {
        screen: TabsBottom
    },
    ListaUsuarios: { screen: ListaUsuariosScreen }
    //defaultNavigationOptions: ({ navigation }) => ({    title: `Atrás`,  }),
}, {
    initialRouteName: "TabsBottom",
    headerMode: 'none',
});






