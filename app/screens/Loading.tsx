import React, { useRef, useState, useEffect, useContext, } from "react";
import { AppContext } from '../provider/provider';
import { StyleSheet, Text, ImageBackground, View, Dimensions } from "react-native";

import Loading from "../components/Loading";
import { initializate } from "./Nutricionistas/functions/functions";
import { Button } from "react-native-elements";

var s = require('../style');




export default (props: any) => {
    const [loading, setLoading] = useState(true)
    let text = props.navigation.getParam('text')
    let func = props.navigation.getParam('function')
    let textDone = props.navigation.getParam('textDone')
    let nextScreen = props.navigation.getParam('nextScreen')

    const initializate = async () => {
        try {
            await func;
            setLoading(false)
            props.navigation.navigate(nextScreen)
        } catch (e) {
            console.log("error on Loading", e)
            textDone = "Ha habido un error"
            setLoading(false)
        }
    }
    useEffect(() => {
        initializate()
    })

    return (
        <ImageBackground style={{ height: Dimensions.get('window').height, alignItems: 'center' }} source={require('../assets/Screen_AcessStack_00.png')}>

            <Text style={{ color: 'white' }}>{loading ? text : textDone}</Text>
            {loading ?
                <Loading />
                : <Button title='aceptar' onPress={() => { props.navigation.navigate(nextScreen) }} />
            }
        </ImageBackground>
    )
}


