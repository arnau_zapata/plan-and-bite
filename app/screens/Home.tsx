import React, { useRef, useState, Component, useContext } from 'react';
import { StyleSheet, Image, Text, View, Input, TextInput, ImageBackground, RadioButton, TouchableOpacity, KeyboardAvoidingView, ScrollView } from 'react-native';
import RadioForm, { RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import Estructura_Global from '../Estructuras/Estructura_Global';
var s = require('../style');
import { createAppContainer, StackActions, NavigationActions } from 'react-navigation'; // Version can be specified in package.json
import { createStackNavigator } from 'react-navigation-stack'
import Demo from '../assets/demoImg.jpg';
//import { ScrollView } from 'react-native-gesture-handler';
import { AppContext } from '../provider/provider'
import { Header } from 'react-navigation-stack'; //<================== NO BORRAR, ES SUPER IMPORTANTE PARA QUE FUNCIONE EL KeyboardAvoidingView
import logImg from '../assets/logImg.png';


const Home = props => {



  return (

    <KeyboardAvoidingView
      keyboardVerticalOffset={Header.HEIGHT + 40} // adjust the value here if you need more padding
      style={{ flex: 1 }}
      behavior="padding" >
      <ScrollView>
        <View style={[{ margin: 0 }]}>
          <ImageBackground source={Demo} style={{ width: 400, height: 220, display: 'flex', textAlignVertical: 'flex-end', justifyContent: 'flex-end' }}>
            <Image source={Demo} style={[s.tinte_gray, { width: 400, height: 75 }]} />
            <Text style={[s.fuente_M, s.padding_M, { fontWeight: 'bold', position: 'absolute', color: 'white' }]}>asdf </Text>
            <Text style={[s.fuente_S, s.horizontal_padding_XL, { fontWeight: 'bold', position: 'absolute', color: 'white', paddingBottom: 5 }]}>zxcv </Text>
          </ImageBackground>
          <ImageBackground source={Demo} style={{ width: 400, height: 220, display: 'flex', textAlignVertical: 'flex-end', justifyContent: 'flex-end' }}>
            <Image source={Demo} style={[s.tinte_gray, { width: 400, height: 75 }]} />
            <Text style={[s.fuente_M, s.padding_M, { fontWeight: 'bold', position: 'absolute', color: 'white' }]}>asdf </Text>
            <Text style={[s.fuente_S, s.horizontal_padding_XL, { fontWeight: 'bold', position: 'absolute', color: 'white', paddingBottom: 5 }]}>zxcv </Text>
          </ImageBackground>
          <ImageBackground source={Demo} style={{ width: 400, height: 220, display: 'flex', textAlignVertical: 'flex-end', justifyContent: 'flex-end' }}>
            <Image source={Demo} style={[s.tinte_gray, { width: 400, height: 75 }]} />
            <Text style={[s.fuente_M, s.padding_M, { fontWeight: 'bold', position: 'absolute', color: 'white' }]}>asdf </Text>
            <Text style={[s.fuente_S, s.horizontal_padding_XL, { fontWeight: 'bold', position: 'absolute', color: 'white', paddingBottom: 5 }]}>zxcv </Text>
          </ImageBackground>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>

  );
}

export default Home;


