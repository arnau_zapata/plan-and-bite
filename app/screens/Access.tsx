import React, { useRef, useState, useEffect, useContext, } from "react";
import { AppContext } from '../provider/provider';
import { StyleSheet, Text, ImageBackground, View, Dimensions } from "react-native";

import axios from 'axios';
import { getModificationRef, getPlanningMenu, IsEmailAndPasswordValid } from '../storage/firebase'
import Estructura_Global from '../Estructuras/Estructura_Global.json'
import {
    clearAllData,
    getEmailUserFirebase,
    actualizar_nevera,
    getListPersonas,
    getUserFirebase,
    setCurrentPlaceFirebase,
} from '../storage/storage';
import { AsyncStorage } from 'react-native';
import { Context } from "../type/type";
import Loading from "../components/Loading";

var s = require('../style');




export default (props: any) => {
    const context: any = useContext(AppContext)
    const [componentLoaded, setComponentLoaded] = useState("")

    const inicializar_Usuario = async () => {
        setComponentLoaded("actualizando nevera")
        await actualizar_nevera(context);

        await context.updateFromFirebase(true, setComponentLoaded)
    }

    const init = async () => {
        console.log("*********************")
        Estructura_Global.email = await getEmailUserFirebase()

        if (Estructura_Global.email && Estructura_Global.email != "") {
            await inicializar_Usuario();
            const list_personas: any[] = await getListPersonas()
            props.navigation.navigate(list_personas.length != 0 ? 'TabsBottom' : "Perfil");
            console.log("email", Estructura_Global.email)
        } else {
            props.navigation.navigate('Login');
        }
    }


    useEffect(() => {
        init()
    }, [])

    return (
        <ImageBackground style={{ height: Dimensions.get('window').height, alignItems: 'center' }} source={require('../assets/Screen_AcessStack_00.png')}>

            <Text style={{ color: 'white' }}>loading...</Text>
            <Loading />
            <Text style={{ color: 'white' }}>{componentLoaded}</Text>
        </ImageBackground>
    )
}


