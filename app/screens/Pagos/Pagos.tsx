import React, { useState, useEffect } from "react";
import { Text, View, Image, TouchableOpacity, Dimensions } from "react-native";
import { Button, CheckBox, ButtonGroup } from 'react-native-elements';
import { useTranslation } from 'react-i18next'

import { CreditCardInput, IInputPlaceholders } from 'react-native-input-credit-card';
import { submit } from "./functions/functions";
import { getCreditCardDataFirebase } from "../../storage/storage";


let cardInput: any;

const Cuenta = (props: any) => {
    const { t } = useTranslation();
    const [cardData, setCardData] = useState<any>({})
    const [errors, setErrors] = useState("")
    const initializate = async () => {
        const cardData: any = await getCreditCardDataFirebase()
        console.log("cardData",cardData)
        setCardData(cardData ? cardData : {})
        if (cardInput && cardData && cardData.values) {
            cardInput.setValues(cardData.values)
        }
    }

    useEffect(() => {
        initializate()
    }, [])



    return (
        <View style={{ justifyContent: 'space-between', height: Dimensions.get('window').height - 30, padding: 15, paddingTop: 50 }}>
            <CreditCardInput
                requiresName
                // cardImageBack={card_back}
                // cardImageFront={card_front}
                onChange={(cardData: any) => {
                    setCardData(cardData);
                }}
                ref={(input: any) => {
                    cardInput = input
                }}
                // onFocus={(field) => { onChangeFocus(field) }}
                additionalInputsProps={
                    {
                        name: {
                            color: 'white',
                            returnKeyType: 'next',
                            onSubmitEditing: () => { cardInput.focus("number") }
                        },
                    }
                }
            />
            <View>
                <Button title='save' onPress={() => submit(cardData, setErrors, props.navigation)} />
                <Text style={{ color: 'red' }}>{errors}</Text>
            </View>
        </View>
    )
}

export default Cuenta;
