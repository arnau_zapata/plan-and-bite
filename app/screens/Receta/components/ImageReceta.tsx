import React, { useRef, useState, Component, useContext } from 'react';
import { StyleSheet, Image, Text, View, TextInput, TouchableOpacity, KeyboardAvoidingView, ImageBackground } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';


type Props = {
    sourceImage: any,
    title: string
}

const ImageReceta = (props: Props) => {
    return (
        <View style={{ marginTop: 20 }}>
            <ImageBackground style={[{ margin: 10, width: '100%', height: 200, alignItems: "center", justifyContent: 'flex-end' }]} source={props.sourceImage}>
                <LinearGradient
                    style={{ width: '100%', height: 75 }}
                    colors={['rgba(0,0,0,0)', 'rgba(128,128,128,0.7)', 'gray']}
                />
                <Text style={{ fontSize: 25, fontWeight: 'bold', position: 'absolute', color: 'white', textAlign: 'center', paddingBottom: 25 }}>{props.title} </Text>
            </ImageBackground>
        </View>

    );
}

export default ImageReceta;


