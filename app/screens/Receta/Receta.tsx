import React, { useRef, useState, Component, useContext, useEffect } from 'react';
import { StyleSheet, Image, Text, View, TextInput, TouchableOpacity, Dimensions } from 'react-native';
var s = require('../../style');
import { ScrollView } from 'react-native-gesture-handler';

import { comida_to_integer } from '../../Utils/utils_calendario'
import { anadir_receta_a_firestore, eliminar_receta_a_firestore } from '../../Utils/utils_planning'

import Clock from '../../assets/Clock.png';
import SafeAreaView from 'react-native-safe-area-view';
import flecha_izquierda from '../../assets/icon_flecha-izquierda.png'
import { Header } from 'react-native-elements';
import { NavigationInjectedProps } from 'react-navigation'
import ImageReceta from './components/ImageReceta'
import { Icon } from 'react-native-elements';
import { useTranslation } from 'react-i18next'
import { AppContext } from '../../provider/provider';
import { Context } from '../../type/type';
import Loading from '../../components/Loading';
import { translate } from '../../Utils/utils';
import { getImageRecipe } from '../../storage/storage';
interface Props extends NavigationInjectedProps { };
const Receta = (props: Props) => {
  const context: any = useContext(AppContext)
  const { t } = useTranslation();
  let last_screen = props.navigation.getParam('last_screen')
  let receta_seleccionada = props.navigation.getParam('receta_seleccionada')
  let month = props.navigation.getParam('month')
  let day = props.navigation.getParam('day')
  let comida = props.navigation.getParam('comida')
  let plato = props.navigation.getParam('plato')
  let year = props.navigation.getParam('year')
  let quantity = props.navigation.getParam('quantity') ? props.navigation.getParam('quantity') : 1
  let warning = props.navigation.getParam('warning')
  const [state, setState] = useState<any>({ receta: {}, loading: true, })


  const init = async () => {
    let receta = receta_seleccionada;
    if (receta.imagen) {
      const imagen_url = await getImageRecipe(receta);
      receta=  { ...receta, imagen_url }
    }
    setState({ receta, loading: false })
  }

  useEffect(() => {
    init();
  }, [])
  const getView_Lista_Ingredientes_Receta = () => {
    let views: any[] = [];
    state.receta.lista_ingredientes.forEach((ingrediente: any) => {
      views.push(
        <View style={[s.row, s.textTabulation]}>
          <Text>{translate(ingrediente.nombre)} :</Text>
          <Text>{ingrediente.cantidad * quantity} </Text>
          <Text>{ingrediente.tipo_cantidad} </Text>
        </View>
      );
    })
    return (<View>{views}</View>);
  }

  const getView_Lista_Valor_Nutricional_Receta = () => {
    let views = [];
    let keys = Object.keys(state.receta.valores_nutricionales)
    let values = Object.values(state.receta.valores_nutricionales)
    for (let i = 0; i < keys.length; i++) {
      views.push(
        <View style={[s.row, s.textTabulation]}>
          <Text>{keys[i]}: </Text>
          <Text>{values[i]} </Text>
          <Text>{keys[i] == "kcal" ? "kcal" : 'g'} </Text>
        </View>
      );
    }
    return (<View>{views}</View>);
  }

  const getView_Bottones = () => {
    let view_buttons;
    if (last_screen == 'Calendario') {
      view_buttons = (
        <View style={[s.row, { width: '100%' }]}>
          <TouchableOpacity
            style={[s.background_blue, { borderWidth: 1, width: '50%', padding: 10, alignItems: 'center' }]}
            onPress={() => {
              props.navigation.navigate({ routeName: 'AnadirReceta' });
            }}
          >
            <Text style={{ color: 'white' }}>Editar</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[s.background_blue, { borderWidth: 1, width: '50%', padding: 10, alignItems: 'center' }]}
            onPress={async () => {
              console.log(year, month, day, comida_to_integer(comida), plato)
              await eliminar_receta_a_firestore(year, month, day, comida_to_integer(comida), plato, context);
              props.navigation.navigate('AccessStack');
              props.navigation.navigate('TabsBottom');
            }}
          >
            <Text style={{ color: 'white' }}>Eliminar</Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      view_buttons = (
        <TouchableOpacity
          style={[s.background_blue, { borderWidth: 1, width: '100%', padding: 10, alignItems: 'center' }]}
          onPress={() => {
            anadir_receta_a_firestore(state.receta.id, year, month, day, comida, plato, context);
            if (state.receta != undefined) {
              props.navigation.navigate({ routeName: 'AccessStack' });
              props.navigation.navigate({ routeName: 'TabsBottom' });
            }
          }}
        >
          <Text style={{ color: 'white' }}>Añadir Receta</Text>
        </TouchableOpacity>
      );
    }
    return view_buttons;
  }
  console.log("state.receta",state.receta)

  const CustomButton = (props: any) =>
    <TouchableOpacity onPress={() => {

      props.navigation.navigate('TabsBottom')
    }} >
      <Image source={flecha_izquierda} style={[s.img_S, { backgroundColor: 'rgba(250,250,250,0.5)', borderWidth: 1 }]} />
    </TouchableOpacity>
  return (
    <View>
      <SafeAreaView>
        <Header
          placement="center"
          leftComponent={CustomButton(props)}
          //leftComponent= {{ icon: 'menu' ,  color: '#fff' }}
          centerComponent={{ text: 'Añadir Receta', style: { color: '#fff' } }}
        // rightComponent={CustomButton(this.props)}
        //rightComponent={{ icon: 'home', color: '#fff' }}
        />
        <View style={[{ margin: 10, justifyContent: 'space-between' }]}>

          <ScrollView style={{ height: Dimensions.get('window').height - 170, }}>
            {state.loading ?
              <Loading />
              :
              <>
                <ImageReceta title={state.receta.nombre} sourceImage={{ uri: state.receta.imagen_url }} />
                <View style={{ paddingHorizontal: 50 }}>
                  <View style={[s.row, { marginVertical: 10 }]}>
                    <Image source={Clock} style={s.img_XS} />
                    <Text>25min</Text>
                  </View>
                  <View style={[s.textReceta, s.basicPadding]}>
                    <Text style={[s.fuente_M, { fontWeight: 'bold' }]}>{t('receta.principal.ingredients')}</Text>
                    {getView_Lista_Ingredientes_Receta()}
                  </View>
                  {state.receta.cost != "" && state.receta.cost != undefined && <View style={[s.textReceta, s.basicPadding]}>
                    <Text style={[s.fuente_M, { fontWeight: 'bold' }]}>{t('receta.principal.cost')}:</Text>
                    <Text style={s.textTabulation}>{state.receta.cost} €</Text>
                  </View>}
                  {state.receta.preparacion != "" && state.receta.preparacion != undefined && <View style={[s.textReceta, s.basicPadding]}>
                    <Text style={[s.fuente_M, { fontWeight: 'bold' }]}>{t('receta.principal.preparation')}:</Text>
                    <Text style={s.textTabulation}>{state.receta.preparacion}</Text>
                  </View>}
                  <View style={[s.textReceta, s.basicPadding]}>
                    <Text style={[s.fuente_M, { fontWeight: 'bold' }]}>{t('receta.principal.nutritional_value')}</Text>
                    {getView_Lista_Valor_Nutricional_Receta()}
                  </View>
                </View>
              </>
            }
          </ScrollView>
          {warning && <View style={{ flexDirection: 'row' }}><Icon name='warning' /><Text>{t('receta.principal.ingredients_missing')}</Text></View>}
          <View style={{ width: '100%', marginBottom: 150 }}>
            {getView_Bottones()}
          </View>
        </View>
      </SafeAreaView>
    </View>
  );
}

export default Receta;


