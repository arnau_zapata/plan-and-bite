import React, { useRef, useState, Component, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { CheckBox, Slider, Button, Image } from 'react-native-elements'
import {
    set_Premium_Features_Firebase,
    get_Premium_Features_Firebase,
    get_Bitecoins_Firebase,
    get_Premium_Features_Prices_Firebase,
    set_Monthly_Premium_Firebase, doPayment, get_pending_Bitecoins_Firebase
} from '../../storage/storage';
import BiteCoin from '../../assets/bitecoin.png'
import { NavigationInjectedProps } from 'react-navigation'
import FeaturePremium from './components/FeaturePremium'
import ModalFeature from './components/ModalFeature'
import moment from 'moment'
import CONSTANT from '../../local/constant'
import TapButton from '../../components/TapButton';
import Loading from '../../components/Loading';
import ModalPayment from '../../components/ModalPayment';

interface Props extends NavigationInjectedProps { };

export default (props: Props) => {

    let [checkVariasPersonas, set_checkVariasPersonas] = useState("");
    let [objetivoPeso, set_objetivoPeso] = useState("");
    let [seguimiento, set_seguimiento] = useState("");

    let [reset, set_reset] = useState(true);
    let [tapbuttonState, set_tapbuttonState] = useState({ index: 1, num_days: 31, type: 'month' });
    let [bitecoins, set_bitecoins] = useState(0);
    let [pending_bitecoins, set_pending_bitecoins] = useState(0);
    let [modalFeaturePremium, set_modalFeaturePremium] = useState<any>({ visible: false });
    let [priceFeatures, set_priceFeatures] = useState<any>({});
    const [modalPremium, setModalPremium] = useState<any>({ visible: false, data: undefined })

    const inicializate = async () => {
        const features = await get_Premium_Features_Firebase();
        set_checkVariasPersonas(features.varias_personas)
        set_seguimiento(features.seguimiento)
        set_objetivoPeso(features.objetivo_peso)
        set_bitecoins(await get_Bitecoins_Firebase())
        set_pending_bitecoins(await get_pending_Bitecoins_Firebase())
        set_priceFeatures(await get_Premium_Features_Prices_Firebase())
        set_reset(false);
    }

    useEffect(() => {
        if (reset) {
            inicializate();
        }
    }, [reset]);

    const update_TapbuttonState = (index: number) => {
        let num_days = 1;
        if (index == 0) num_days = 365;
        else if (index == 1) num_days = 31;
        else if (index == 2) num_days = 7;
        else if (index == 3) num_days = 1;


        let type = 'day';
        if (index == 0) type = 'year';
        else if (index == 1) type = 'month';
        else if (index == 2) type = 'week';
        else if (index == 3) type = 'day';
        set_tapbuttonState({ index, num_days, type })
    }

    const getViewFeaturePremiumGroup = () => {
        return (
            <View>
                <FeaturePremium
                    checked={checkVariasPersonas > moment().format(CONSTANT.formatMoment)}
                    expirationDate={checkVariasPersonas}
                    numberCoins={priceFeatures.varias_personas && priceFeatures.varias_personas[tapbuttonState.type] ? priceFeatures.varias_personas[tapbuttonState.type] : 0}
                    title='Planificacion varias personas'
                    onPress={() => {
                        set_modalFeaturePremium({
                            visible: true,
                            price: priceFeatures.varias_personas && priceFeatures.varias_personas[tapbuttonState.type] ? priceFeatures.varias_personas[tapbuttonState.type] : 0,
                            insuficient_coins: bitecoins < (priceFeatures.varias_personas && priceFeatures.varias_personas[tapbuttonState.type] ? priceFeatures.varias_personas[tapbuttonState.type] : 0),
                            description: 'planificacion para varias personas te permite planificar la dieta para varias personas simultaneamente',
                            cancel: checkVariasPersonas > moment().format(CONSTANT.formatMoment),
                            accessor: 'varias_personas',
                            firebase_action: () => { set_Premium_Features_Firebase({ varias_personas: moment().add(tapbuttonState.num_days, 'days').format(CONSTANT.formatMoment) }) }
                        })
                    }}
                />
                <FeaturePremium
                    checked={objetivoPeso > moment().format(CONSTANT.formatMoment)}
                    expirationDate={objetivoPeso}
                    numberCoins={priceFeatures.objetivo_peso && priceFeatures.objetivo_peso[tapbuttonState.type] ? priceFeatures.objetivo_peso[tapbuttonState.type] : 0}
                    title='Objetivo de peso'
                    onPress={() => {
                        set_modalFeaturePremium({
                            visible: true,
                            price: priceFeatures.objetivo_peso && priceFeatures.objetivo_peso[tapbuttonState.type] ? priceFeatures.objetivo_peso[tapbuttonState.type] : 0,
                            insuficient_coins: bitecoins < (priceFeatures.objetivo_peso && priceFeatures.objetivo_peso[tapbuttonState.type] ? priceFeatures.objetivo_peso[tapbuttonState.type] : 0),
                            description: 'Objetivo de peso permite que la planificacion automatica adapte las recetas con el objetivo de conseguir o mantener tu peso ideal',
                            cancel: objetivoPeso > moment().format(CONSTANT.formatMoment),
                            accessor: 'objetivo_peso',
                            firebase_action: () => set_Premium_Features_Firebase({ objetivo_peso: moment().add(tapbuttonState.num_days, 'days').format(CONSTANT.formatMoment) })
                        })
                    }}
                />
                <FeaturePremium
                    checked={seguimiento > moment().format(CONSTANT.formatMoment)}
                    expirationDate={seguimiento}
                    numberCoins={priceFeatures.seguimiento && priceFeatures.seguimiento[tapbuttonState.type] ? priceFeatures.seguimiento[tapbuttonState.type] : 0}
                    title='Seguimiento'
                    onPress={() => {
                        set_modalFeaturePremium({
                            visible: true,
                            price: priceFeatures.seguimiento && priceFeatures.seguimiento[tapbuttonState.type] ? priceFeatures.seguimiento[tapbuttonState.type] : 0,
                            insuficient_coins: bitecoins < (priceFeatures.seguimiento && priceFeatures.seguimiento[tapbuttonState.type] ? priceFeatures.seguimiento[tapbuttonState.type] : 0),
                            description: 'Seguimiento permite ver la evolucion del peso semanal y mensual. Tambien permite ver cuanto se ajustan las dietas a la cantidad recomendada',
                            cancel: seguimiento > moment().format(CONSTANT.formatMoment),
                            accessor: 'seguimiento',
                            firebase_action: () => set_Premium_Features_Firebase({ seguimiento: moment().add(tapbuttonState.num_days, 'days').format(CONSTANT.formatMoment) }),
                        })
                    }}
                />
                <ModalFeature
                    visible={modalFeaturePremium.visible}
                    accessor={modalFeaturePremium.accessor}
                    onPressAccept={() => { modalFeaturePremium.firebase_action(); set_reset(true); set_modalFeaturePremium({ ...modalFeaturePremium, visible: false }) }}
                    onPressCancel={() => { set_modalFeaturePremium({ ...modalFeaturePremium, visible: false }); set_reset(true); }}
                    description={modalFeaturePremium.description}
                    price={modalFeaturePremium.price}
                    cancel={modalFeaturePremium.cancel}
                    insuficient_coins={modalFeaturePremium.insuficient_coins}
                    navigator={props.navigation}
                />
                <ModalPayment
                    visible={modalPremium.visible}
                    closeModal={() => { setModalPremium({ visible: false, data: undefined }) }}
                    amount={modalPremium.data ? modalPremium.data.price : undefined}
                    onPressAccept={async () => {
                        await set_Monthly_Premium_Firebase();
                    }}
                    navigation={props.navigation}
                />
            </View>
        )
    }


    return (
        <View>
            <View style={{ padding: 20, flexDirection: 'row', alignItems: 'center' }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}> {bitecoins ? bitecoins : 0}</Text>
                <Image source={BiteCoin} style={{ height: 25, width: 25 }} />
                <Text style={{ marginLeft: 20 }}>bitecoin/s</Text>
                {pending_bitecoins > 0 ?
                    <View style={{ padding: 20, flexDirection: 'row', alignItems: 'center' }}>
                        <Text>(+</Text>
                        <Text style={{ fontSize: 20, fontWeight: 'bold' }}> {pending_bitecoins ? pending_bitecoins : 0}</Text>
                        <Text style={{marginLeft:10}}>pending)</Text>
                    </View>
                    :
                    <View />
                }
            </View>
            <Button
                title="Consigue Bitecoins Gratis"
                onPress={() => {
                    props.navigation.navigate('FreeBitecoins', { nextScreen: 'AddData' })
                }}
            />
            <TapButton index={tapbuttonState.index} setindex={update_TapbuttonState} tabButtonNames={["Año", "Mes", "Semana", 'Dia']} />
            {priceFeatures != undefined && Object.entries(priceFeatures).length > 0 ? getViewFeaturePremiumGroup() : <Loading />}
            <View style={{ justifyContent: 'center' }}>
                <Button
                    title="Comprar BiteCoins"
                    buttonStyle={{ margin: 20 }}
                    onPress={() => {
                        props.navigation.navigate('BuyBiteCoins')
                    }}
                />
                <Button
                    title="Premium"
                    buttonStyle={{ margin: 20 }}
                    onPress={async () => {
                        const features: any = await get_Premium_Features_Prices_Firebase()
                        const price = features.premium
                        if (price) setModalPremium({ visible: true, data: { price } })
                    }}
                />
            </View>
        </View>
    );
}

