import React, { useRef, useState, Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { Button, Image } from 'react-native-elements'
import BiteCoin from '../../../assets/bitecoin.png'
import ModalPlanNBite from '../../../components/ModalPlanNBite';
import { set_Premium_Features_Firebase, substract_Bitecoins_Firebase, get_Premium_Features_Firebase } from '../../../storage/storage';


type Props = {
    visible: boolean,
    accessor: string,
    description: string,
    price: number,
    cancel: boolean,
    onPressCancel: () => void,
    onPressAccept: () => void,
    insuficient_coins: boolean,
    navigator: any,
}

export default ({ accessor, visible, onPressCancel, onPressAccept, description, price, cancel, insuficient_coins, navigator }: Props) => {

    return (
        <ModalPlanNBite visible={visible} style={{ borderWidth: 10 }} title={cancel ? 'Cancela una feature' : 'Compra una Feature'} onPressCancel={onPressCancel}>

                    <Text>{description}</Text>
                    <TouchableOpacity
                        style={{ borderWidth: 3, marginTop: 10, borderRadius: 10, borderColor: 'green', justifyContent: 'center', padding: 10, flexDirection: 'row', alignItems: 'center' }}
                        onPress={() => {
                            const aux: any = {};
                            aux[accessor] = ""
                            console.log(insuficient_coins)
                            if (cancel) {
                                set_Premium_Features_Firebase(aux);
                                onPressCancel();
                            }
                            else if (insuficient_coins) {
                                onPressCancel();
                                navigator.navigate("BuyBiteCoins")
                            }
                            else {
                                onPressAccept();
                                substract_Bitecoins_Firebase(price);
                            }
                        }}
                    >
                        {(!insuficient_coins || cancel) && <Text>{cancel ? 'Cancelar' : 'Pagar con'} </Text>}
                        {insuficient_coins && !cancel && <Text>comprar Bitecoins</Text>}
                        {!cancel && !insuficient_coins && <Text>{price}</Text>}
                        {!cancel && !insuficient_coins && <Image source={BiteCoin} style={{ height: 25, width: 25 }} />}
                    </TouchableOpacity>

        </ModalPlanNBite>
    );
}

