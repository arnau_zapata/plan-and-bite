import React, { useRef, useState, Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { CheckBox, Image } from 'react-native-elements'
import BiteCoin from '../../../assets/bitecoin.png'

type Props = {
    title: string,
    numberCoins: number,
    checked: boolean,
    onPress: () => void,
    expirationDate: string,
}

export default ({ title, numberCoins, checked, onPress, expirationDate }: Props) => {

    return (
        <View style={{ paddingVertical: 3, backgroundColor: 'rgba(0,0,0,0.05)' }}>
            <TouchableOpacity
                onPress={onPress}
                style={{ padding: 10, backgroundColor: 'white' }}
            >
                <View
                    style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}
                >
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <CheckBox
                            checked={checked}
                            containerStyle={{ padding: 0, paddingVertical: 0 }}
                        />
                        <Text style={{ fontWeight: 'bold' }}>{title}</Text>
                    </View>
                    {checked ?
                        <Text style={{ color: 'red', fontWeight: 'bold', fontSize: 20 }}>X</Text>
                        :
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold' }}> {numberCoins}</Text>
                            <Image source={BiteCoin} style={{ height: 25, width: 25 }} />
                        </View>

                    }
                </View>
                {checked &&
                    <Text>fecha de expiracion: {expirationDate}</Text>
                }
            </TouchableOpacity>
        </View>
    );
}

