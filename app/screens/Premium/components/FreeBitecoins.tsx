import React, { useRef, useState, Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { Button, CheckBox, Image } from 'react-native-elements'

type Props = {
    navigation: any
}

export default (props: Props) => {
    const nextScreen = props.navigation.getParam('nextScreen')
    return (
        <View style={{ paddingVertical: 3, backgroundColor: 'rgba(0,0,0,0.05)' }}>
            <Button title='Añadir Ingredientes' buttonStyle={{ marginVertical: 10 }} onPress={() => { props.navigation.navigate(nextScreen,{type:"ingredients"}) }} />
            <Button title='Añadir Recetas' buttonStyle={{ marginVertical: 10 }} onPress={() => { props.navigation.navigate(nextScreen,{type:"recipes"}) }} />
            <Button title='Añadir Precios' buttonStyle={{ marginVertical: 10 }} onPress={() => { props.navigation.navigate(nextScreen,{type:"price_ingredient"}) }} />
        </View>
    );
}

