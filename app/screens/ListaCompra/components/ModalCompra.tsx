import React, { useRef, useState, useContext, useEffect } from "react";
import { Text, View, ImageBackground, TouchableOpacity } from "react-native";
import { Button, CheckBox, ButtonGroup } from 'react-native-elements';
var s = require('../../../style');
import { comandaCompraFirebase, getCostIngredientsList } from '../../../storage/storage'
import ModalPlanNBite from "../../../components/ModalPlanNBite";
import Loading from "../../../components/Loading";
import { translate } from "../../../Utils/utils";

type Props = {
    modalVisible: boolean,
    set_modalVisible: (x: boolean) => void,
    lista_compra: any[],
    set_lista_ingredientes_compra: (x: any[]) => void,
}

export default ({ modalVisible, set_modalVisible, lista_compra, set_lista_ingredientes_compra }: Props) => {
    const [cost, set_cost] = useState(0)

    const init = async () => {
        set_cost(await getCostIngredientsList())
    }

    useEffect(() => {
        init()
    }, [])

    console.log(cost)
    return (
        <ModalPlanNBite visible={modalVisible} onPressCancel={() => set_modalVisible(false)}>
            {cost > 0 ?
                <View style={{ width: '100%' }}>
                    {lista_compra.map((ingrediente: any) => (
                        <View style={[s.row, s.magicIcon2LaVenganza, { marginBottom: 20, paddingHorizontal: 20, width: '100%', alignItems: "space-between", justifyContent: "space-between" }]}>
                            {/* <Image style={s.img_XS} source={logImg} /> */}
                            <Text> {translate(ingrediente.nombre)}</Text>
                            <Text> {ingrediente.cantidad} {ingrediente.tipo_cantidad}</Text>
                        </View>
                    ))}
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>

                        <Button title='Cancelar' onPress={() => {
                            set_modalVisible(false);
                        }} />
                        <Button title={`Comprar (${cost}€)`} onPress={() => {
                            set_modalVisible(false);
                            comandaCompraFirebase(cost);
                            set_lista_ingredientes_compra([]);
                        }} />
                    </View>
                </View>
                :
                <View style={{height:100}}>
                    <Loading />
                </View>
            }
        </ModalPlanNBite>

    )
}
