import React, { useRef, useState, useContext, useEffect } from "react";
import { Text, View, ImageBackground, TouchableOpacity } from "react-native";
import { Button, CheckBox, ButtonGroup } from 'react-native-elements';
import { AppContext } from '../../provider/provider'
var s = require('../../style');
import { ScrollView } from 'react-native-gesture-handler';
import { Header } from 'react-native-elements';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import { get_lista_compra, vaciarListaCompraFirebase, comandaCompraFirebase, getCostIngredientsList } from '../../storage/storage'
import { useTranslation } from 'react-i18next'
import ModalPayment from "../../components/ModalPayment";
import { Context } from '../../type/type';
import { translate } from "../../Utils/utils";

const CustomButton = (props: any) =>
    <TouchableOpacity onPress={() => {

        props.navigation.navigate('Premium')
    }} >
        <Icon2 name="view-list" size={26} style={{ color: 'white' }} />
    </TouchableOpacity>



const ListaCompra = (props: any) => {
    const context: any = useContext(AppContext)
    console.log("context",context.lista_ingredientes_lista_compra)

    const [user, setUser] = React.useState({ ...props.user });
    const [modalPremium, setModalPremium] = useState<any>({ visible: false, data: undefined })

    const { t } = useTranslation();



    const show_View_compra = () => {
        let views;
        if (context.lista_ingredientes_lista_compra.length > 0) {
            views = context.lista_ingredientes_lista_compra.map((ingrediente: any) => (
                <View style={[s.row, s.magicIcon2LaVenganza, { marginBottom: 20, alignItems: "space-between", justifyContent: "space-between" }]}>
                    {/* <Image style={s.img_XS} source={logImg} /> */}
                    <View>
                        <Text style={[s.text_color_gray, s.fuente_M]} > {translate(ingrediente.nombre)}</Text>
                    </View>
                    <View style={{ width: 100 }}>
                        <Text style={[s.text_color_gray, s.fuente_M]} > {ingrediente.cantidad} {ingrediente.tipo_cantidad}</Text>
                    </View>
                </View>
            ));
        } else {
            views = (
                <View>
                    <Text style={[s.fuente_M, { textAlign: 'center', color: 'white', marginTop: '40%', letterSpacing: 2, marginHorizontal: '5%' }]}>{t("lista_compra.empty_state.title")}</Text>
                    <Text style={[s.fuente_S, { textAlign: 'center', padding: 10, color: 'white', marginTop: '5%', marginHorizontal: '10%' }]}>{t("lista_compra.empty_state.description")}</Text>
                </View>
            )
        }
        return <View>{views}</View>;
    }
    const inicialize = () => {

    }
    const generarView_Button_Anadir_Nevera = () => {
        let button;
        if (context.lista_ingredientes_lista_compra.length > 0) {
            button = (
                <Button style={{ marginTop: 50 }} title="Añadir a la Nevera"
                    onPress={() => {
                        vaciarListaCompraFirebase()
                        context.set_lista_ingredientes_lista_compra([])
                    }}
                />
            );
        } else {
            button = <View />
        }
        return button;
    }


    const generarView_Button_Comprar = () => {
        let button;
        if (context.lista_ingredientes_lista_compra.length > 0) {
            button = (
                <Button
                    style={{ marginTop: 50 }}
                    title="Comprar"
                    onPress={async () => {
                        const price = await getCostIngredientsList()
                        setModalPremium({ visible: true, data: { price } })
                    }}
                />
            );
        } else {
            button = <View />
        }
        return button;

    }

    const getViewModalPayment = () => {
        return (
            <ModalPayment
                visible={modalPremium.visible}
                closeModal={() => { setModalPremium({ visible: false, data: undefined }) }}
                amount={modalPremium.data ? modalPremium.data.price : undefined}
                onPressAccept={async () => {
                    comandaCompraFirebase(modalPremium.data.price);
                    context.set_lista_ingredientes_lista_compra([]);
                }}
                navigation={props.navigation}
            >
                <View>
                    {context.lista_ingredientes_lista_compra.map((ingrediente: any) => (
                        <View style={[s.row, s.magicIcon2LaVenganza, { marginBottom: 20, paddingHorizontal: 20, width: '100%', alignItems: "space-between", justifyContent: "space-between" }]}>
                            {/* <Image style={s.img_XS} source={logImg} /> */}
                            <Text> {translate(ingrediente.nombre)}</Text>
                            <Text> {ingrediente.cantidad} {ingrediente.tipo_cantidad}</Text>
                        </View>
                    ))}
                </View>
            </ModalPayment>
        )
    }


    return (

        <ImageBackground style={[{ alignItems: "center", height: '100%' }]} source={require('../../assets/Screen_BuyList_00.png')}>
            <Header
                placement="center"

                //leftComponent= {<CustomButton/>}
                //leftComponent= {{ icon: 'menu' ,  color: '#fff' }}
                centerComponent={{ text: 'Lista de la compra', style: { color: '#fff' } }}
                rightComponent={CustomButton(props)}
            //rightComponent={{ icon: 'home', color: '#fff' }}
            />
            <View style={{ marginHorizontal: 40, height: '75%' }}>
                <ScrollView >
                    {show_View_compra()}
                </ScrollView>
            </View>
            {generarView_Button_Anadir_Nevera()}
            {generarView_Button_Comprar()}
            {getViewModalPayment()}
        </ImageBackground>
    )
}

export default ListaCompra;