import { getNutricionistasFirebase, get_Premium_Features_Firebase, set_Monthly_Premium_Firebase, set_Nutricionist_Firebase} from '../../../storage/storage';
import moment from 'moment'
import { formatMomentLite } from '../../../local/constant';

export const initializate = async (setState: (x: any) => void) => {
    const listNutricionistas: any = await getNutricionistasFirebase();
    const features = await get_Premium_Features_Firebase()
    setState({ listNutricionistas, loaded: true, featureNutricionistas: features.nutricionista>moment().format(formatMomentLite) })
}

export const onPressNutricionista = (nutricionista: any, groupState: any) => {
    const [state, setState] = groupState;
    setState({ ...state, modalPayVisible: true, modalPayData: nutricionista })
}


export const onModalPressAccept = async(nutricionista: any) => {
    await set_Monthly_Premium_Firebase(true)
    await set_Nutricionist_Firebase(nutricionista.email);
}