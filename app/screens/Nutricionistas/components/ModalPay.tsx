import React, { useRef, useState, Component, useEffect } from 'react';
import { StyleSheet, TouchableOpacity, Text, View, TextInput, Image, KeyboardAvoidingView, ImageBackground, Dimensions } from "react-native";
import ModalPlanNBite from '../../../components/ModalPlanNBite';
import { ViewRow } from '../../../components/commonStyledComponents'
import { Button } from 'react-native-elements';


type Props = {
    visible: boolean,
    onPressCancel: () => void,
    onPressAccept: () => void,
    nutricionista: any
}

export default (props: Props) => {


    return (
        <ModalPlanNBite visible={props.visible} onPressCancel={props.onPressCancel}>
            <Text>pagar {props.nutricionista ? props.nutricionista.price : ""}€</Text>
            <ViewRow>
                <Button title='Cancel' onPress={props.onPressCancel} />
                <Button title='Accept' onPress={props.onPressAccept} />
            </ViewRow>
        </ModalPlanNBite>
    );

}


