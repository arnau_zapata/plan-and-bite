import React, { useRef, useState, Component, useEffect } from 'react';
import { StyleSheet, TouchableOpacity, Text, View, } from "react-native";
import Loading from '../../components/Loading';
import ModalPayment from '../../components/ModalPayment';
import ModalPay from './components/ModalPay';
import Nutricionista from './components/Nutricionista'
import { initializate, onPressNutricionista, onModalPressAccept } from './functions/functions';

type State = {
    listNutricionistas: any[],
    loaded: boolean,
    featureNutricionistas: boolean
    modalPayVisible: boolean,
    modalPayData: any
}

type Props = {
    navigation: any
}

export default (props: Props) => {
    const [state, setState] = useState<any>({
        listNutricionistas: [],
        loaded: false,
        featureNutricionistas: false,
        modalPayVisible: false,
        modalPayData: undefined
    })
    useEffect(() => {
        initializate(setState)
    }, [])

    return (
        <View>
            {state.loaded ?
                (state.featureNutricionistas ?
                    <Nutricionista />
                    : state.listNutricionistas.map((nutricionista: any) => {
                        return (
                            <TouchableOpacity
                                style={{ backgroundColor: 'white', padding: 10, marginBottom: 3, borderBottomWidth: 1, borderBottomColor: 'rgba(0,0,0,0.1)' }}
                                onPress={() => onPressNutricionista(nutricionista, [state, setState])}
                            >
                                <Text>{nutricionista.nombre}</Text>
                            </TouchableOpacity>
                        )
                    })
                )
                : <Loading />
            }
            {state.modalPayData &&
                <ModalPayment
                    visible={state.modalPayVisible == true}
                    closeModal={() => { setState({ ...state, modalPayVisible: false }) }}
                    amount={state.modalPayData ? state.modalPayData.price : undefined}
                    onPressAccept={() => {
                        setState({ ...state, modalPayVisible: false, modalPayData: undefined });
                        onModalPressAccept(state.modalPayData)
                    }}
                    navigation={props.navigation}
                />
            }
        </View>
    );

}


