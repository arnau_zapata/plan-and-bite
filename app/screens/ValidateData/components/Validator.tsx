import React, { useState, useEffect, useContext } from "react";
import { Text, View, StyleSheet, TextInput, TouchableOpacity, Dimensions } from "react-native";

import { NavigationInjectedProps } from 'react-navigation'
import { Icon } from "react-native-elements";
interface Props {
    onPress: (x: boolean) => void
}

const styles = StyleSheet.create({
    option: {
        borderRadius: 100,
        padding: 5
    }
})

export default (props: Props) => {
    const [optionSelected, setOptionSelected] = useState(-1);

    return (
        <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity style={[styles.option, { marginHorizontal: 15, borderWidth: optionSelected == 0 ? 1 : 0 }]}
                onPress={() => { setOptionSelected(0); props.onPress(true) }}
            >
                <Icon name={'check'} size={25} />
            </TouchableOpacity>
            <TouchableOpacity style={[styles.option, { borderWidth: optionSelected == 1 ? 1 : 0 }]}
                onPress={() => { setOptionSelected(1); props.onPress(false) }}
            >
                <Icon name={'cancel'} size={25} />
            </TouchableOpacity>
        </View>
    )
}

