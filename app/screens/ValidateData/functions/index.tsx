import { validateDataFirebase } from "../../../storage/storage";

export const validateName = (check:boolean,state:any,setState:any, typeData:string, navigation:any) => {
  let data: any = { ...state.data }

  if (check) {
    delete data[state.id]
    setState({ ...state,data, onlyNameData: false })
  } else {
    const senderData: any = { ...state.data, }
    // delete senderData.id;
    Object.entries(senderData).forEach(([key, value]: any) => {
      senderData[key] = { ...value, validated: false }
    })
    console.log("senderData", senderData)
    validateDataFirebase(typeData, state.firstParam.data, senderData);
    const index = state.index;
    data = state.collectionData[index] ? state.collectionData[index] : {}
    const id = state.id;

    if (state.index + 1 < state.collectionData.length) setState({ ...state, senderData: {}, index: index + 1, data, onlyNameData: true })
    else navigation.navigate("FreeBitecoins")
  }
}