import React, { useState, useEffect, useContext } from "react";
import { Text, View, ImageBackground, TextInput, TouchableOpacity, Dimensions } from "react-native";

import { NavigationInjectedProps } from 'react-navigation'
import ProcessBar from "../../components/ProcessBar";
import {
  LineChart,
} from 'react-native-chart-kit'
import TapButton from "../../components/TapButton";
import moment from 'moment'

import {
  getCollectionDataFirebase, getLeyendUploadDataFirebase, validateDataFirebase,
} from "../../storage/storage";
import Loading from "../../components/Loading";
import { useTranslation } from 'react-i18next'
import { AppContext } from '../../provider/provider';
import { Button, Icon } from "react-native-elements";
import Validator from "./components/Validator";
import { validateName } from "./functions";
interface Props extends NavigationInjectedProps { }



export default (props: Props) => {
  const { t } = useTranslation();
  const context: any = useContext(AppContext)
  const [state, setState] = useState({
    collectionData: [],
    senderData: {},
    index: 0,
    loading: true,
    data: {},
    onlyNameData: true,
    id: "",
    firstParam: {}
  });

  console.log("state",state)

  const typeData = props.navigation.getParam("type")
  console.log(typeData, state.senderData)
  const init = async () => {
    const collectionData: any = await getCollectionDataFirebase(typeData)
    const data: any = collectionData[state.index] ? collectionData[state.index] : {}
    const leyend = await getLeyendUploadDataFirebase(typeData)
    setState({ ...state, collectionData, data, id: leyend[0].id, loading: false, firstParam: data[leyend[0].id] })
  };
  useEffect(() => {
    init();
  }, [])

  const renderText = (text: string) => {
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <Text >{text}</Text>
      </View>
    )
  }

  const renderMap = (keyMap: string, data: any) => {
    console.log("value", data)
    return (
      <View>
        {!Array.isArray(data) &&
          Object.entries(data).map(([key, value]: any) => {
            const isMap = typeof (value.data) == typeof {};
            return (
              <View style={{ marginBottom: 15, flexDirection: 'row', marginLeft: 15, justifyContent: 'space-between', width: Dimensions.get('window').width - 45 }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={{ fontWeight: 'bold', marginRight: 5 }}>{key}:</Text>
                  {renderText(value.data)}
                </View>
                <Validator onPress={(check) => {
                  const senderData: any = { ...state.senderData, }
                  const mapData = { ...senderData[keyMap] }
                  mapData[key] = { ...value, validated: check }
                  senderData[keyMap] = mapData;
                  setState({ ...state, senderData })
                }} />
              </View>
            )
          })
        }
        {Array.isArray(data) &&
          data.map((value: any, position) => {
            return (
              <View style={{ marginBottom: 15, flexDirection: 'row', marginLeft: 15, justifyContent: 'space-between', width: Dimensions.get('window').width - 45 }}>
                <View style={{ flexDirection: 'row' }}>
                  {/* <Text style={{ fontWeight: 'bold', marginRight: 5 }}>{key}:</Text> */}
                  {renderText(value.data)}
                </View>
                <Validator onPress={(check) => {
                  try {
                    const senderData: any = { ...state.senderData, }
                    const mapData = senderData[keyMap] ? [...senderData[keyMap]] : []
                    mapData[position] = { ...value, validated: check }
                    senderData[keyMap] = mapData;
                    setState({ ...state, senderData })
                  } catch (e) {
                    console.log(e)
                  }
                }} />
              </View>
            )
          })
        }
      </View>
    )
  }

  const renderData = (key: any, value: any, isMap: boolean, isArray: boolean) => {
    return (
      <View style={{ marginBottom: 15, flexDirection: 'row', justifyContent: 'space-between' }}>
        <View style={{ flexDirection: (isMap || isArray) ? 'column' : 'row' }}>
          <Text style={{ fontWeight: 'bold', marginRight: 5 }}>{key}:</Text>
          {(!isMap && !isArray) ? renderText(value.data) : renderMap(key, value)}
        </View>
        {!isMap &&
          <Validator onPress={(check) => {
            const senderData: any = { ...state.senderData, }
            senderData[key] = { ...value, validated: check }
            setState({ ...state, senderData })
          }} />
        }
      </View>
    )
  }

  return (
    <View style={{ height: '100%', backgroundColor: 'white', padding: 15 }} >
      {state.onlyNameData && !state.loading && state.firstParam &&
        <View style={{ marginBottom: 15, flexDirection: 'row', justifyContent: 'space-between' }}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold', marginRight: 5 }}>{state.id}:</Text>
            {renderText(state.firstParam.data)}
          </View>
          <Validator onPress={(check) => validateName(check, state, setState, typeData, props.navigation)} />
        </View>
      }
      {!state.onlyNameData && !state.loading &&
        Object.entries(state.data).map(([key, value]: any) => {
          const isMap = typeof value == typeof {} && !value.data
          const isArray = Array.isArray(value);
          // console.log("value",value,isMap)

          return (
            renderData(key, value, isMap, isArray)
          )
        })
      }
      {!state.onlyNameData && !state.loading && <Button title={`Añadir `} onPress={() => {
        // console.log("final Data",typeData, state.firstParam.data, state.senderData)
        console.log("**************************************************")

        validateDataFirebase(typeData, state.firstParam.data, state.senderData);
        if (state.index + 1 < state.collectionData.length) setState({ ...state, senderData: [], index: state.index + 1 })
        else props.navigation.navigate("FreeBitecoins")
      }} />}
    </View>
  )
}

