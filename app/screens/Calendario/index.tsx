import React, { useContext, useEffect, useState } from 'react';
import Calendario from './Calendario';
import { useTranslation } from 'react-i18next'
import { AppContext } from '../../provider/provider';
import { loadPlanning } from './functions'
import moment from 'moment'

import { Context } from '../../type/type';

export default (props:any) => {
    const context:any = useContext(AppContext)

    const [state, setState] = useState({
        items: {},
        visible: false,
    })
    useEffect(() => {
        console.log(`context.reset (${context.reset})`)
        if (context.reset) {
            context.set_reset(context, false)
            loadPlanning(moment().format("YYYY-MM-DD"),context, state, setState)
        }
    },[context.reset])
    const { t } = useTranslation();
    return <Calendario t={t} navigation={props.navigation} context={context} state={state} setState={setState} />
} 