import React, { useContext } from 'react';
import { StyleSheet, TouchableOpacity, Text, View, Image, Alert } from "react-native";
import { Button } from 'react-native-elements';
import { Plato } from '../../../type/type';
import moment from 'moment'
import { Icon } from 'react-native-elements';
import { useTranslation } from 'react-i18next'
import { AppContext } from '../../../provider/provider';

import { Context } from '../../../type/type';
import { translate } from '../../../Utils/utils';

var s = require('../../../style');

const { integer_to_comida } = require('../../../Utils/utils_calendario')
const { get_Receta } = require('../../../Utils/utils')
const { get_Num_plato_vacio, eliminar_receta_a_firestore } = require('../../../Utils/utils_planning')
const icon_edit = require('../assets/icon_edit.png');
const icon_eliminar = require('../assets/icon_eliminar.png');
const icon_anadir = require('../assets/icon_anadir.png');

type Props = {
    planningDia: any,
    navigation: any,
    ingredientes_nevera: any
}
const MenuDia = (props: Props) => {
    const context = useContext(AppContext)

    const { t } = useTranslation();
    let lista_recetas = props.planningDia.lista_recetas;
    let actualDayFormated = moment().format('YYYY-MM-DD')
    let daySelectedFormated = moment().year(props.planningDia.year).month(props.planningDia.mes - 1).date(props.planningDia.dia).format('YYYY-MM-DD')
    let canModifyRecipes = daySelectedFormated >= actualDayFormated;
    const getViewAddRecipe = () => {
        return (
            <TouchableOpacity
                style={[{ borderRadius: 100, backgroundColor: 'white', padding: 2 }]}
                onPress={() => {
                    let year = props.planningDia.year;
                    let month = props.planningDia.mes;
                    let day = props.planningDia.dia;
                    let comida = props.planningDia.comida;
                    let plato = get_Num_plato_vacio(lista_recetas);
                    props.navigation.navigate('AnadirReceta', { year, month, day, comida, plato });
                }
                }>
                <Image style={s.img_S} source={icon_anadir} />
            </TouchableOpacity>
        );
    }

    const getView_lista_platos = (item: any) => {
        let lista_recetas = item.lista_recetas;
        let num_plato = 0;
        lista_recetas = lista_recetas.map((receta: Plato) => {
            const lista_ingredientes = get_Receta(context, receta.receta).lista_ingredientes
            const b = lista_ingredientes.every((ingredienteNevera: any) => props.ingredientes_nevera.find((ingrediente: any) => ingrediente.id_ingrediente == ingredienteNevera.id_ingrediente && ingredienteNevera.cantidad >= ingrediente.cantidad) != undefined)

            return (
                <TouchableOpacity
                    style={[s.row, { justifyContent: "space-between", padding: 10, borderWidth: 1, backgroundColor: b ? 'white' : 'yellow' }]}
                    onPress={() => {
                        props.navigation.navigate('Receta', {
                            last_screen: 'Calendario',
                            receta_seleccionada: get_Receta(context, receta.receta),
                            year: item.year,
                            month: item.mes,
                            day: item.dia,
                            comida: item.comida,
                            plato: receta.plato,
                            quantity: receta.quantity ? receta.quantity : 1,
                            warning: !b
                        });
                    }}>
                    <View style={{ width: canModifyRecipes ? '50%' : '100%' }}>
                        <Text>{get_Receta(context, receta.receta) && translate(get_Receta(context, receta.receta).nombre)}</Text>
                    </View>
                    {!b && <Icon name='warning' type='font-awesome' />}
                    {canModifyRecipes &&
                        <View style={s.row}>
                            <Button
                                buttonStyle={{ borderWidth: 1, borderColor: 'black', backgroundColor: 'white' }}
                                icon={
                                    <Image style={s.img_XS} source={icon_edit} />
                                }
                                onPress={() => {
                                    props.navigation.navigate('AnadirReceta', {
                                        year: item.year,
                                        month: item.mes,
                                        day: item.dia,
                                        comida: item.comida,
                                        plato: receta.plato,
                                        quantity: item.quantity ? item.quantity : 1,
                                        warning: !b
                                    });

                                }}
                            />
                            <Button
                                buttonStyle={{ borderWidth: 1, borderColor: 'black', backgroundColor: 'white', marginLeft: 10 }}
                                icon={
                                    <Image style={s.img_XS} source={icon_eliminar} />
                                }
                                onPress={async () => {
                                    console.log(item.year, item.mes, item.dia, (item.comida), receta.plato,)
                                    await eliminar_receta_a_firestore(item.year, item.mes, item.dia, (item.comida), receta.plato, context)
                                    // props.navigation.navigate('Loading', {
                                    //     text: "eliminando receta, por favor, espere",
                                    //     function: eliminar_receta_a_firestore(item.year, item.mes, item.dia, comida_to_integer(item.comida), receta.plato, context),
                                    //     textDone: "receta eliminada",
                                    //     nextScreen: "TabsBottom"
                                    // });
                                }}
                            />
                        </View>}

                </TouchableOpacity>
            );
            // }

        });

        return <View style={{ marginLeft: 20 }}>{lista_recetas}</View>;
    }

    return (
        <View style={[{ marginTop: 10, borderRadius: 5, padding: 10, marginRight: 10 }]}>
            <View style={[s.row, s.background_blue, { padding: 10, width: '100%', alignItems: 'center', justifyContent: "space-between" }]} >
                <Text style={[s.fuente_M, s.text_color_gray]}>{integer_to_comida(props.planningDia.comida)} </Text>
                {lista_recetas.length < 3 && canModifyRecipes && getViewAddRecipe()}
            </View>
            {getView_lista_platos(props.planningDia)}
        </View>
    );
}





const styles = StyleSheet.create({
    item: {
        flex: 1,

    },
    emptyDate: {
        height: 15,
        flex: 1,
        paddingTop: 30
    }
});


export default MenuDia;