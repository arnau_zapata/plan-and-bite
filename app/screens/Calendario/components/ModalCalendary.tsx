import React, { useContext, useEffect, useState } from 'react';
import { StyleSheet, TouchableOpacity, Text, View, Image, Alert } from "react-native";
import { Button } from 'react-native-elements';
import Estructura_Global from '../../../Estructuras/Estructura_Global.json';
import { Plato, Persona } from '../../../type/type';
import moment from 'moment'
import ModalPlanNBite from '../../../components/ModalPlanNBite';
import TouchableUser from './TouchableUser';
import CONSTANT from '../../../local/constant'
import { useTranslation } from 'react-i18next'
import { AppContext } from '../../../provider/provider';
import { Context } from '../../../type/type';
import { rellenar_calendario_con_IA } from '../../../Utils/utils_planning';
const {
    get_Premium_Features_Firebase,
    updateSelectedPerson,
    getPersonSelectedValues,
} = require('../../../storage/storage')
var s = require('../../../style');


const MenuDia = (props: any) => {
    const context: any = useContext(AppContext)
    const { t } = useTranslation();
    const [featureVariasPersonas, setFeatureVariasPersonas] = useState(true)
    const [lista_personas, setLsta_personas] = useState([])
    const [error_message, set_error_message] = useState("")
    const initializate = async () => {
        let lista_personas: any = context.lista_personas;
        setLsta_personas(lista_personas)
        let premiumFeatures = await get_Premium_Features_Firebase()
        setFeatureVariasPersonas(premiumFeatures && premiumFeatures.varias_personas && premiumFeatures.varias_personas > moment().format(CONSTANT.formatMoment))
    }
    useEffect(() => {
        initializate()
    }, [])
    useEffect(() => {
        set_error_message("")
    }, [props.visible]);

    return (
        <ModalPlanNBite
            visible={props.visible}
            onPressCancel={() => props.callBack()}
        >
            <View style={{ backgroundColor: 'white' }}>
                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{t("calendario.modal.automatic_planning")}</Text>
                <Text>{t("calendario.modal.init_automatic_planning")}</Text>
                {featureVariasPersonas &&
                    <View>
                        <Text>{t('calendario.modal.select_people')}</Text>
                        <View style={{ flexDirection: 'row' }}>
                            {lista_personas && lista_personas.map((persona: Persona, index: number) => {
                                return (
                                    <TouchableUser name={persona.nombre} index={index} selected={persona.selected} callBack={async (i: number, touch: boolean) => {
                                        await updateSelectedPerson(i, touch, context)
                                    }} />
                                )
                            })}
                        </View>
                        <Text>{error_message}</Text>
                    </View>
                }
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>

                    <Button title='Cancel ' onPress={() => props.callBack()} />
                    <Button title='Planificar' onPress={async () => {
                        let { averageNutricionalValues, numberSelectedPerson } = await getPersonSelectedValues(context)
                        if (numberSelectedPerson == 0 && featureVariasPersonas) {
                            set_error_message("Tienes que seleccionar una opcion")
                            return
                        } else {
                            let mom = moment();
                            let year = mom.year();
                            let mes = mom.month();
                            let dia = mom.date();
                            props.navigation.navigate('Loading', {
                                text: "Se esta actualizando el planning, por favor, espere",
                                function: rellenar_calendario_con_IA(year, mes, dia, featureVariasPersonas, context),
                                textDone: "planning actualizado",
                                nextScreen: "TabsBottom"
                            });
                        }
                    }} />
                </View>
            </View>
        </ModalPlanNBite>
    );
}







export default MenuDia;
