import React, { Component, useEffect } from 'react';
import { StyleSheet, TouchableOpacity, Text, View, Image, Alert, Modal } from "react-native";
import { Button } from 'react-native-elements';
var s = require('../../style');
import SafeAreaView from 'react-native-safe-area-view';
import { Header } from 'react-native-elements';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import moment from 'moment';
import MenuDia from './components/MenuDia'
import ModalCalendary from './components/ModalCalendary';
import { Context, Menu, Plato } from '../../type/type';
import { loadPlanning } from './functions';

const CustomButton = (props: any) =>

  <TouchableOpacity onPress={() => {

    props.navigation.navigate('Premium')
  }} >
    <Icon2 name="view-list" size={26} style={{ color: 'white' }} />
  </TouchableOpacity>

const { Agenda } = require('react-native-calendars');

class PlanningManual extends Component {

  constructor(props: any) {
    super(props);
  }


  getViewModal = (context: any, state: any, setState: any, navigation: any) => {
    return (
      <ModalCalendary
        visible={state.visible}
        navigation={navigation}
        callBack={() => {
          setState({ ...state, visible: false });
        }}
      />
    )
  }

  render() {
    const { t, context, state, setState, navigation }: any = this.props
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{ flex: .92 }}>
          <Header
            placement="center"
            centerComponent={{ text: 'Calendario', style: { color: '#fff' } }}
            rightComponent={CustomButton(this.props)}
          />
          <Agenda
            items={state.items}
            loadItemsForMonth={(data: any) => this.loadItems(data, context, state, setState)}
            selected={moment().format("YYYY-MM-DD")}
            renderItem={(data: any) => this.renderItem(data, context, navigation)}
            renderEmptyDate={(data: any) => this.renderEmptyDate(data, context, navigation)}
            rowHasChanged={this.rowHasChanged.bind(this)}
          />
        </View>
        <View style={{ flex: .08 }}>
          <View style={{ position: 'absolute', left: 0, right: 0, bottom: 0 }}>
            <Button
              icon={
                <Text style={{ color: 'white' }}>{t("calendario.principal.plan_automatically")}</Text>
              }
              buttonStyle={{
                padding: 26, //borderWidth: 1 
              }}
              onPress={() => {
                setState({ ...state, visible: true });
              }}
            />
          </View>
          {this.getViewModal(context, state, setState, navigation)}
        </View>
      </SafeAreaView>

    );
  }


  load_calendario = async (day: any, context: Context, state: any, setState: any) => {
    loadPlanning(day.dateString, context, state, setState)
  }

  loadItems(day: any, context: any, state: any, setState: any) {
    this.load_calendario(day, context, state, setState);
  }

  renderItem(item: any, context: Context, navigation: any) {
    return <MenuDia planningDia={item} navigation={navigation} ingredientes_nevera={context.lista_ingredientes_nevera} />
  }


  renderEmptyDate(time: any, context: Context, navigation: any) {
    const date = this.timeToString(time);
    let aux = date.split("-");
    let year = parseInt(aux[0]);
    let mes = parseInt(aux[1]);
    let dia = parseInt(aux[2]);
    return (
      <View>
        {[0, 1, 2].map((comida) => {
          const item = ({
            button: true,
            comida: (comida),
            lista_recetas: [],
            dia: dia,
            mes: mes,
            year: year,
            height: 50,
          });
          return <MenuDia planningDia={item} navigation={navigation} ingredientes_nevera={context.lista_ingredientes_nevera} />
        })}
      </View>
    );
  }

  rowHasChanged(r1: any, r2: any) {
    return r1.name !== r2.name;
  }

  timeToString(time: any) {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  }
}


const styles = StyleSheet.create({
  item: {
    flex: 1,

  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 30
  }
});


export default PlanningManual;