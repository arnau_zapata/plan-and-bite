import moment from 'moment'
import { Plato } from '../../../type/type';

export const loadPlanning = (dateString: string, context: any, state: any, setState: any) => {
    const day = moment();
    const items: any = {}
    console.log(context.lista_planning);
    for (let i = -2; i < 8; i++) {
        const strTime = moment(dateString, "YYYY-MM-DD").add(i, "days").format("YYYY-MM-DD");
        let aux = strTime.split("-");
        let year = parseInt(aux[0]);
        let mes = parseInt(aux[1]);
        let dia = parseInt(aux[2]);
        if (!items[strTime]) {
            items[strTime] = [];
            const menu = context.lista_planning.filter((obj: Plato) => obj.dia === dia && obj.mes === mes && obj.year === year);

            menu.length != 0 && [0, 1, 2].forEach(comida => {
                const listaPlatos = menu.filter((obj: Plato) => obj.comida === comida)
                items[strTime].push({
                    comida: (comida),
                    lista_recetas: listaPlatos,
                    dia: dia,
                    mes: mes,
                    year: year,
                    height: 50,
                });
            });
        }
    }
    setState({
        ...state,
        items: {...state.items, ...items},
        visible: false,
    });
}