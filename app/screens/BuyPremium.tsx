import React, { useRef, useState, Component, useContext } from 'react';
import { StyleSheet, Image, Text, View, Input, TextInput, RadioButton, TouchableOpacity, KeyboardAvoidingView, Dimensions } from 'react-native';

var s = require('../style');

import { Button, ButtonGroup } from 'react-native-elements';

import Swiper from "react-native-swiper";


const window = Dimensions.get('window');

var styles = {
    wrapper: {
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black'
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black'
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black'
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold'
    }
}



//const Premium = props => {

export default () =>
    <View style={[{ paddingVertical: 50, height: '100%', margin: 0, backgroundColor: 'black', alignItems: 'center' }]}>
        <Text style={[s.fuente_L, s.basicPadding, { color: 'white' }]}>Subscripciones</Text>
        <Text style={[s.fuente_XS, { color: 'white' }]}>Subscribete en menos de 30 segundos. Ahorra dinero y tiempo a la</Text>
        <Text style={[s.fuente_XS, { color: 'white' }]}>hora de comprar tu comida semanalmente</Text>
        <Swiper style={[{ width: window.width, justifyContent: 'center', alignItems: 'center', color: 'white' }]} showsButtons>

            <View style={styles.slide1}>
                <View style={{ marginVertical: 20, backgroundColor: 'white', borderRadius: 20, borderWidth: 1, marginTop: 20, width: 200, height: 400 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#008F7A', marginTop: 20, height: 70, width: '100%' }}>
                        <Text style={{ fontSize: 16, color: 'white', fontWeight: "bold" }}>Freemium</Text>
                    </View>
                    <View style={{ marginHorizontal: 15, height: 250 }}>
                        <Text style={[s.fuente_M, { marginVertical: 15 }]}>Gratuito</Text>
                        <Text style={[s.fuente_12, s.vertical_padding_XXS, {}]}>Poder tener acceso al planning manual y automatico</Text>
                        <Text style={[s.fuente_12, s.vertical_padding_XXS, {}]}>Control de peso</Text>
                        <Text style={[s.fuente_12, s.vertical_padding_XXS, {}]}>Crear tu super lista</Text>
                    </View>
                </View>
            </View>

            <View style={styles.slide2}>
                <View style={{ backgroundColor: 'white', borderRadius: 20, borderWidth: 1, width: 200, height: 400 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#008F7A', marginTop: 20, height: 70, width: '100%' }}>
                        <Text style={{ fontSize: 16, color: 'white', fontWeight: "bold" }}>Premium</Text>
                    </View>
                    <View style={{ marginHorizontal: 15, height: 250 }}>
                        <Text style={[s.fuente_M, { marginVertical: 15 }]}>€4,99/mes</Text>
                        <Text style={[s.fuente_12, s.vertical_padding_XXS, {}]}>Descarga de recetas</Text>
                        <Text style={[s.fuente_12, s.vertical_padding_XXS, {}]}>Sin publicidad</Text>
                        <Text style={[s.fuente_12, s.vertical_padding_XXS, {}]}>Control de peso y objetivos</Text>
                    </View>
                    <Button buttonStyle={{ marginHorizontal: 20 }} title='Comprar' type="outline" />
                </View>
            </View>

            <View style={styles.slide3}>
                <View style={{ backgroundColor: 'white', borderRadius: 20, borderWidth: 1, width: '60%', height: 400 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#008F7A', marginTop: 20, height: 70, width: '100%' }}>
                        <Text style={{ fontSize: 16, fontWeight: "bold", color: 'white' }}>Deluxe</Text>
                    </View>
                    <View style={{ marginHorizontal: 15, height: 250 }}>
                        <Text style={[s.fuente_M, { marginVertical: 15 }]}>€9,99/mes</Text>
                        <Text style={[s.fuente_12, s.vertical_padding_XXS, {}]}>Descarga de recetas</Text>
                        <Text style={[s.fuente_12, s.vertical_padding_XXS, {}]}>Control de peso</Text>
                        <Text style={[s.fuente_12, s.vertical_padding_XXS, {}]}>Sin publicidad</Text>
                        <Text style={[s.fuente_12, s.vertical_padding_XXS, {}]}>Planning automatico</Text>
                        <Text style={[s.fuente_12, s.vertical_padding_XXS, {}]}>Acceso a dietista personal</Text>
                    </View>
                    <Button buttonStyle={{ marginHorizontal: 20 }} title='Comprar' type="outline" />
                </View>
            </View>

        </Swiper>
    </View>

/*

    return (
        <KeyboardAvoidingView
            keyboardVerticalOffset={Header.HEIGHT + 40} // adjust the value here if you need more padding
            style={{ flex: 1 }}
            behavior="padding" >
            <View style={[{ paddingVertical: 50, height: 1000, margin: 0, backgroundColor: 'black', alignItems: 'center' }]}>
                <Text style={[s.fuente_L, s.basicPadding, { color: 'white' }]}>Subscripciones</Text>
                <Text style={[s.fuente_XS, { color: 'white' }]}>Subscribete en menos de 30 segundos. Ahorra dinero y tiempo a la</Text>
                <Text style={[s.fuente_XS, { color: 'white' }]}>hora de comprar tu comida semanalmente</Text>
                <Swiper style={[{ height: 400, width: window.width, justifyContent: 'center', alignItems: 'center' }]}>
                    <View style={{ marginVertical: 20, backgroundColor: 'white', borderRadius: 20, borderWidth: 1, marginTop: 20, width: window.width, height: 400 }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#008F7A', marginTop: 20, height: 70, width: '100%' }}>
                            <Text>Freemium</Text>
                        </View>
                        <View style={{ marginHorizontal: 15, height: 250 }}>
                            <Text style={[s.fuente_M, { marginVertical: 15 }]}>Gratuito</Text>
                            <Text style={[s.fuente_XS, s.vertical_padding_XXS, {}]}>Poder tener acceso al planning manual y automatico</Text>
                            <Text style={[s.fuente_XS, s.vertical_padding_XXS, {}]}>Control de peso</Text>
                            <Text style={[s.fuente_XS, s.vertical_padding_XXS, {}]}>Crear tu super lista</Text>
                        </View>
                        <Button buttonStyle={{ marginHorizontal: 20 }} title='Comprar' type="outline" />
                    </View>
                    <View style={{ backgroundColor: 'white', borderRadius: 20, borderWidth: 1, width: window.width, height: 400 }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#008F7A', marginTop: 20, height: 70, width: '100%' }}>
                            <Text>Premium</Text>
                        </View>
                        <View style={{ marginHorizontal: 15, height: 250 }}>
                            <Text style={[s.fuente_M, { marginVertical: 15 }]}>€4,99/mes</Text>
                            <Text style={[s.fuente_XS, s.vertical_padding_XXS, {}]}>Descargar las recetas off-line</Text>
                            <Text style={[s.fuente_XS, s.vertical_padding_XXS, {}]}>Sin publicidad</Text>
                            <Text style={[s.fuente_XS, s.vertical_padding_XXS, {}]}>mas funcionalidades y objetivos</Text>
                        </View>
                        <Button buttonStyle={{ marginHorizontal: 20 }} title='Comprar' type="outline" />
                    </View>
                    <View style={{ backgroundColor: 'white', borderRadius: 20, borderWidth: 1, marginTop: 20, width: window.width, height: 400 }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#008F7A', marginTop: 20, height: 70, width: '100%' }}>
                            <Text>Premium Deluxe</Text>
                        </View>
                        <View style={{ marginHorizontal: 15, height: 250 }}>
                            <Text style={[s.fuente_M, { marginVertical: 15 }]}>€9,99/mes</Text>
                            <Text style={[s.fuente_XS, s.vertical_padding_XXS, {}]}>Descargar las recetas off-line</Text>
                            <Text style={[s.fuente_XS, s.vertical_padding_XXS, {}]}>control de peso</Text>
                            <Text style={[s.fuente_XS, s.vertical_padding_XXS, {}]}>Sin publicidad</Text>
                            <Text style={[s.fuente_XS, s.vertical_padding_XXS, {}]}>Planning automatico</Text>
                            <Text style={[s.fuente_XS, s.vertical_padding_XXS, {}]}>dietista/nutricionista</Text>
                        </View>
                        <Button buttonStyle={{ marginHorizontal: 20 }} title='Comprar' type="outline" />
                    </View>
                </Swiper>
                <Text style={[s.fuente_XS, { color: 'white' }]}>hora de comprar tu comida semanalmente</Text>
            </View>
        </KeyboardAvoidingView>
    );
}
*/
//export default Premium;


