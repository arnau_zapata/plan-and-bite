import React, { useState, useContext, useEffect } from 'react';
import { Image, Text, View, TextInput, TouchableOpacity, ImageBackground, Dimensions } from 'react-native';
import { Button } from 'react-native-elements';
import Estructura_Global from '../../Estructuras/Estructura_Global.json';
var s = require('../../style');
import SafeAreaView from 'react-native-safe-area-view';
import { Header } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import flecha_izquierda from '../../assets/icon_flecha-izquierda2.png'
import ToggleSwitch from 'toggle-switch-react-native'
import { comida_to_integer } from '../../Utils/utils_calendario'
import { getImageIngredient, getImageRecipe, getListaRecetasFirestore, getLocalRecipesFirebase, get_Ingredientes_Planificables, get_Premium_Features_Firebase } from '../../storage/storage'
import { NavigationInjectedProps } from 'react-navigation'
import { Receta, Ingrediente } from '../../type/type';
import ModalAnadirReceta from './components/ModalAnadirReceta';
import moment from 'moment'
import CONSTANT from '../../local/constant'
import { useTranslation } from 'react-i18next'
import Loading from '../../components/Loading';
import { AppContext } from '../../provider/provider';
import { Context } from '../../type/type';
import { translate } from '../../Utils/utils';
interface Props extends NavigationInjectedProps { };

const PlanningManual = (props: Props) => {

  const context: any = useContext(AppContext)
  console.log("context", context)
  const { t } = useTranslation();
  const search = require('../../assets/search.png');
  const icon_anadir = require('../../assets/icon_anadir.png');
  const { clone, getPosition_Estructure_in_Array } = require('../../Utils/utils')
  const { filtrar_nevera } = require('../../Utils/utils_nevera')
  const { anadir_receta_a_firestore, filtrar_comida, usarFiltros } = require('../../Utils/utils_planning')

  const [state, setState] = useState<any>({
    pre_texto_introducido: "",
    texto_introducido: "",
    modal: { visible: false, receta: -1, time: {} },
    ingredientes_planificables: [],
    lista_ingredientes_introducidos: [],
    original_lista_recetas: [],
    local_lista_recetas: [],
    original_lista_ingredientes: [],
    show_lista_recetas: [],
    show_lista_ingredientes: [],
    lista_filtros: [],
    switch_check: false,
    switch_check_recetas_locales: false,
    loading: true
  })
  const [featureVariasPersonas, setFeatureVariasPersonas] = useState(true)
  let year = props.navigation.getParam('year')
  let month = props.navigation.getParam('month')
  let day = props.navigation.getParam('day')
  let comida = props.navigation.getParam('comida')
  let plato = props.navigation.getParam('plato')

  const initializate = async () => {
    let lista_ingredientes_planificables = await get_Ingredientes_Planificables();

    let premiumFeatures = await get_Premium_Features_Firebase()
    setFeatureVariasPersonas(premiumFeatures && premiumFeatures.varias_personas && premiumFeatures.varias_personas > moment().format(CONSTANT.formatMoment))

    let total_lista_recetas: any = context.lista_recetas;

    total_lista_recetas = await usarFiltros(total_lista_recetas);
    total_lista_recetas = filtrar_comida(comida, total_lista_recetas);
    let lista_ingredientes = [...context.lista_ingredientes];
    let param_lista_ingredientes_introducidos = props.navigation.getParam('lista_ingredientes_introducidos');

    let local_lista_recetas = total_lista_recetas.filter((receta: any) => receta.local);
    local_lista_recetas = await Promise.all(local_lista_recetas.map(async (receta: Receta) => {
      return { ...receta, imagen_url: await getImageRecipe(receta) }
    }))
    const global_recetas = total_lista_recetas.filter((receta: any) => !receta.local);
    if (param_lista_ingredientes_introducidos != undefined) {
      param_lista_ingredientes_introducidos = await Promise.all(param_lista_ingredientes_introducidos.map(async (ingrediente: any) => {
        if (ingrediente.image) {
          const image_url = await getImageIngredient(ingrediente);
          return { ...ingrediente, image_url }
        }
        else return ingrediente
      }))
      let new_lista = clone(state.lista_filtros);
      new_lista.push((receta: any) => getPosition_Estructure_in_Array(receta.lista_ingredientes, param_lista_ingredientes_introducidos[0]) >= 0);
      let show_lista_recetas = await filter_list_recetas(global_recetas, state.switch_check, new_lista, state.texto_introducido)
      show_lista_recetas = show_lista_recetas.splice(0, 20)
      setState({
        ...state,
        lista_filtros: new_lista,
        lista_ingredientes_introducidos: param_lista_ingredientes_introducidos,
        original_lista_recetas: global_recetas,
        original_lista_ingredientes: lista_ingredientes,
        ingredientes_planificables: lista_ingredientes_planificables,
        show_lista_recetas,
        local_lista_recetas,
        loading: false,
        switch_check_recetas_locales: false,
      })

    } else {
      let show_lista_recetas = global_recetas.slice(0, 20)
      show_lista_recetas = await Promise.all(show_lista_recetas.map(async (receta: Receta) => {
        if (receta.imagen) {
          return { ...receta, imagen_url: await getImageRecipe(receta) }
        }
      }))

      setState({
        ...state,
        original_lista_recetas: global_recetas,
        original_lista_ingredientes: lista_ingredientes,
        ingredientes_planificables: lista_ingredientes_planificables,
        show_lista_recetas,
        local_lista_recetas,
        loading: false,
        switch_check_recetas_locales: false,
      })
    }
  }

  useEffect(() => {
    if (context.reset == true) {
      initializate();
      context.set_reset(context, false)
    }
  }, [context.reset]);


  useEffect(() => {
    initializate();
  }, []);


  const getView_Receta = (receta: Receta) => {
    return (

      <View>
        <TouchableOpacity
          style={{ marginHorizontal: '10%' }}
          onPress={() => {
            let last_screen = 'Anadir_Receta'
            props.navigation.navigate('Receta', { last_screen, year, month, day, comida, plato, receta_seleccionada: receta });
          }}>

          <ImageBackground style={[s.row, { margin: 10, width: '100%', height: 100, alignItems: "center", justifyContent: 'flex-start' }]} source={{ uri: receta.imagen_url }}>
            <View style={{ position: 'absolute', width: '100%', height: '100%', backgroundColor: 'rgba(128,128,128,0.5)' }} />
            <View style={[s.row, { padding: 10 }]}>
              <View style={[{ width: '70%', marginRight: 20 }]}>
                <Text style={[s.text_color_gray, s.fuente_M, { fontWeight: 'bold', textAlign: 'left' }]}>{translate(receta.nombre)} </Text>
              </View>
              <TouchableOpacity
                style={[{ borderRadius: 100, backgroundColor: 'white', padding: 2 }]}
                onPress={async () => {
                  let receta_seleccionada = receta;
                  if (receta_seleccionada != undefined) {
                    await anadir_receta_a_firestore(receta_seleccionada.id_receta, year, month, day, comida_to_integer(comida), plato, context)
                    props.navigation.navigate('TabsBottom');
                  }
                }}
                onLongPress={() => {
                  if (featureVariasPersonas) setState({ ...state, modal: { visible: true, receta: receta, time: { year, month, day, comida, plato } } })
                }}
              >
                <Image style={s.img_M} source={icon_anadir} />
              </TouchableOpacity>
            </View>
          </ImageBackground>
        </TouchableOpacity>
      </View>
    );

  }

  const getView_Ingrediente = (ingrediente: any) => {
    return (
      <TouchableOpacity onPress={async () => {
        let ingrediente_seleccionado = { ...clone(ingrediente), image_url: await getImageIngredient(ingrediente) };
        //anadir View Ingrediente
        let new_lista = clone(state.lista_ingredientes_introducidos);
        new_lista.push(ingrediente_seleccionado);
        //anadir ingrediente a la lista de filtros

        let new_lista_filtros = clone(state.lista_filtros);
        new_lista_filtros.push((receta: any) => getPosition_Estructure_in_Array(receta.lista_ingredientes, ingrediente_seleccionado) >= 0);
        let show_lista_recetas = await filter_list_recetas(state.original_lista_recetas, state.switch_check, new_lista_filtros, state.texto_introducido)
        show_lista_recetas = show_lista_recetas.splice(0, 20)
        Estructura_Global.ingrediente_seleccionado = "";
        Estructura_Global.receta_seleccionada = "";

        setState({ ...state, lista_ingredientes_introducidos: new_lista, lista_filtros: new_lista_filtros, texto_introducido: "", show_lista_recetas });
      }}>
        <ImageBackground style={[s.row, { marginHorizontal: '10%', marginVertical: 5, height: 100, alignItems: "center", justifyContent: 'flex-start' }]} source={{ uri: ingrediente.image_url }}>
          <View>
            <Text style={[s.text_color_gray, s.fuente_M, { backgroundColor: 'rgba(0,0,0,0.2)', fontWeight: 'bold', textAlign: 'right' }]}>{translate(ingrediente.nombre)} </Text>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    );

  }
  const generar_View_Lista_Recetas = () => {
    return (
      <View style={{ width: '100%', alignItems: 'center' }}>
        <Text style={s.fuente_M}>{t('anadir_receta.principal.receipts')}</Text>
        {state.show_lista_recetas.map((receta: Receta) => {
          let new_receta = getView_Receta(receta);
          return new_receta
        })}
      </View>
    );
  }

  const generar_View_Lista_Ingredientes = () => {
    if (state.texto_introducido != "") {

      return (
        <View style={{ width: '100%', }}>
          <Text style={s.fuente_M}>Ingredientes:</Text>
          {
            state.show_lista_ingredientes.map((ingrediente: any) => {
              let new_ingrediente = getView_Ingrediente(ingrediente);
              return new_ingrediente
            })
          }
        </View>
      );
    }
  }

  const generarView_Ingredientes_introducidos = () => {
    let views = [];
    let view_init;
    if (state.lista_ingredientes_introducidos.length == 0) {
      view_init = <View />
    }
    else {
      view_init = <Text>{t('anadir_receta.principal.selected_ingredients')}</Text>
    }
    for (let i = 0; i < state.lista_ingredientes_introducidos.length; i++) {
      views.push(
        <TouchableOpacity style={{ margin: 2, borderWidth: 1 }} onPress={async () => {
          let new_lista = clone(state.lista_ingredientes_introducidos);
          new_lista = new_lista.filter((obj_lista: any) => obj_lista != state.lista_ingredientes_introducidos[i]);
          const show_lista_recetas = await filter_list_recetas(state.original_lista_recetas, state.switch_check, new_lista, state.texto_introducido);
          setState({ ...state, lista_ingredientes_introducidos: new_lista, show_lista_recetas });
        }}
        >
          <Image style={s.img_S} source={{ uri: state.lista_ingredientes_introducidos[i].image_url }} />
        </TouchableOpacity>);
    }
    return <View>{view_init}<View style={s.row}>{views}</View></View>;
  }

  const CustomButton = (props: any) =>
    <TouchableOpacity onPress={() => {

      props.navigation.navigate('TabsBottom')
    }} >
      <Image source={flecha_izquierda} style={[s.img_S, { backgroundColor: 'rgba(250,250,250,0.0)', borderWidth: 1 }]} />
    </TouchableOpacity>

  const filter_list_recetas = async (lista_recetas: any[], switchNevera: boolean, lista_filtros: any[], texto_introducido: string) => {

    if (switchNevera) {
      lista_recetas = filtrar_nevera(lista_recetas, state.ingredientes_planificables);
    }

    if (texto_introducido != "") {
      let texto = texto_introducido.toLowerCase();
      lista_recetas = lista_recetas.map((receta: Receta) => { receta.nombre = translate(receta.nombre).toLowerCase(); return receta });
      lista_recetas = lista_recetas.filter((receta: Receta) => receta.nombre.includes(texto));
      lista_recetas = lista_recetas.map((receta: Receta) => { receta.nombre = translate(receta.nombre).charAt(0).toUpperCase() + translate(receta.nombre).slice(1); return receta });
    }

    lista_filtros.forEach((filtro) => {
      lista_recetas = lista_recetas.filter(filtro);
      return filtro;
    });
    console.log("lista_recetas", lista_recetas)

    lista_recetas = await Promise.all(lista_recetas.map(async (receta: Receta) => {
      return { ...receta, imagen_url: await getImageRecipe(receta), }
    }))
    console.log("lista_recetas", lista_recetas)
    return lista_recetas;
  }

  return (

    <View style={[s.appColor, { alignItems: "center" }]}>
      <Header
        placement="center"
        leftComponent={CustomButton(props)}
        centerComponent={{ text: 'Añadir Receta', style: { color: '#fff' } }}
      />
      {state.loading ?
        <Loading />
        : <View style={{ marginTop: 20, marginBottom: 20, alignItems: "center" }}>
          <View style={[s.row, { alignItems: 'center', width: '60%', marginTop: 20, borderWidth: 1, shadowOffset: { width: 2, height: 2 }, shadowColor: 'black', shadowOpacity: 0.8, }]}>
            <TextInput style={{ backgroundColor: 'white', width: '80%' }} onChangeText={(text) => setState({ ...state, pre_texto_introducido: text })} />
            <Button
              type="clear"
              icon={
                <Image style={s.img_XS} source={search} />
              }
              onPress={async () => {
                const lista_recetas = await filter_list_recetas(state.original_lista_recetas, state.switch_check, state.lista_filtros, state.pre_texto_introducido);
                let lista_ingredientes: any = state.original_lista_ingredientes;
                let texto = state.texto_introducido.toLowerCase();
                lista_ingredientes = lista_ingredientes.map((ingrediente: any) => { ingrediente.nombre = translate(ingrediente.nombre).toLowerCase(); return ingrediente });
                lista_ingredientes = lista_ingredientes.filter((ingrediente: any) => ingrediente.nombre.includes(texto));
                lista_ingredientes = lista_ingredientes.map((ingrediente: any) => { ingrediente.nombre = translate(ingrediente.nombre).charAt(0).toUpperCase() + translate(ingrediente.nombre).slice(1); return ingrediente });
                lista_ingredientes = await Promise.all(lista_ingredientes.map(async (ingrediente: any) => {
                  if (ingrediente.image) {
                    const image_url = await getImageIngredient(ingrediente);
                    return { ...ingrediente, image_url }
                  }
                  else return ingrediente
                }));
                setState({
                  ...state,
                  texto_introducido: state.pre_texto_introducido,
                  show_lista_recetas: lista_recetas,
                  show_lista_ingredientes: lista_ingredientes
                })
              }}
            />
          </View>
          <ToggleSwitch
            isOn={state.switch_check}
            onColor="green"
            offColor="gray"
            label={t('anadir_receta.principal.only_ingredients_fridge')}
            labelStyle={{ color: "black", fontWeight: "900" }}
            size="large"
            onToggle={async (isOn: any) => {
              const lista_recetas = await filter_list_recetas(state.original_lista_recetas, isOn, state.lista_filtros, state.texto_introducido);
              setState({ ...state, show_lista_recetas: lista_recetas, switch_check: isOn })
              console.log("changed to : ", isOn)
            }}
          />
          <ToggleSwitch
            isOn={state.switch_check_recetas_locales}
            onColor="green"
            offColor="gray"
            label={'Recetas locales'}
            labelStyle={{ color: "black", fontWeight: "900" }}
            size="large"
            onToggle={async (isOn: any) => {
              let lista_recetas = isOn ? state.local_lista_recetas : state.original_lista_recetas;
              lista_recetas = await filter_list_recetas(lista_recetas, state.switch_check, state.lista_filtros, state.texto_introducido);
              setState({ ...state, show_lista_recetas: lista_recetas, switch_check_recetas_locales: isOn })
              console.log("changed to : ", isOn)
            }}
          />
          <View>
            {generarView_Ingredientes_introducidos()}
          </View>
          <View style={{ height: Dimensions.get('window').height - 330, }}>
            <ScrollView >
              {generar_View_Lista_Ingredientes()}
              {generar_View_Lista_Recetas()}
            </ScrollView>
          </View>
          {state.switch_check_recetas_locales &&
            <Button title='añadir nueva receta (en local)' onPress={() => { props.navigation.navigate('AddData', { nextScreen: 'AnadirReceta', type: "recipes", local: true }); }} />
          }
          {/* <Text>hola</Text> */}
        </View>
      }
      <ModalAnadirReceta
        visible={state.modal.visible}
        receta={state.modal.receta}
        time={state.modal.time}
        callBack={() => { setState({ ...state, modal: { visible: false, receta: -1, time: {} } }) }}
        navigation={props.navigation}
      />
    </View>
  );
}

export default PlanningManual;


