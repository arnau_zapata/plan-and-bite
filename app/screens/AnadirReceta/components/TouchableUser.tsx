import React, { Component, useState } from 'react';
import { StyleSheet, TouchableOpacity, Text, View, Image, Alert } from "react-native";
import { Button } from 'react-native-elements';
import Estructura_Global from '../../../Estructuras/Estructura_Global.json';
import { Plato } from '../../../type/type';
import moment from 'moment'

var s = require('../../../style');
import logImg from './../../../assets/logImg.png';

type Props={
    name:string,
    index:number,
    selected:boolean|undefined,
    callBack:(x:number,y:boolean)=>void
}

const TouchableUser = (props: Props) => {
    const [touch,setTouch] = useState(props.selected?props.selected:false);

    return (
        <TouchableOpacity
            style={[{backgroundColor:touch?'rgba(0,0,0,0.2)':'white', marginTop: 10, borderRadius: 500, flexDirection:"row", alignItems:"center",justifyContent:"center", padding: 10, width:75,height:75,marginRight: 10 }]}
            onPress={()=>{
                props.callBack(props.index,!touch)
                setTouch(!touch);
            }}
        >
            <View>
                <Image source={logImg} style={{ height: 25, width: 25 }} />
                <Text style={{color:touch?'white':'black'}}>{props.name}</Text>
            </View>
        </TouchableOpacity>
    );
}





export default TouchableUser;