import React, { useContext, useEffect, useState } from 'react';
import { StyleSheet, TouchableOpacity, Text, View, Image, Alert } from "react-native";
import { Button } from 'react-native-elements';
import Estructura_Global from '../../../Estructuras/Estructura_Global.json';
import { Plato, Persona } from '../../../type/type';
import moment from 'moment'
import ModalPlanNBite from '../../../components/ModalPlanNBite';
import TouchableUser from './TouchableUser';
import CONSTANT from '../../../local/constant'
import { comida_to_integer } from '../../../Utils/utils_calendario'
import { anadir_receta_a_firestore } from '../../../Utils/utils_planning'
import { useTranslation } from 'react-i18next'
const {
    get_Premium_Features_Firebase,
    updateSelectedPerson,
    getPersonSelectedValues,
    setPendingPlanningFirebase,
} = require('../../../storage/storage')
import { AppContext } from '../../../provider/provider';
import { Context } from '../../../type/type';
var s = require('../../../style');


type Props = {
    visible: boolean,
    callBack: () => void,
    navigation: any,
    receta: any
    time: any
}

export default ({ visible, callBack, navigation, receta, time }: Props) => {
    const context:any = useContext(AppContext)
    const { t } = useTranslation();
    const [featureVariasPersonas, setFeatureVariasPersonas] = useState(true)
    const [lista_personas, setLsta_personas] = useState<any>([])
    const [error_message, set_error_message] = useState("")
    const initializate = async () => {

        let lista_personas = context.lista_personas;
        setLsta_personas(lista_personas)
        let premiumFeatures = await get_Premium_Features_Firebase()
        setFeatureVariasPersonas(premiumFeatures && premiumFeatures.varias_personas && premiumFeatures.varias_personas > moment().format(CONSTANT.formatMoment))
    }
    useEffect(() => {
        initializate()
    }, [])
    useEffect(() => {
        set_error_message("")
    }, [visible]);

    return (
        <ModalPlanNBite
            visible={visible}
            onPressCancel={() => callBack()}
        >
            <View style={{ backgroundColor: 'white' }}>
                <View>
                    <Text>{t('anadir_receta.modal.select_people')}</Text>
                    <View style={{ flexDirection: 'row' }}>
                        {lista_personas && lista_personas.map((persona: Persona, index: number) => {
                            return (
                                <TouchableUser name={persona.nombre} index={index} selected={persona.selected} callBack={async (i: number, touch: boolean) => {
                                    await updateSelectedPerson(i, touch, context)
                                }} />
                            )
                        })}
                    </View>
                    <Text>{error_message}</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>

                    <Button title='Cancel ' onPress={() => callBack()} />
                    <Button title='Enviar Receta' onPress={async () => {
                        console.log("hago cosas")
                        let { numberSelectedPerson, emails } = await getPersonSelectedValues(context)
                        if (numberSelectedPerson == 0 && featureVariasPersonas) {
                            set_error_message("Tienes que seleccionar una opcion")
                            return
                        } else {
                            let year = time.year;
                            let mes = time.month;
                            let dia = time.day;
                            let comida = time.comida;
                            let plato = time.plato;
                            console.log(year, mes, dia, comida, plato, receta)
                            if (emails.length > 0) await setPendingPlanningFirebase(emails, [{ receta, year, mes, dia, comida: comida_to_integer(comida), plato, quantity: numberSelectedPerson }]);

                            if (numberSelectedPerson > emails.length) {
                                anadir_receta_a_firestore(receta.id_receta, time.year, time.month, time.day, comida_to_integer(comida), plato, context);
                            }
                            callBack()
                            navigation.navigate('AccessStack');
                            navigation.navigate('TabsBottom');

                            // console.log('Planificando...');
                            // navigation.navigate('AccessStack');
                            // navigation.navigate('TabsBottom');
                            // //navigate(this.props,'Acceso');
                            // //navigate(this.props,'AppNavigation');
                        }
                    }} />
                </View>
            </View>
        </ModalPlanNBite>
    );
}


