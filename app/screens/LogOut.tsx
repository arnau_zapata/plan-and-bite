import React from "react";
import { Text, View, Button } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

const Logout = props => {
    return (
        <View style={{
            flex: 1,
            backgroundColor: 'green',
            alignItems: 'center',
            justifyContent: 'center',
            textAlign: "center"
        }}>
            {/*  <Button title='Log Out' onPress={()=> {props.navigation.navigate({routeName: 'AccessStack'})}}></Button>   */}
            <TouchableOpacity onPress={() => { props.navigation.navigate({ routeName: 'Acceso' }) }}>
                <Text style={{ color: 'white', textAlign: 'center' }}>Click me to Log Out</Text>
            </TouchableOpacity>
        </View>
    )
}

export default Logout;
