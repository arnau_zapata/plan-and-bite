import React, { useRef, useState, Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { CheckBox, Slider } from 'react-native-elements'
import { setUserConfigurationFirebase } from '../../storage/storage';
import { useTranslation } from 'react-i18next'

const Configuracion = () => {
  const { t } = useTranslation();


  let [presupuesto_checked, set_presupuesto_checked] = useState(false);
  let [tiempo_checked, set_tiempo_checked] = useState(false);
  let [vegetariano_checked, set_vegetariano_checked] = useState(false);
  let [val_presupuesto, set_val_presupuesto] = useState(0);
  let [val_tiempo, set_val_tiempo] = useState(0);

  const switch_checked = (checked: boolean, set_checked: (x: boolean) => void) => {
    set_checked(!checked);

  }

  return (
    <View>
      <CheckBox title={t("cuenta.configuracion.budget_limit")}
        checked={presupuesto_checked}
        onPress={() => { switch_checked(presupuesto_checked, set_presupuesto_checked); }}
      />
      {presupuesto_checked &&
        <View>
          <Slider
            value={val_presupuesto}
            maximumValue={200}
            onValueChange={value => { value = Math.floor(value); set_val_presupuesto(value) }}
          />
          <Text>{val_presupuesto}</Text>
        </View>
      }
      <CheckBox title={t("cuenta.configuracion.time_limit")}
        checked={tiempo_checked}
        onPress={() => { switch_checked(tiempo_checked, set_tiempo_checked); }}
      />
      {tiempo_checked &&
        <View>
          <Slider
            value={val_tiempo}
            maximumValue={60}
            onValueChange={value => { value = Math.floor(value); set_val_tiempo(value) }}
          />
          <Text>{val_tiempo}</Text>
        </View>
      }
      <CheckBox title={t("cuenta.configuracion.vegetarian")}
        checked={vegetariano_checked}
        onPress={() => { switch_checked(vegetariano_checked, set_vegetariano_checked); }}
      />
      <Button title='Save' onPress={() => {
        setUserConfigurationFirebase({
          vegetariano: vegetariano_checked,
          limite_presupuesto: presupuesto_checked,
          presupuesto_establecido: val_presupuesto,
          limite_tiempo: tiempo_checked,
          tiempo_establecido: val_tiempo
        });
      }
      } />
    </View>
  );
}


export default Configuracion;
