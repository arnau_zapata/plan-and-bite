import React, { useRef, useState, useContext, useEffect } from 'react';
import { StyleSheet, TouchableOpacity, Text, View, TextInput, Image, KeyboardAvoidingView, ImageBackground, Dimensions } from "react-native";
var s = require('../style');
import {
    get_Notifications_Firebase,
    deleteNotificationFirebase,
    setPendingPlanningFirebase
} from '../storage/storage';
import { Icon } from 'react-native-elements';
import { anadir_receta_a_firestore } from '../Utils/utils_planning';
import { Receta } from '../type/type';
import { AppContext } from '../provider/provider';

import { Context } from '../type/type';



export default (props: any) => {
    const context = useContext(AppContext)

    const [listNotifications, set_listNotifications] = useState([])
    const [loaded, set_loaded] = useState(false)

    const initializate = async () => {
        set_listNotifications(await get_Notifications_Firebase())
    }

    useEffect(() => {
        initializate()
    }, [])

    const onPressCheck = async (notification: any) => {
        if (notification.type == 'planning') {
            await Promise.all(notification.planning.map(async (receta: any) => {
                await anadir_receta_a_firestore(receta.receta, receta.year, receta.mes, receta.dia, receta.comida, receta.plato, context)
            }))
            await deleteNotificationFirebase(notification.time);
        }
    }

    const getViewRoundButton = (type: string, notification: any) => {
        let color = 'green';
        if (type == 'delete') color = 'rgba(200,0,0,1)'
        return (
            <TouchableOpacity style={{ paddingHorizontal: 20 }}
                onPress={() => {
                    if (type == 'check') {
                        onPressCheck(notification);
                    }
                    else {
                        deleteNotificationFirebase(notification.time);
                    }
                }}
            >
                <View style={{ borderColor: color, borderWidth: 3, borderRadius: 50, padding: 10 }}>
                    <Icon name={type} type='fontawesome' color={color} />
                </View>
            </TouchableOpacity>
        )
    }

    const getViewNotificacion = (notification: any) => {
        return (
            <View style={{ paddingVertical: 3, backgroundColor: 'rgba(0,0,0,0.05)' }}>
                <View
                    style={{ flexDirection: 'row', padding: 10, backgroundColor: 'white', justifyContent: 'space-between' }}
                >
                    <TouchableOpacity style={{ width: '50%', justifyContent: 'center' }}>
                        <Text>peticion de cosis</Text>
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'row' }}>
                        {getViewRoundButton("check", notification)}
                        {getViewRoundButton("delete", notification)}
                    </View>
                </View>
            </View>
        );
    }

    return (
        <View>
            {listNotifications.map((notification: any) => getViewNotificacion(notification))}
        </View>
    );

}

