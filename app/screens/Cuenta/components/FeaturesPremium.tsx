import React, { useRef, useState, Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { CheckBox, Slider } from 'react-native-elements'
import { set_Premium_Features } from '../../../storage/storage';

import { NavigationInjectedProps } from 'react-navigation'

interface Props extends NavigationInjectedProps { };

const Configuracion = (props: Props) => {
    //function Registro() {


    let [checkVariasPersonas, set_checkVariasPersonas] = useState(false);
    let [objetivoPeso, set_objetivoPeso] = useState(false);

    const switch_checked = (checked: boolean, set_checked: (x: boolean) => void) => {
        set_checked(!checked);

    }

    return (
        <View style={{ marginHorizontal: 20 }}>
            <Button
                title="Premium"
                onPress={() => {
                    props.navigation.navigate('Premium')
                }}
            />
            <CheckBox title='planificacion para varias personas'
                checked={checkVariasPersonas}
                onPress={() => { set_Premium_Features({ varias_personas: !checkVariasPersonas }); switch_checked(checkVariasPersonas, set_checkVariasPersonas); }}
            />
            {/* <CheckBox title='Objetivo de peso'
                checked={objetivoPeso}
                onPress={() => { set_Premium_Features({ objetivo_peso: !objetivoPeso }); switch_checked(objetivoPeso, set_objetivoPeso); }}
            /> */}

        </View>
    );
}


export default Configuracion;
