import React, { useRef, useState, useEffect, useContext } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, Alert, Button, Dimensions } from 'react-native';
import { get_Premium_Features_Firebase, get_Ingrediente, removePersonFirebase } from '../../../storage/storage';
import { Persona, Ingrediente } from '../../../type/type';
import { Icon } from 'react-native-elements'
import { NavigationInjectedProps } from 'react-navigation'
import ActionButton from 'react-native-action-button';
import moment from 'moment'
import CONSTANT from '../../../local/constant'
import Loading from '../../../components/Loading';
import { useTranslation } from 'react-i18next'
import { apiCallGet, translate } from '../../../Utils/utils';
import * as firebase from "@react-native-firebase/storage";
import { AppContext } from '../../../provider/provider';

interface Props extends NavigationInjectedProps { };
const ListaUsuarios = (props: Props) => {
    const context: any = useContext(AppContext)

    const { t } = useTranslation();
    const [state, setState] = useState({
        position: 0,
        lista_alergias: [],
        loading: true,
        featureVariasPersonas: false,
    })

    const init = async () => {
        let premiumFeatures = await get_Premium_Features_Firebase()
        const isFeatureVariasPersonas = premiumFeatures && premiumFeatures.varias_personas && premiumFeatures.varias_personas > moment().format(CONSTANT.formatMoment)
        setState({ ...state, loading: false, featureVariasPersonas: isFeatureVariasPersonas })
    }

    useEffect(() => {
        init()
    }, []);

    // useEffect(() => {
    //     let persona: Persona = context.lista_personas[state.position];
    //     if (!persona) return;
    //     // inicializarAlergias(persona)
    // }, [state.position]);

    const inicializarAlergias = async (persona: Persona) => {

        let lista = await Promise.all(persona.alergias.map(async (alergia: number) => await get_Ingrediente(alergia)));

        // setState(lista);
    }
    const generar_View_lista_usuarios = () => {
        let inside_view = context.lista_personas.map((persona: Persona, index: number) => {
            return (
                <View style={{ borderTopWidth: 1, width: '100%' }}>
                    <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <TouchableOpacity style={{ flexDirection: 'row', padding: 10, justifyContent: 'space-between', width: '90%' }}
                            onPress={async () => {
                                const position = (state.position !== index) ? index : -1;
                                let lista_alergias: any = await Promise.all(persona.alergias.map(async (alergia: number) => {
                                    const ingrediente: any = await get_Ingrediente(alergia);
                                    return ingrediente ? translate(ingrediente.nombre) : ""
                                }))

                                setState({ ...state, position, lista_alergias })
                            }}
                        >
                            <Text style={{ minWidth: 40 }}>{persona.nombre} {index == 0 && '(Tu)'} </Text>
                            <Text>edad: {persona.edad} </Text>
                            <Icon name={persona.gender === "Mujer" ? "venus" : "mars"} type='font-awesome' />
                            <Icon name={state.position === index ? "eye-slash" : "eye"} type='font-awesome' style={{ marginRight: 15 }} />
                        </TouchableOpacity>
                        <View style={{ flexDirection: "row", }}>
                            {index != 0 &&
                                <TouchableOpacity onPress={() => {
                                    Alert.alert(
                                        'Eliminar Persona',
                                        'Estas seguro que quieres eliminar a esta persona',
                                        [
                                            {
                                                text: 'No',
                                                onPress: () => console.log('Cancel Pressed'),
                                                style: 'cancel'
                                            },
                                            {
                                                text: 'Si', onPress: async () => {
                                                    await removePersonFirebase(persona, context);
                                                    setState({ ...state, position: -1 })

                                                }
                                            }
                                        ],
                                        { cancelable: false }
                                    );
                                }}>
                                    <Icon name={"trash"} type='font-awesome' />
                                </TouchableOpacity>}

                        </View>
                    </View>
                    {state.position === index && generar_View_persona()}
                </View >
            );
        });
        let view = (
            <View style={{ borderWidth: 1, alignItems: 'center', justifyContent: 'space-between' }}>
                {inside_view}
            </View>
        );
        return view;
    }

    const renderAttrPersona = (text: string, iconNameArray: string[]) => {
        return (
            <View style={{ flexDirection: "row", marginTop: 5 }}>
                {iconNameArray.map((iconName) => {
                    return <Icon name={iconName} type='font-awesome' />
                })}
                <Text> {text}</Text>
            </View>
        )
    }

    const generar_View_persona = () => {
        let persona: Persona = context.lista_personas[state.position];
        console.log("context.lista_personas",context.lista_personas)
        let inside_view;
        inside_view = (
            <View style={{ borderWidth: 1, borderTopColor: 'rgba(0,0,0,0.2)', }}>
                <Text style={{ alignSelf: "center" }}> Hola, {persona && persona.nombre} </Text>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ padding: 10, width: '50%' }}>
                        {persona && persona.edad && renderAttrPersona(`${t("cuenta.lista_usuarios.age")} ${persona && persona.edad} años`, ["birthday-cake"])}
                        {persona && persona.gender && renderAttrPersona(`${t("cuenta.lista_usuarios.gender")} ${persona && persona.gender}`, ["venus-mars"])}
                        {persona && persona.peso && renderAttrPersona(`${t("cuenta.lista_usuarios.weight")} ${persona && persona.peso} Kg`, ["balance-scale"])}
                        {persona && persona.altura && renderAttrPersona(`${t("cuenta.lista_usuarios.height")} ${persona && persona.altura} m`, ["arrows-v", "male"])}
                        {persona && persona.nivel_ejercicio && renderAttrPersona(`${t("cuenta.lista_usuarios.exercise_level")} ${persona && persona.nivel_ejercicio}`, ["arrows-v", "male"])}
                    </View>

                    <View>
                        <View style={{ flexDirection: "row", alignItems: 'center' }}>
                            {persona && persona.imported && <Text style={{ marginTop: 10, backgroundColor: "yellow", padding: 5 }}>persona importada</Text>}
                            {(!persona || (persona && !persona.imported)) &&
                                <TouchableOpacity onPress={() => {
                                    props.navigation.navigate("Perfil", { persona_seleccionada: persona, index: state.position, last_screen: 'Cuenta' });
                                }}>
                                    <Icon name={"edit"} type='font-awesome' style={{ fontSize: 35 }} />
                                </TouchableOpacity>}
                        </View>
                    </View>
                </View>
                {state.lista_alergias.length > 0 && <View style={{ flexDirection: 'row' }}>
                    <Text> alergias: </Text>
                    {state.lista_alergias.map((ingrediente: string) => <Text style={{ marginRight: 5 }}>{ingrediente},</Text>)}
                </View>}
            </View>
        );
        return <View>{inside_view}</View>;
    }
    return (
        <View style={{ height: Dimensions.get('window').height }}>
            {state.loading
                ? <Loading />
                : <View style={{ height: '100%' }}>
                    {state.featureVariasPersonas && generar_View_lista_usuarios()}
                    {!state.featureVariasPersonas && generar_View_persona()}
                    {state.featureVariasPersonas && <ActionButton
                        buttonColor="rgba(231,76,60,1)"
                        onPress={() => {
                            props.navigation.navigate("Perfil", { persona_seleccionada: {}, index: context.lista_personas.length, last_screen: 'Cuenta' });
                        }}
                    />}
                </View>
            }
        </View>
    );
}


export default (ListaUsuarios);
