import React, { useRef, useState, Component } from 'react';
import { StyleSheet, Text, View, } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icon } from 'react-native-elements'

type Props = {
    component?: any,
    title: string,
    onPress?:()=>void,
}

const Configuracion = (props: Props) => {


    let [option_visible, set_option_visible] = useState(false);


    return (
        <View>
            <TouchableOpacity style={{backgroundColor:'rgba(0,0,0,0.1)',flexDirection:"row",justifyContent:'space-between',padding:10, borderBottomWidth:1, borderTopWidth:1}} onPress={() => props.onPress?props.onPress():set_option_visible(!option_visible)}>
                <Text>{props.title}</Text>
                <Icon name={option_visible ? "chevron-down":"chevron-right"  } type='font-awesome'/>
            </TouchableOpacity>
            {option_visible &&
                <View style={{paddingVertical:10}}>
                    <View style={{ marginHorizontal: 20,   }}>
                        {props.component}
                    </View>
                </View>
            }
        </View>
    );
}


export default Configuracion;
