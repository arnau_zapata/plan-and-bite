import React, { useState, useEffect } from "react";
import { Text, View, Image, TouchableOpacity, Dimensions } from "react-native";
import { Button, CheckBox, ButtonGroup } from 'react-native-elements';
// import { TouchableOpacity } from "react-native-gesture-handler";
import { Header } from 'react-native-elements';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import { Icon } from 'react-native-elements'

import OptionDropBox from './components/OptionDropBox'
import { useTranslation } from 'react-i18next'


// import logImg from '../../assets/logImg.png';

import {
    deleteEmailUserFirebase, getIsAdminFirebase,
} from '../../storage/storage';
import { ScrollView } from "react-native-gesture-handler";
import { Persona } from "../../type/type";


var s = require('../../style');
const Cuenta = (props: any) => {
    const { t } = useTranslation();
    const [isAdmin, setIsAdmin] = useState(false)

    const initialization = async () => {
        const isAdmin = await getIsAdminFirebase();
        setIsAdmin(isAdmin)
    }
    useEffect(() => {
        initialization()
    }, [])

    return (
        <View style={{ justifyContent: 'space-between', height: Dimensions.get('window').height - 100 }}>
            <View>
                <Header
                    placement="center"
                    centerComponent={{ text: 'Cuenta', style: { color: '#fff' } }}
                />
                <ScrollView style={{}}>
                    <OptionDropBox title={t("cuenta.principal.user_list")} onPress={() => props.navigation.navigate("ListaUsuarios")} />
                    <OptionDropBox title={t("cuenta.principal.tracing")} onPress={() => props.navigation.navigate("Seguimiento")} />
                    {/* <OptionDropBox title={t("cuenta.principal.setting")} onPress={() => props.navigation.navigate("Configuracion")} /> */}
                    <OptionDropBox title={t("cuenta.principal.premium")} onPress={() => props.navigation.navigate("Premium")} />
                    {/* <OptionDropBox title={t("cuenta.principal.notifications")} onPress={() => props.navigation.navigate("Notificaciones")} /> */}
                    {/* <OptionDropBox title='Nutricionistas' onPress={() => props.navigation.navigate("Nutricionistas")} /> */}
                    <OptionDropBox title='Pagos' onPress={() => props.navigation.navigate("Pagos")} />
                    {isAdmin &&
                        <OptionDropBox title='Validation' onPress={() => props.navigation.navigate("FreeBitecoins",{nextScreen:"ValidateData"})} />
                    }
                </ScrollView>
            </View>
            <Button title='Cerrar Sesion' onPress={async () => {
                await deleteEmailUserFirebase();
                props.navigation.navigate("Login")
            }}
            />
        </View>
    )
}

export default Cuenta;
