import React, { useRef, useState, useContext, useEffect } from "react";
import { StyleSheet, Text, View, TextInput, Image, KeyboardAvoidingView, ImageBackground, Dimensions, Alert, AsyncStorage } from "react-native";
import { Button } from 'react-native-elements';
import { TouchableOpacity } from "react-native-gesture-handler";
import axios from 'axios';
import { IsEmailAndPasswordValid } from '../storage/firebase'
import Estructura_Global from '../Estructuras/Estructura_Global.json'
import {
    clearAllData,
    getEmailUserFirebase,
    setEmailUserFirebase,
} from '../storage/storage';
import TextInputPlanNBite from '../components/TextInputPlanNBite'

import * as Keychain from 'react-native-keychain';
import { AppContext } from '../provider/provider';
import { Context } from '../type/type';
//needed for facebook button
//import { LoginButton } from 'react-native-fbsdk';
//Imported Style
var s = require('../style');

let textInput: any = {}
//Unused imports
import PlanAndBiteLogo from '../assets/Plan_n_BiteLogo.png';
import { loginFirebase } from "../Utils/utils_auth";


const Login = (props: any) => {
    const context: Context = useContext(AppContext)
    const [loading,setLoading] = useState(false)
    const [loadingText,setloadingText] = useState("")
    const [name, setName] = useState("");
    const [pass, setPass] = useState("");

    const [state_err_username, set_state_err_username] = useState("");
    const [state_err_password, set_state_err_password] = useState("");


    const init = async () => {
        try {
            // Retreive the credentials
            const credentials = await Keychain.getGenericPassword();
            if (credentials && credentials.username && credentials.username.length > 0) {
                setName(credentials.username);
                setPass(credentials.password);
                // signupFirebase(credentials.username, credentials.password)
            } else {
                console.log('No credentials stored')
            }
        } catch (error) {
            console.log('Keychain couldn\'t be accessed!', error);
        }
    }

    useEffect(() => {
        init();
    }, []);

    const queryData = async () => {
        setLoading(true)
        console.log("queryData")
        clearAllData();
        console.log('queryData ejecuted')
        console.log('Name: ' + name);
        console.log('Pass: ' + pass);
        let email = name.toLocaleLowerCase();
        email = email.replace(new RegExp(" ", 'g'), "")
        Estructura_Global.email = email;
        await setEmailUserFirebase(email)
        console.log("You are inside the app");
        await context.updateFromFirebase(false,setloadingText)
        props.navigation.navigate('TabsBottom');



    }


    const access = async () => {
        console.log("access")
        Keychain.setGenericPassword(name, pass)

        let loginSuccess = false;
        let error = ""
        const result = await loginFirebase(name.trim(), pass)
        if (result != undefined && result.user != undefined) {
            loginSuccess = true;
        }
        else {
            loginSuccess = false;
            error = result;
        }

        if (loginSuccess) {
            console.log(`User account with password created & signed in!`);
            setEmailUserFirebase(name.trim())
            await queryData();
            // props.navigation.navigate('TabsBottom')
        }
        else {
            console.log("error", error)
        }
    }

    const focusNextField = (nextField: string) => {
        textInput[nextField].focus();
    }

    return (
        <ImageBackground style={[{ justifyContent: "center", height: '100%', paddingHorizontal: 40 }]} source={require('../assets/Screen_AcessStack_00.png')}>
            <KeyboardAvoidingView behavior="padding" enabled>
                <Image style={{ width: 250, height: 250, }} source={PlanAndBiteLogo} />
                <View style={[s.form, { marginTop: 20 }]}>
                    <TextInputPlanNBite
                        setText={setName}
                        error={state_err_username}
                        text={name}
                        placeholder='Username'
                        focusNextField={() => focusNextField("password")}

                    />
                    <TextInputPlanNBite
                        setText={setPass}
                        error={state_err_password}
                        text={pass}
                        placeholder='Password'
                        password={true}
                        reference={input => {
                            textInput["password"] = input;
                        }}
                        focusNextField={access}
                    />
                </View>

                <View>
                    <Button
                        buttonStyle={{ backgroundColor: "white", borderRadius: 10, marginTop: 50, marginBottom: 20, width: 300 }}
                        onPress={access}
                        loading={loading}
                        loadingProps={{ color: 'black'}}
                        icon={
                            <Text style={[{ color: "black", fontWeight: 'bold' }]}>
                                Login
                            </Text>
                        }
                    />
                    <Text style={{color:'white'}}>{loadingText}</Text>
                </View>

                <View style={[s.row, { marginTop: 20, alignItems: 'center', justifyContent: 'center' }]}>
                    <Text style={s.text_color_gray}>¿No tienes cuenta?</Text>
                    <TouchableOpacity onPress={() => { props.navigation.navigate({ routeName: 'Registro' }) }}>
                        <Text style={{ color: 'red', marginLeft: 20 }}>Registrate</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        </ImageBackground>
    )
}

export default Login;