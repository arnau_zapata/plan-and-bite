import React, { useState, useEffect } from "react";
import { Text, View, ImageBackground, KeyboardAvoidingView, Image, TouchableOpacity } from "react-native";
import { Persona } from "../../../type/type";



type Props={
    optionSelected:number
    setOptionSelected:(x:number)=>void
    listPersona:Persona[]
    fillInput:(x:Persona)=>void
}


const PerfilSelection = (props: Props) => {


  const getViewButton = (item: Persona, index: number) => {
    return (
      <TouchableOpacity
        style={{ width: 100, height: 75, padding: 5, borderWidth: 1, backgroundColor: props.optionSelected == index ? 'rgba(0,0,0,0.1)' : 'white', justifyContent: 'center', alignItems: 'center' }}
        onPress={() => {
          props.setOptionSelected(index);
          props.fillInput(item)
        }}
      >
        <Text>{item && item.nombre ? item.nombre : 'nueva persona'}</Text>
      </TouchableOpacity>
    )
  }

  return (
    
        <View style={{ flexDirection: 'row', backgroundColor: 'white', borderBottomWidth: 1 }}>
          {props.listPersona.map(getViewButton)}
          {getViewButton(undefined, props.listPersona.length)}
        </View>
  )
}

export default PerfilSelection;
