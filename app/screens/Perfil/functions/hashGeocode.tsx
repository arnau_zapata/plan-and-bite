import { Place } from "../../../type/type";
import axios from 'axios'
import { gMapsApiKey } from "../../../local/constant";

const getRefQuery = (key: string) => {
    if (key === "province") return "administrative_area_level_2";
    if (key === "region") return "administrative_area_level_1"
    return key
}

const getUrlValue = (value: string) => {
    return value.replace(new RegExp(' ', 'g'), '+')
}

const getURL = (location: string, referenceLocation: any) => {
    let query = `https://maps.googleapis.com/maps/api/geocode/json?components=locality:${getUrlValue(location)}`
    Object.entries(referenceLocation).forEach(([key, value]: any) => {
        const refQuery = getRefQuery(key);
        query += `|${refQuery}:${getUrlValue(value)}`
    })

    query += `&key=${gMapsApiKey}&language=es`;
    return query
}

const getHashLocation = async (location: string, referenceLocation: any) => {
    const query = getURL(location, referenceLocation)
    const response: any = await axios.get(query)
    return response.data.results[0].place_id
}

export const hashGeocode = async (currentPlace: Place) => {
    currentPlace.city
    const hashResult: any = {}
    const city = currentPlace.city;
    const province = currentPlace.province;
    const region = currentPlace.region;
    const country = currentPlace.country;
    hashResult.city = await getHashLocation(city, { province, region, country })
    hashResult.province = await getHashLocation(province, { region, country })
    hashResult.region = await getHashLocation(region, { country })
    hashResult.country = await getHashLocation(country, {})
    return hashResult
}