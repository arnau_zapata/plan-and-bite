
import { PermissionsAndroid, Platform } from 'react-native'
import Geolocation from 'react-native-geolocation-service';
import axios from 'axios'
import { gMapsApiKey } from '../../../local/constant';

const getProvinceCode = (province: string) => {
    switch (province) {
        case "Barcelona": return "B";
        default: return "B";
    }
}

const fillAddress = (addressGeocode: any, useGeocode: boolean) => {
    let geometry = addressGeocode.geometry;
    let addressComponent = addressGeocode.address_components;
    let location = geometry.location;

    let location_type: string = geometry.location_type;
    let newAddress = {
        address: '',
        city: 'Barcelona',
        country: 'Espana',
        countryCodeV2: 'ES',
        latitude: null,
        longitude: null,
        province: '',
        provinceCode: 'B',
        region: "",
        zip: '',
    };
    let aux;
    aux = addressComponent.filter((component: any) => component.types.includes("street_number"))[0];
    let streetNum = aux != undefined ? ", " + aux.long_name : "";

    aux = addressComponent.filter((component: any) => component.types.includes("route"))[0];
    let street = aux != undefined ? aux.long_name + streetNum : "";

    if (street == "") {
        aux = addressComponent.filter((component: any) => component.types.includes("sublocality"))[0];
        street = aux != undefined ? aux.long_name + streetNum : "";
    }
    if (street == "") {
        aux = addressComponent.filter((component: any) => component.types.includes("neighborhood"))[0];
        street = aux != undefined ? aux.long_name + streetNum : "";
    }

    aux = addressComponent.filter((component: any) => component.types.includes("locality"))[0];
    let city = aux != undefined ? aux.long_name : "";

    aux = addressComponent.filter((component: any) => component.types[0] == "administrative_area_level_2")[0];
    let province = aux != undefined ? aux.long_name : "";

    aux = addressComponent.filter((component: any) => component.types[0] == "administrative_area_level_1")[0];
    let region = aux != undefined ? (aux.long_name) : "";

    aux = addressComponent.filter((component: any) => component.types[0] == "country")[0];
    let country = aux != undefined ? aux.long_name : "";

    aux = addressComponent.filter((component: any) => component.types[0] == "country")[0];
    let countryCodeV2 = aux != undefined ? aux.short_name : "";
    aux = addressComponent.filter((component: any) => component.types[0] == "postal_code")[0];
    let zip = aux != undefined ? aux.long_name : "";

    let latitude = location.lat;
    let longitude = location.lng;
    newAddress.address = street;
    newAddress.city = city;
    newAddress.province = province;
    newAddress.region = region;
    newAddress.country = country;
    newAddress.countryCodeV2 = countryCodeV2;
    newAddress.zip = zip;
    newAddress.latitude = latitude;
    newAddress.longitude = longitude;
    if (useGeocode) {
        switch (location_type) {
            case "ROOFTOP":
                newAddress.address = street;
                newAddress.city = city;
                newAddress.province = province;
                newAddress.country = country;
                newAddress.countryCodeV2 = countryCodeV2;
                newAddress.zip = zip;
                newAddress.latitude = latitude;
                newAddress.longitude = longitude;
                newAddress.region = region;
                break;
            case "RANGE_INTERPOLATED":
                newAddress.address = street;
                newAddress.city = city;
                newAddress.province = province;
                newAddress.country = country;
                newAddress.region = region;
                newAddress.countryCodeV2 = countryCodeV2;
                newAddress.zip = zip;
                break;
            case "GEOMETRIC_CENTER":
                newAddress.address = street;
                newAddress.city = city;
                newAddress.province = province;
                newAddress.region = region;
                newAddress.country = country;
                newAddress.countryCodeV2 = countryCodeV2;
                newAddress.zip = zip;
                break;
            case "APPROXIMATE":
                newAddress.city = city;
                newAddress.province = province;
                newAddress.region = region;
                newAddress.country = country;
                newAddress.countryCodeV2 = countryCodeV2;
                newAddress.zip = zip;
                break;
            default:
                break;
        }
    }
    return newAddress;
}



const findActualAddress = async (position: any, callback?: (x: any) => void) => {
    console.log("position", position)
    let lat = position.coords.latitude;
    let long = position.coords.longitude;
    const latlng = lat + "," + long;
    try {
        const url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latlng}&key=${gMapsApiKey}&language=es`
        console.log(url)
        const response = await axios.get(url)
        console.log("response", response.data.results[0])
        if (response.data.status !== 'OK') {

        }

        let newAddress = fillAddress(response.data.results[0], true);
        console.log("address", newAddress)
        return newAddress
    } catch (e) {
        console.log('Error finding address: ' + e)
        throw e
    }
}

const getActualAddress = async (callback?: (x: any) => void) => {
    const result = await Geolocation.getCurrentPosition(async (position: any) => {
        const result = await findActualAddress(position, callback)

        callback && await callback(result)
        return result
    }, (error: any) => {
        console.log(error.code, error.message);
    },
        { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
    return result
}

export const geocode = async (callback?: (x: any) => void) => {
    try {
        console.log(Platform.OS, Platform.OS === 'android')
        if (Platform.OS === 'android') {
            let granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                const result = await getActualAddress(callback)
                return result
            }
            else {

            }
        }
        else {
            return await getActualAddress(callback)
        }
    } catch (e) {
        console.log(e)
    }

}
