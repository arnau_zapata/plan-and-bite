import React, { useState, useEffect, useContext } from "react";
import { Text, View, ImageBackground, KeyboardAvoidingView, Image, TouchableOpacity } from "react-native";
import { Button, CheckBox, Slider } from 'react-native-elements';
import { Header } from 'react-navigation-stack';
import Estructura_Global from '../../Estructuras/Estructura_Global.json'
import TextInputPlanNBite from '../../components/TextInputPlanNBite'
import {
  getListIngredientsFirebase,
  getUserFirebase,
  addPersonFirebase,
  importPersonFirebase,
  addImportedPersonFirebase, getPlacesFirebase, getCurrentPlaceFirebase, setCurrentPlaceFirebase
} from '../../storage/storage';
import { getUser } from "../../storage/firebase";
import { Persona } from "../../type/type";
import { AsyncStorage } from 'react-native';
import { NavigationInjectedProps } from 'react-navigation'
import PerfilSelection from './components/PerfilSelection'
import { ScrollView } from "react-native-gesture-handler";
import moment from 'moment'
import CONSTANT, { Docs } from '../../local/constant'


import {
  get_Premium_Features_Firebase,
  updatePesoFirebase,
} from '../../storage/storage'
import TapButton from "../../components/TapButton";
import { geocode, hashGeocode } from "./functions";
import { AppContext } from '../../provider/provider';
import { translate } from "../../Utils/utils";

interface Props extends NavigationInjectedProps { }

var s = require('../../style');
const FirstPage = (props: Props) => {
  const context: any = useContext(AppContext)
  const aux_persona_seleccionada = props.navigation.getParam('persona_seleccionada');
  const [persona_seleccionada, setPersona_seleccionada] = useState(
    aux_persona_seleccionada ?
      { ...aux_persona_seleccionada }
      : {}
  )
  const [state, setState] = useState<any>({
    listPersona: [],
    listIngredients: [],
    error: {

    },
    listIngredientsAlergySelected: []
  });
  const [places, setPlaces] = useState<any>({
    city: [],
    province: [],
    region: [],
    country: [],
  })
  const [optionSelected, setOptionSelected] = useState(0);
  const [checkImportPerson, setcheckImportPerson] = useState(false);
  const [current_place, set_current_place] = useState({ current_place: {}, current_place_name: {} })
  const last_screen = props.navigation.getParam('last_screen');
  const index = props.navigation.getParam('index');
  let [peso_checked, set_peso_checked] = useState(false);


  const initialization = async () => {
    const current_place = await getCurrentPlaceFirebase()
    console.log("current_place", current_place)
    set_current_place(current_place)
    // AsyncStorage.removeItem('personas')
    const features = await get_Premium_Features_Firebase();
    set_peso_checked(features.objetivo_peso != undefined && features.objetivo_peso && features.objetivo_peso > moment().format(CONSTANT.formatMoment))

    persona_seleccionada.imported && setcheckImportPerson(true)
    // persona_seleccionada.email && setPersona_seleccionada({...persona_seleccionada,altura:text})(persona_seleccionada.email)
    let listIngredients = await getListIngredientsFirebase()
    listIngredients = listIngredients.map((item: any) => { return { id: item.id_ingrediente, name: item.nombre } })
    let listPersons = context.lista_personas
    setState({ ...state, listPersona: listPersons, listIngredients: listIngredients })
    setOptionSelected(index ? parseInt(index) : 0)
    fillInput(persona_seleccionada ? persona_seleccionada : state.listPersona[0])

    let listPlaces = await getPlacesFirebase(Docs.country)
    // listPlaces = listPlaces.map((obj)=>obj.placeName.es)
    console.log("listPLaces", listPlaces)
    setPlaces({ ...places, country: listPlaces })

  }

  const addNewIngredient = (id: number) => {
    setState({
      ...state,
      listIngredients: state.listIngredients.filter((item: any) => item.id != id),
      listIngredientsAlergySelected: [...state.listIngredientsAlergySelected, state.listIngredients.find((item: any) => item.id == id)]
    })
  }

  const removeIngredient = (id: number) => {
    const list: any[] = [...state.listIngredients, state.listIngredientsAlergySelected.find((item: any) => item.id == id)]
    setState({
      ...state,
      listIngredients: list,
      listIngredientsAlergySelected: state.listIngredientsAlergySelected.filter((item: any) => item.id != id)
    })
  }

  let textInput: any = {}

  const focusNextField = (nextField: string) => {
    textInput[nextField].focus();
  }

  useEffect(() => {
    initialization()

  }, [])

  const getViewSelectedItems = () => {
    return (
      <View style={{ flexDirection: 'row' }}>
        {state.listIngredientsAlergySelected.map((item: any) => {
          return (
            <TouchableOpacity
              style={{ flexDirection: 'row', backgroundColor: 'rgba(0,0,0,0.5)', marginRight: 5, padding: 3 }}
              onPress={() => removeIngredient(item.id)}
            >
              <Text style={{ color: '#fff', marginRight: 5 }}>{item && item.name ? item.name : ""}</Text>
              <Text style={{ color: '#fff' }}>X</Text>
            </TouchableOpacity>
          );
        })}
      </View>
    )
  }


  const save = async () => {
    const user: any = await getUserFirebase();
    const nameUser = optionSelected == 0 ? user.nombre : persona_seleccionada.nombre;
    console.log(state.listIngredientsAlergySelected)
    const attr = {
      ...persona_seleccionada,
      altura: parseInt(persona_seleccionada.altura),
      nombre: nameUser,
      edad: parseInt(persona_seleccionada.edad),
      peso_ideal: persona_seleccionada.peso_ideal != undefined ? persona_seleccionada.peso_ideal : persona_seleccionada.peso,
      alergias: state.listIngredientsAlergySelected.map((item: any) => item.id),
    }
    await addPersonFirebase(attr, optionSelected, context);
    await setCurrentPlaceFirebase(current_place)
    await updatePesoFirebase(parseInt(persona_seleccionada.peso), context)
    let lista = context.lista_personas
    setState({ ...state, listPersona: lista })
    setOptionSelected(lista.length)
    fillInput(state.listPersona[lista.length])
  }

  const fillEmptyInput = async () => {
    setPersona_seleccionada({})
    let list = await getListIngredientsFirebase()
    list = list.map((item: any) => { return { id: item.id_ingrediente, name: translate(item.nombre) } })
    setState({
      ...state,
      listIngredientsAlergySelected: [],
      listIngredients: list
    });

  }

  const fillInput = async (item: Persona) => {
    try {
      if (item == undefined) {
        fillEmptyInput()
      }
      else {
        setPersona_seleccionada(item);
        let list = await getListIngredientsFirebase()
        const listSelected = item.alergias
          ? item.alergias.map(id => {
            let ingrediente = list.find((item: any) => item.id_ingrediente === id)
            return { id: ingrediente.id_ingrediente, name: ingrediente.nombre }
          })
          : [];
        list = list.map((item: any) => { return { id: item.id_ingrediente, name: translate(item.nombre) } })

        listSelected.forEach(itemSel => list = list.filter((item: any) => item.id != itemSel.id))
        setState({
          ...state,
          listIngredients: list,
          listIngredientsAlergySelected: listSelected
        })

      }
    } catch (e) {
      fillEmptyInput()
    }
  }

  const confidentString = (str: string) => {
    try {
      if (str != undefined) return str.toString()
      else return "";
    } catch (e) {
      return ""
    }
  }
  const getCurrentPlaceText = (current_place: any) => {
    const address = current_place.address;
    const city = current_place.city;
    const province = current_place.province;
    const country = current_place.country;
    return `${address},${city},${province},${country}`
  }

  const getViewPersonSelected = () => {
    return (
      <View>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <TextInputPlanNBite keyboardType='phone-pad'
            error={state.error.error}
            text={confidentString(persona_seleccionada.edad)}
            setText={(text) => setPersona_seleccionada({ ...persona_seleccionada, edad: text })}
            placeholder='edad'
            focusNextField={() => focusNextField("weight")}
          />
          <Text style={{ color: '#fff' }}>Años</Text>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <TextInputPlanNBite keyboardType='phone-pad'
            error={state.error.error}
            text={confidentString(persona_seleccionada.altura)}
            setText={(text) => setPersona_seleccionada({ ...persona_seleccionada, altura: text })}
            placeholder='altura'
            focusNextField={() => focusNextField("weight")}
          />
          <Text style={{ color: '#fff' }}>Cm</Text>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <TextInputPlanNBite
            keyboardType='phone-pad'
            error={state.error.error}
            text={confidentString(persona_seleccionada.peso)}
            setText={(text) => setPersona_seleccionada({ ...persona_seleccionada, peso: text })}
            placeholder='peso'
            reference={input => {
              textInput["weight"] = input;
            }}
          />

          <Text style={{ color: '#fff' }}>Kg</Text>
        </View>
        <TextInputPlanNBite
          error={state.error.errorNombre}
          text={confidentString(persona_seleccionada.nombre)}
          setText={(text) => setPersona_seleccionada({ ...persona_seleccionada, nombre: text })}
          placeholder='nombre'
        />
        <TextInputPlanNBite
          dropDownData={['Mujer', 'Hombre']}
          error='' text={confidentString(persona_seleccionada.gender)}
          setText={(text) => setPersona_seleccionada({ ...persona_seleccionada, gender: text })}
          placeholder='sexo'
        />

        <TextInputPlanNBite
          dropDownData={['Ninguno', 'Vegetariano', 'Vegano']}
          text={persona_seleccionada.noCome}
          error=''
          setText={(text) => setPersona_seleccionada({ ...persona_seleccionada, noCome: text })}
          placeholder='estilo alimenticio'
        />
        <TextInputPlanNBite
          dropDownData={['Nada', 'Poco', 'Normal', 'Mucho']}
          text={persona_seleccionada.nivel_ejercicio}
          error=''
          setText={(text) => setPersona_seleccionada({ ...persona_seleccionada, nivel_ejercicio: text })}
          placeholder='Nivel ejercicio'
        />
        {getViewSelectedItems()}
        <TextInputPlanNBite
          dropDownSearchableData={state.listIngredients}
          error=''
          setText={addNewIngredient}
          placeholder='alergias'
        />
        {peso_checked &&
          <View>
            <Text>Objetivo de peso:</Text>
            <View style={{}}>
              <Slider
                value={persona_seleccionada.peso_ideal ? persona_seleccionada.peso_ideal : 0}
                maximumValue={100}
                onValueChange={value => { value = Math.floor(value); setPersona_seleccionada({ ...persona_seleccionada, peso_ideal: value }) }}
              />
              <Text>{persona_seleccionada.peso_ideal ? persona_seleccionada.peso_ideal : 0} KG</Text>
            </View>
          </View>
        }
        <Button title='Obtener direccion actual' onPress={async () => { await geocode(callback); }} />
        <View style={{}}>
          <Text style={{color:'white'}}>direccion actual:</Text>
          <Text style={{color:'white'}}>{current_place && current_place.current_place_name && Object.entries(current_place.current_place_name).length != 0 ? getCurrentPlaceText(current_place.current_place_name) : ""}</Text>
        </View>
      </View>
    )
  }

  const getViewImportPerson = () => {
    return (
      <CheckBox title='importar email'
        checked={checkImportPerson}
        onPress={() => { setcheckImportPerson(!checkImportPerson) }}
      />
    )

  }


  const callback = async (result: any) => {
    const hashResults = await hashGeocode(result);
    const data = { ...current_place };
    data.current_place_name = result;
    data.current_place = hashResults;
    set_current_place(data)
  }

  const getViewAttributesPersonSelected = () => {
    return (
      <View style={{ flex: 1, marginTop: 20, paddingHorizontal: 50 }}>
        {getViewPersonSelected()}
      </View>
    )
  }

  const importUser = async () => {
    // setcheckImportPerson(false);
    await addImportedPersonFirebase(persona_seleccionada.email, optionSelected, context)
    props.navigation.navigate('TabsBottom')
  }

  const getViewEmailImput = () => {
    return (
      <View style={{ flexDirection: 'row' }}>
        {state.error.errorEmail && <TextInputPlanNBite error={state.error.errorEmail} text={persona_seleccionada.email} setText={(text) => setPersona_seleccionada({ ...persona_seleccionada, email: text })} placeholder='email' />}
        <Button title='import User' buttonStyle={{ padding: 10 }}
          onPress={() => {
            importUser()
          }}
        />

      </View>
    )
  }

  return (
    <ImageBackground style={{ height: '100%' }} source={require('../../assets/Screen_AcessStack_00.png')}>
      <ScrollView>

        {last_screen === 'Cuenta' && !persona_seleccionada.email && getViewImportPerson()}
        {checkImportPerson
          ? getViewEmailImput()
          : getViewAttributesPersonSelected()
        }
      </ScrollView>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }}>
        <Button title='save' buttonStyle={{ padding: 10 }} onPress={() => {
          setState({ ...state, error: {} })
          if (persona_seleccionada.age === "0" || persona_seleccionada.weight === "0" || persona_seleccionada.age === "" || persona_seleccionada.weight === "") { setState({ ...state, error: { error: "tienes que rellenar la edad y el peso" } }) }
          else if (optionSelected != 0 && persona_seleccionada.nombre == "") {
            setState({ ...state, error: { errorNombre: "tienes que rellenar el nombre" } })
          }
          else {
            save();
            props.navigation.navigate('Cuenta')
          }
        }} />
      </View>
    </ImageBackground>
  )
}

export default FirstPage;
