import React, { useState, useEffect,useContext } from "react";
import { Text, View, ImageBackground, TextInput } from "react-native";

import { NavigationInjectedProps } from 'react-navigation'
import ProcessBar from "../../components/ProcessBar";
import {
  LineChart,
} from 'react-native-chart-kit'
import TapButton from "../../components/TapButton";
import moment from 'moment'

import {
  getPesoDiaFirebase,
  getPesoSemanaFirebase,
  getPesoMesFirebase,
  getValoresNutricionalesMenuDia,
  getValoresNutricionalesSemmana,
  getValoresNutricionalesMes,
  getValoresNutricionalesRecomendadosFirebase,
  updatePesoFirebase,
} from "../../storage/storage";
import { ValorNutricional } from "../../type/type";
import { get_Premium_Features_Firebase } from "../../storage/storage";
import { weekOfMonth } from "../../Utils/utils";
import { int_to_mes } from "../../Utils/utils_calendario";
import { Button } from "react-native-elements";
import CONSTANT from '../../local/constant'
import Estructura_Global from "../../Estructuras/Estructura_Global.json";
import Loading from "../../components/Loading";
import { useTranslation } from 'react-i18next'
import { AppContext } from '../../provider/provider';
import { ScrollView } from "react-native-gesture-handler";
interface Props extends NavigationInjectedProps { }

const getViewLineChart = (datos: any, index: number) => {
  const keyDatos = Object.keys(datos).map((dato: string) => {
    let ret: string = "";
    if (index == 0) ret = moment(dato, 'YYYY-MM-DD').date().toString();
    else if (index == 1) ret = moment(dato, 'YYYY-MM-DD').day(-6).date() + "-" + moment(dato, 'YYYY-MM-DD').date();
    return ret + " " + int_to_mes(moment(dato, 'YYYY-MM-DD').month() + 1)
  })
  return (
    <LineChart
      data={{
        labels: keyDatos,
        datasets: [{
          data: Object.values(datos)
        }]
      }}
      width={375} // from react-native
      height={300}
      yAxisLabel={'kg'}
      chartConfig={{
        backgroundColor: 'black',
        backgroundGradientFrom: 'white',
        backgroundGradientTo: 'white',
        decimalPlaces: 2, // optional, defaults to 2dp
        color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
        style: {
          borderRadius: 16
        }
      }}
      bezier
      style={{
        marginVertical: 8,
        borderRadius: 16
      }}
    />
  )
}

const getPercentajeValoresNutricionales = async (valoresNutricionales: any) => {
  const valoresRecomendados = await getValoresNutricionalesRecomendadosFirebase()
  let percentajeValoresNutricionales: any = {};
  Object.entries(valoresRecomendados).map(([key, value]: any[]) => {
    const ValorNutricional = valoresNutricionales[key] ? valoresNutricionales[key] : 0;
    percentajeValoresNutricionales[key] = Math.round(ValorNutricional / value * 100)
  })
  return percentajeValoresNutricionales;
}

export default (props: Props) => {
  const { t } = useTranslation();
  const context: any = useContext(AppContext)
  const [state, setState] = useState<any>({
    index: 0,
    index2: 0,
    datosPeso: { '2020-08-01': 1, '2020-08-02': 1, '2020-08-03': 1, },
    valoresNutricionalesSeleccionados: {},
    pesoActual: 0,
    seguimiento: false,
    variasPersonas: false,
    emails: [Estructura_Global.email],
    selected_email: undefined,
    loaded: false,
  });


  const init = async () => {
    const actualMoment = moment();

    const year: number = actualMoment.year();
    const month: number = actualMoment.month() + 1;
    const date: number = actualMoment.date();
    const datosPeso = await getPesoDiaFirebase(state.selected_email)
    let valoresNutricionalesSeleccionados = await getValoresNutricionalesMenuDia(year, month, date, state.selected_email)
    valoresNutricionalesSeleccionados = await getPercentajeValoresNutricionales(valoresNutricionalesSeleccionados);
    const listaPersonas = context.lista_personas;
    const emails = listaPersonas.map((obj: any) => obj.email);
    const nombres = listaPersonas.map((obj: any) => obj.nombre);
    const features = await get_Premium_Features_Firebase()
    const check_seguimiento = features && features.seguimiento && features.seguimiento > moment().format(CONSTANT.formatMoment)
    const variasPersonas = features && features.varias_personas && features.varias_personas > moment().format(CONSTANT.formatMoment)

    setState({
      ...state,
      datosPeso,
      valoresNutricionalesSeleccionados,
      seguimiento: check_seguimiento,
      variasPersonas: variasPersonas,
      emails: emails,
      nombres: nombres,
      selected_email: emails[0],
      loaded: true,
    });

  }

  useEffect(() => {
    init();
  }, [])


  const updateData = async (index: number, email?: boolean) => {
    let datosPeso = {}
    let valoresNutricionalesSeleccionados: any = {}
    const defaultValoresNutricionales = { kcal: 0, hidratos: 0, proteinas: 0, fibra: 0, grasa: 0, azucar: 0 }
    const actualMoment = moment();
    const year: number = actualMoment.year();
    const month: number = actualMoment.month() + 1;
    const date: number = actualMoment.date();
    const week: number = weekOfMonth(year, month, date)

    if (index == 0) {
      datosPeso = await getPesoDiaFirebase(state.email);
      valoresNutricionalesSeleccionados = await getValoresNutricionalesMenuDia(year, month, date, state.email)
      if (Object.keys(valoresNutricionalesSeleccionados).length == 0) valoresNutricionalesSeleccionados = Object.assign(defaultValoresNutricionales, {})
    }
    else if (index == 1) {
      datosPeso = await getPesoSemanaFirebase(state.email)
      valoresNutricionalesSeleccionados = await getValoresNutricionalesSemmana(year, month, week, state.email)
    }
    else if (index == 2) {
      datosPeso = await getPesoMesFirebase(state.email)
      valoresNutricionalesSeleccionados = await getValoresNutricionalesMes(year, month, state.email)
    }
    valoresNutricionalesSeleccionados = await getPercentajeValoresNutricionales(valoresNutricionalesSeleccionados);
    if (email) setState({ ...state, index2: index, datosPeso, valoresNutricionalesSeleccionados, selected_email: state.emails[index] });
    else setState({ ...state, index, datosPeso, valoresNutricionalesSeleccionados });
  }

  console.log(state)
  return (
    <ScrollView style={{ height: '100%', backgroundColor: 'white' }} >
      {state.loaded ?
        <View>
          {state.seguimiento && state.variasPersonas && <TapButton index={state.index2} setindex={(num) => { updateData(num, true) }} tabButtonNames={state.nombres} />}
          {state.seguimiento && <TapButton index={state.index} setindex={updateData} tabButtonNames={[t("seguimiento.day"), t("seguimiento.week"), t("seguimiento.month")]} />}
          <View style={{ padding: 15 }}>
            <Text>{t("seguimiento.evolution_weight")}</Text>
            {getViewLineChart(state.datosPeso, state.index)}
            {(state.selected_email == undefined || state.selected_email == Estructura_Global.email) &&
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <TextInput
                  keyboardType='phone-pad'
                  value={state.pesoActual}
                  onChangeText={(text) => setState({ ...state, pesoActual: text })}
                  placeholder={t("seguimiento.actual_weight")}
                />
                <Button title={t("seguimiento.save_actual_weight")}
                  onPress={() => {
                    updatePesoFirebase(parseInt(state.pesoActual),context)
                    updateData(state.index)
                  }}
                />
              </View>
            }
            {state.seguimiento &&
              <View>
                <Text>{t("seguimiento.nutricional_values")}</Text>
                <View style={{ flexDirection: "row", flexWrap: 'wrap' }}>
                  <ProcessBar title={t("seguimiento.kcal")} percentaje={state.valoresNutricionalesSeleccionados['kcal']} style={{ width: 130, marginHorizontal: 15, marginVertical: 10 }} />
                  <ProcessBar title={t("seguimiento.carbohydrates")} percentaje={state.valoresNutricionalesSeleccionados['hidratos']} style={{ width: 130, marginHorizontal: 15, marginVertical: 10 }} />
                  <ProcessBar title={t("seguimiento.protein")} percentaje={state.valoresNutricionalesSeleccionados['proteinas']} style={{ width: 130, marginHorizontal: 15, marginVertical: 10 }} />
                  <ProcessBar title={t("seguimiento.sugar")} percentaje={state.valoresNutricionalesSeleccionados['azucar']} style={{ width: 130, marginHorizontal: 15, marginVertical: 10 }} />
                  <ProcessBar title={t("seguimiento.fats")} percentaje={state.valoresNutricionalesSeleccionados['grasa']} style={{ width: 130, marginHorizontal: 15, marginVertical: 10 }} />
                  <ProcessBar title={t("seguimiento.fiber")} percentaje={state.valoresNutricionalesSeleccionados['fibra']} style={{ width: 130, marginHorizontal: 15, marginVertical: 10 }} />
                </View>
              </View>
            }
          </View>
        </View>
        : <Loading />
      }
    </ScrollView>
  )
}

