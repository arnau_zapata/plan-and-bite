import React, { useRef, useState, useContext } from 'react';
import { StyleSheet, Text, View, KeyboardAvoidingView, ImageBackground, AsyncStorage } from "react-native";
import { Button } from 'react-native-elements';
var s = require('../style');
import {
  clearAllData,
  getEmailUserFirebase,
  setEmailUserFirebase,
} from '../storage/storage';
import Estructura_Global from '../Estructuras/Estructura_Global.json'
import { addNewUser } from '../storage/storage';
import TextInputPlanNBite from '../components/TextInputPlanNBite'
import { loginFirebase, signupFirebase } from '../Utils/utils_auth';
import { AppContext } from '../provider/provider';
import { Context } from '../type/type';

const Registro = (props: any) => {
  //function Registro() {

  const [selectedIndex, set_selectedIndex] = useState(0);
  const buttons = ['Hombre', 'Mujer'];
  const context: Context = useContext(AppContext)
  const [loading, setLoading] = useState(false)
  const [loadingText, setloadingText] = useState("")
  let [contrasena, set_contrasena] = useState("");
  let [otherContrasena, setOtherContrasena] = useState("");
  let [email, set_email] = useState("");
  const [state_err_email, set_state_err_email] = useState("");
  const [state_err_password, set_state_err_password] = useState("");




  const regist = async () => {
    clearAllData();
    set_state_err_email("");
    set_state_err_password("");
    const registerEmail = email.toLocaleLowerCase().trim();
    if (contrasena == "" || otherContrasena == "" || registerEmail == "") {
      registerEmail == "" && set_state_err_email("error");
      (otherContrasena == "" || contrasena == "") && set_state_err_password("error")

    }
    if (contrasena != otherContrasena) {
      set_state_err_password("error")
      return;
    }
    if (await addNewUser(registerEmail)) {
      setEmailUserFirebase(registerEmail)

      context.updateFromFirebase(false, setloadingText);
      props.navigation.navigate('Perfil', { persona_seleccionada: {}, index: 0, last_screen: 'Registro' });
    } else {
      set_state_err_email("error")
      console.log("error")
    }

  }
  const checkRegister = async () => {
    console.log('registering...')
    let result: any = await signupFirebase(email.trim(), contrasena)
    if (result != undefined && result.user != undefined) {
      result = await loginFirebase(email.trim(), contrasena)
      if (result.user != undefined) {
        Estructura_Global.email = email;
        console.log(`User account   with password created & signed in!`);
        AsyncStorage.setItem('email', email);
        setLoading(true)
        regist();
        return
      }
      return
    }
    set_state_err_email("error " + result)
    console.log(result)
  };


  let textInput: any = {}

  const focusNextField = (nextField: string) => {
    textInput[nextField].focus();
  }

  return (
    <ImageBackground style={[s.containerRegistro]} source={require('../assets/Screen_AcessStack_00.png')}>
      <KeyboardAvoidingView behavior="padding" enabled>
        <View>
          <Button
            buttonStyle={{ backgroundColor: 'white', width: 75 }}
            icon={
              <Text style={[s.fuente_S, { fontWeight: 'bold' }]}>
                Back
            </Text>
            }
            onPress={() => {
              props.navigation.navigate({ routeName: 'Login' });
            }}
          />
        </View>

        <View style={s.form}>
          <TextInputPlanNBite
            text={email}
            error={state_err_email}
            setText={set_email}
            placeholder={'Email'}
            reference={input => {
              textInput["email"] = input;
            }}
            focusNextField={() => focusNextField("password")}
          />
          <TextInputPlanNBite
            text={contrasena}
            error={state_err_password}
            setText={set_contrasena}
            placeholder={'Password'}
            reference={input => {
              textInput["password"] = input;
            }}
            focusNextField={() => focusNextField("repeat_password")}
            password={true}
          />
          <TextInputPlanNBite
            text={otherContrasena}
            error={state_err_password}
            setText={setOtherContrasena}
            placeholder={'Repeat Password'}
            reference={input => {
              textInput["repeat_password"] = input;
            }}
            focusNextField={checkRegister}
            password={true}
          />

          <Button
            buttonStyle={{ backgroundColor: 'white', borderRadius: 10, marginTop: 50, width: 300 }}
            loading={loading}
            loadingProps={{ color: 'black'}}
            icon={
              <Text style={[s.fuente_M, { fontWeight: 'bold' }]}>
                Sign Up
            </Text>
            }
            onPress={() => checkRegister()}
          />
          <Text style={{ color: 'white' }}>{loadingText}</Text>
        </View>
      </KeyboardAvoidingView>
    </ImageBackground>
  );
}



export default Registro;