import React, { useRef, useState, useEffect, Component, useContext } from 'react';
import { StyleSheet, Image, Text, View, TouchableOpacity, } from 'react-native';
import {
    get_Bitecoins_Firebase, add_Bitecoins_Firebase,
} from '../storage/storage';

var s = require('../style');

import { Button, ButtonGroup } from 'react-native-elements';
import BiteCoin from '../assets/bitecoin.png'
import ModalPayment from '../components/ModalPayment';

export default ({ navigation }: any) => {
    let [bitecoins, set_bitecoins] = useState(0);
    let [reset, set_reset] = useState(true);
    const [modalPremium, setModalPremium] = useState<any>({ visible: false, data: undefined })

    const inicializate = async () => {
        set_bitecoins(await get_Bitecoins_Firebase())
        set_reset(false);
    }

    useEffect(() => {
        if (reset) {
            inicializate();
        }
    }, [reset]);

    const getViewButton = (num_eur: number) => {
        let suplement = 1;
        if (num_eur == 5) suplement = 1.1;
        else if (num_eur == 10) suplement = 1.2;
        else if (num_eur == 20) suplement = 1.3;
        else if (num_eur == 50) suplement = 1.4;
        else if (num_eur == 100) suplement = 1.5;
        return (
            <TouchableOpacity
                style={{ borderWidth: 3, marginTop: 10, borderRadius: 10, borderColor: 'green', justifyContent: 'center', padding: 10, flexDirection: 'row', alignItems: 'center' }}
                onPress={() => {
                    setModalPremium({visible:true,data:{price:num_eur,bitecoins:num_eur * 1000 * suplement}})
                }}
            >
                <Text>comprar {num_eur * 1000 * suplement} </Text>
                <Image source={BiteCoin} style={{ height: 25, width: 25 }} />
                <Text>   ({num_eur}€) </Text>

            </TouchableOpacity>
        )
    }

    return (
        <View >
            <View style={{ padding: 20, flexDirection: 'row', alignItems: 'center' }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}> {bitecoins}</Text>
                <Image source={BiteCoin} style={{ height: 25, width: 25 }} />
                <Text style={{ marginLeft: 20 }}>bitecoin/s</Text>
            </View>
            {getViewButton(1)}
            {getViewButton(5)}
            {getViewButton(10)}
            {getViewButton(20)}
            {getViewButton(50)}
            {getViewButton(100)}
            <ModalPayment
                visible={modalPremium.visible}
                closeModal={() => { setModalPremium({ visible: false, data: undefined }) }}
                amount={modalPremium.data ? modalPremium.data.price : undefined}
                onPressAccept={async () => {
                    add_Bitecoins_Firebase(modalPremium.data.bitecoins);
                    navigation.navigate('Premium');
                }}
                navigation={navigation}
            />
        </View>
    )
}




