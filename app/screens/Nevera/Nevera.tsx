import React, { useRef, useState, useEffect, Component, useContext } from 'react';
import { Modal, StyleSheet, Image, Text, View, TextInput, ImageBackground, TouchableOpacity, Alert } from 'react-native';

import ActionButton from 'react-native-action-button';
import { ScrollView } from 'react-native-gesture-handler';
import { Header } from 'react-native-elements';
import { useTranslation } from 'react-i18next'
import { AppContext } from '../../provider/provider';


//Imported Styles
var s = require('../../style');

import NeveraEdit from './components/NeveraEdit';
import TapButton from '../../components/TapButton';
import moment from 'moment';
import ModalSelectDate from './components/ModalSelectDate';
import { Context } from '../../type/type';
import { translate } from '../../Utils/utils';



const Nevera = (props: any) => {
  const context: any = useContext(AppContext)
  const [index, setIndex] = useState(0)
  const [modalEdit, setModalEdit] = useState<any>({ visible: false });
  const [modalSelectDateVisible, setModalSelectDateVisible] = useState({ visible: false, data: undefined });
  const { t } = useTranslation();

  // const lista_ingredientes=state.Nevera.lista_ingredientes_en_nevera;


  const getViewNevera = () => {
    let views = [];
    for (let i = 0; i < context.lista_ingredientes_nevera.length; i++) {
      const ingrediente: any = context.lista_ingredientes_nevera[i];
      views.push(
        <TouchableOpacity
          style={[s.row, s.magicIcon2LaVenganza, { marginBottom: 20, alignItems: "space-between", justifyContent: "space-between" }]}
          onPress={() => {
            setModalEdit({
              visible: true,
              ingrediente_seleccionado: ingrediente,
            });
          }}
        >

          {/* <Image style={s.img_XS} source={logImg} /> */}
          <View>
            <Text style={[s.text_color_gray, s.fuente_M]}> {translate(ingrediente.nombre)}</Text>
          </View>
          <View style={{ width: 100 }}>
            <Text style={[s.text_color_gray, s.fuente_M]}> {ingrediente.cantidad} {ingrediente.tipo_cantidad}</Text>
          </View>
        </TouchableOpacity>
      );
    }
    return views;
  }

  const sortByExpirationDate = (x: any, y: any) => {
    if (x.expiration_date == undefined && y.expiration_date == undefined) return 0;
    if (x.expiration_date == undefined) return 1;
    if (y.expiration_date == undefined) return -1;
    if (x.expiration_date > y.expiration_date) return -1;
    else return 1
  }

  const getViewExpirationDate = () => {
    let views = [];
    for (let i = 0; i < context.lista_ingredientes_nevera.sort(sortByExpirationDate).length; i++) {
      const ingrediente: any = context.lista_ingredientes_nevera[i];
      views.push(
        <TouchableOpacity
          style={[s.row, s.magicIcon2LaVenganza, { marginBottom: 20, alignItems: "space-between", justifyContent: "space-between" }]}
          onPress={() => {
            setModalSelectDateVisible({ visible: true, data: ingrediente })
          }}
        >
          <View>
            <Text style={[s.text_color_gray, s.fuente_M]}> {translate(ingrediente.nombre)}</Text>
          </View>
          <View style={{ width: 150 }}>
            <Text style={[s.text_color_gray, s.fuente_M]}> {ingrediente.expiration_date ? moment(ingrediente.expiration_date, 'YYYY-MM-DD').format('DD-MM-YYYY') : '?'}</Text>
          </View>
        </TouchableOpacity>
      );
    }
    return views;
  }

  const generate_view_Dialog = () => {
    return (

      <NeveraEdit
        visible={modalEdit.visible}
        setModal={setModalEdit}
        ingrediente_seleccionado={modalEdit.ingrediente_seleccionado ? modalEdit.ingrediente_seleccionado : {}}
      />

    )

  }

  const getView_ModalExpirationDate = () => {
    return (
      <ModalSelectDate
        visible={modalSelectDateVisible.visible}
        setVisible={setModalSelectDateVisible}
        data={modalSelectDateVisible.data}
        navigation={props.navigation}
      />

    )
  }

  return (
    <View>
      <Header
        placement="center"
        centerComponent={{ text: 'Tu nevera', style: { color: '#fff' } }}
      />
      <ImageBackground style={[{ paddingTop: 20, alignItems: "center", justifyContent: 'space-between', height: '90%' }]} source={require('../../assets/Screen_Fridge_00.png')}>
        <View style={{ marginTop: 20 }}>
          <View style={[s.row, s.magicIcon2LaVenganza, { paddingBottom: 20, alignItems: "center", justifyContent: "center" }]}>
            {/*<Image style={[s.img_M]} source={ToolbarNevera}/> */}
            <Text style={[s.fuente_L, s.text_color_gray]}>{t('nevera.principal.ingredients')}</Text>
          </View>
          <TapButton index={index} setindex={setIndex} tabButtonNames={[t('nevera.principal.show_quantity'), t('nevera.principal.show_expiration_date')]} />
          <ScrollView style={{ marginTop: 40 }}>
            {index == 0 && getViewNevera()}
            {index == 1 && getViewExpirationDate()}
          </ScrollView>
        </View>

        {generate_view_Dialog()}
        {getView_ModalExpirationDate()}

        <ActionButton
          buttonColor="#6495ed"
          onPress={() => {
            props.navigation.navigate('NeveraAdd');
            // props.navigation.navigate({ routeName: 'NeveraAdd'});
          }}
        />
      </ImageBackground>
    </View>
  );
}

export default Nevera;


