import React, { useRef, useState, useContext, Component } from 'react';
import { StyleSheet, Image, Text, View, TextInput, ImageBackground, TouchableOpacity } from 'react-native';
import { Button } from 'react-native-elements';
import ModalPlanNBite from '../../../components/ModalPlanNBite';
import DatePicker from 'react-native-datepicker'
import moment from 'moment'
import TapButton from '../../../components/TapButton';


type Props = {
    visible: boolean,
    setVisible: (x: any) => void
    data: any,
    navigation: any
}

export default (props: Props) => {
    console.log(props.data)
    const [state,setState] = useState({date:moment().format('YYYY-MM-DD')})
    const [comida,setComida] = useState(0)
    const [plato,setPlato] = useState(0)
    return (
        <ModalPlanNBite
            visible={props.visible}
            onPressCancel={() => props.setVisible({ visible: false })}
        >
            <DatePicker
                style={{ width: 200 }}
                date={state.date}
                mode="date"
                placeholder="select date"
                format="YYYY-MM-DD"
                minDate={moment().format('YYYY-MM-DD')}
                maxDate={moment().add(14,'days').format('YYYY-MM-DD')}
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                    dateIcon: {
                        position: 'absolute',
                        left: 0,
                        top: 4,
                        marginLeft: 0
                    },
                    dateInput: {
                        marginLeft: 36
                    }
                    // ... You can check the source to find the other keys.
                }}
                onDateChange={(date:any) => { setState({ date: date }) }}
            />
            <Text>seleccione Comida</Text>
            <TapButton index={comida} setindex={setComida} tabButtonNames={['Desayuno','Comida','Cena']}/>
            <Text>seleccione Plato</Text>
            <TapButton index={plato} setindex={setPlato} tabButtonNames={['1er','2do','3er']}/>

            <View style={{ flexDirection: "row" }}>
                <Button title='cancel' onPress={() => props.setVisible({ visible: false })} />
                <Button title='accept' onPress={() => {
                    props.navigation.navigate('AnadirReceta', { 
                        lista_ingredientes_introducidos: [props.data],
                        year:moment(state.date,"YYYY-MM-DD").year(),
                        month:moment(state.date,"YYYY-MM-DD").month(),
                        day:moment(state.date,"YYYY-MM-DD").day(),
                        comida,
                        plato, 
                    })
                }} />
            </View>
        </ModalPlanNBite>
    );
}


