import React, { useRef, useState, Component, useContext } from 'react';
import { Text, View, TextInput, TouchableOpacity } from 'react-native';
import { Button, ButtonGroup } from 'react-native-elements';
import Estructura_Global from '../../../Estructuras/Estructura_Global.json'
var s = require('../../../style');
import { isInteger, translate } from '../../../Utils/utils'
import {
  update_Ingrediente_Nevera,
} from '../../../storage/storage'
import ModalPlanNBite from '../../../components/ModalPlanNBite';
import { useTranslation } from 'react-i18next'
import { AppContext } from '../../../provider/provider';
const NeveraEdit = (props: any) => {
  const context: any = useContext(AppContext)
  const { t } = useTranslation();


  let [cantidad, set_cantidad] = useState("");
  let [err_num, set_err_num] = useState("");



  return (

    <ModalPlanNBite
      visible={props.visible}
      onPressCancel={() => props.setModal({ visible: false })}
    >
      <View style={[s.appColor, { alignItems: "center" }]}>
        <Text style={s.fuente_L}>{t("nevera.editar_nevera.edit_ingredients")}</Text>
        <View style={{ display: "flex", flexDirection: "row", backgroundColor: 'white' }}>
          <Text style={[s.basicPadding, s.row, { borderWidth: 1, width: '50%' }]}>{translate(props.ingrediente_seleccionado.nombre)} </Text>
          <TextInput style={[s.basicPadding, s.row, { borderWidth: 1, width: '50%' }]} placeholder='nueva cantidad' onChangeText={(text) => set_cantidad(text)} />
        </View>
        <Text >{t("nevera.editar_nevera.actual_quantity")}{props.ingrediente_seleccionado.cantidad} {props.ingrediente_seleccionado.tipo_cantidad} </Text>
        <Text>{err_num}</Text>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Button
            title="Cancelar"
            buttonStyle={{ marginRight: 50 }}
            onPress={() => props.setModal({ visible: false })}
          />
          <Button
            title="Modificar"
            onPress={() => {
              if (isInteger(cantidad)) {
                set_err_num("");
                let new_cantidad = parseInt(cantidad);
                let old_cantidad = props.ingrediente_seleccionado.cantidad ? props.ingrediente_seleccionado.cantidad : 0;
                console.log(old_cantidad)
                console.log(new_cantidad)
                const diffCantidad = new_cantidad - old_cantidad;
                if (new_cantidad > 0) {
                  let ingrediente: any = { ...props.ingrediente_seleccionado, cantidad: new_cantidad }
                  update_Ingrediente_Nevera(ingrediente, diffCantidad, context)
                } else {
                  let ingrediente: any = { ...props.ingrediente_seleccionado, cantidad: 0 }

                  update_Ingrediente_Nevera(ingrediente, -old_cantidad, context)
                }
                props.setModal({ visible: false })
              }
              else {
                set_err_num("No es integer");
              }
            }} />
        </View>
      </View>
    </ModalPlanNBite>

  );
}

export default NeveraEdit;

