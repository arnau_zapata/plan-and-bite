import React, { useRef, useState, useContext, Component } from 'react';
import {  Image, Text, View, ImageBackground, TouchableOpacity } from 'react-native';
var s = require('../../../style');

import { isInteger, translate } from '../../../Utils/utils'
import Dialog from "react-native-dialog";
import { Header } from 'react-native-elements';
import flecha_izquierda from '../../../assets/icon_flecha-izquierda.png'
import { get_Ingrediente, update_Ingrediente_Nevera } from '../../../storage/storage'
import { useTranslation } from 'react-i18next'
import { AppContext } from '../../../provider/provider';
import { Context, Ingrediente } from '../../../type/type';
const NeveraAdd = (props: any) => {
  const context: any = useContext(AppContext)
  const { t } = useTranslation();
  let [ingredieteSeleccionado, setIngredienteSeleccionado] = useState<any>({});


  let [cantidad, set_cantidad] = useState("");
  let [err_num, set_err_num] = useState("");

  const [visible, set_visible] = useState(false);




  const getViewNevera = () => {
    let views = [];
    const lista_ingredientes = context.lista_ingredientes;
    for (let i = 0; i < lista_ingredientes.length; i++) {
      const ingrediente: any = lista_ingredientes[i];
      views.push(
        <TouchableOpacity
          style={[s.row, s.magicIcon2LaVenganza, { marginBottom: 20, alignItems: "center", justifyContent: "space-between" }]}
          onPress={() => {
            setIngredienteSeleccionado(lista_ingredientes[i]);
            set_visible(true);
          }}
        >
          {/* <Image style={[s.img_XS,{marginRight:30}]} source={logImg}/> */}
          <Text style={[s.text_color_gray, s.fuente_M]}>{translate(ingrediente.nombre)} </Text>
        </TouchableOpacity>
      );
    }
    return views;
  }

  const generarTexto = () => {
    let lista = getViewNevera();
    return (<View>{lista}</View>);

  }

  const generate_view_Dialog = () => {
    return (
      <Dialog.Container visible={visible}>
        <Dialog.Title>Modificar Cantidad</Dialog.Title>

        <Dialog.Description>{translate(ingredieteSeleccionado.nombre)} </Dialog.Description>
        <Dialog.Input style={[s.basicPadding, s.row, { borderWidth: 1, width: '50%' }]} placeholder='nueva cantidad' onChangeText={(text: string) => set_cantidad(text)} />
        {/* <Dialog.Description >cantidad actual: {Estructura_Global.ingrediente_seleccionado.cantidad} {Estructura_Global.ingrediente_seleccionado.tipo_cantidad} </Dialog.Description>
            <Dialog.Description>{err_num}</Dialog.Description> */}
        <Dialog.Button label="Cancel" onPress={() => set_visible(false)} />
        <Dialog.Button label="Añadir" onPress={async () => {
          console.log("parseInt(cantidad)", parseInt(cantidad))
          if (isInteger(cantidad)) {
            set_err_num("");
            if (parseInt(cantidad) == 0) {
              return;
            }
            const auxIngrediente: any = await get_Ingrediente(ingredieteSeleccionado.id_ingrediente)
            let ingrediente: Ingrediente = {
              ...auxIngrediente,
              id_ingrediente: ingredieteSeleccionado.id_ingrediente,
              cantidad: parseInt(cantidad),

            }
            update_Ingrediente_Nevera(ingrediente, parseInt(cantidad), context)
            setIngredienteSeleccionado({ ...ingredieteSeleccionado, cantidad })
            set_visible(false)

            props.navigation.navigate('AccessStack');
            props.navigation.navigate('Nevera');
          }
          else {
            set_err_num("No es integer");
          }
        }} />
      </Dialog.Container>
    );
  }

  const CustomButton = (props: any) =>
    <TouchableOpacity onPress={() => {

      props.navigation.navigate('Nevera')
    }} >
      <Image source={flecha_izquierda} style={[s.img_S, { backgroundColor: 'rgba(250,250,250,0.5)', borderWidth: 1 }]} />
    </TouchableOpacity>

  return (
    <ImageBackground style={[{ alignItems: "center", height: '100%' }]} source={require('../../../assets/Screen_Fridge_00.png')}>
      <Header
        placement="center"

        leftComponent={CustomButton(props)}
        //leftComponent= {{ icon: 'menu' ,  color: '#fff' }}
        centerComponent={{ text: 'Añadir Ingrediente', style: { color: '#fff' } }}
      // rightComponent={CustomButton(this.props)}
      //rightComponent={{ icon: 'home', color: '#fff' }}
      />

      <Text style={[s.fuente_L, s.text_color_gray]}>{t("nevera.anadir_nevera.add_ingredients")}</Text>
      {generarTexto()}
      {generate_view_Dialog()}
    </ImageBackground>
  );
}

export default NeveraAdd;

