import React, { useState, useContext, useEffect } from 'react';
import { Image, Text, View, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { Input, Icon, Button } from 'react-native-elements';
import { useTranslation } from 'react-i18next'
import BiteCoin from '../../../assets/bitecoin.png'
import SearchableDropdown from 'react-native-searchable-dropdown';
import StyledSearchableDropdown from './StyledSearchableDropdown';
import { getPersonalizedUrlDataFirebase, getUploadDataFirebase } from '../../../storage/storage';
import ModalPlanNBite from '../../../components/ModalPlanNBite';


const styles = StyleSheet.create({
    mapKey: {
        marginHorizontal: 15,
        borderWidth: 1,
        width: '100%',
        flexDirection: 'row',
        alignItems: "center"
    },
    mapValue: {
        width: '50%',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    }
})
type LeyendAttr = {
    id: string,
    name: string,
    bitecoins: number,
    value?: any,
    type: string,
    url?: string,
}

type Item = {
    id: number,
    value: string,
    name: string
}

interface Props {
    leyendAttr: LeyendAttr,
    onChangeText: (z: string | number) => void
    typeMap?: string,
    typeData?: string
    nameData?: string
};

export default (props: Props) => {

    const initValue = props.leyendAttr.value ? props.leyendAttr.value.toString() : "";
    const [value, setValue] = useState(initValue);
    const [searchableData, setSearchableData] = useState<any[]>([]);
    const [keySerchableDropdown, setKeySerchableDropdown] = useState({ id: props.leyendAttr.id, name: props.leyendAttr.name })
    const [visible, setVisible] = useState(false)
    console.log("keySerchableDropdown", keySerchableDropdown)
    useEffect(() => {
        const init = async () => {
            if ((props.typeMap == 'searchable-map' || props.typeMap == 'searchable-array') && props.leyendAttr.url) {
                let verifiedData: any = await getUploadDataFirebase(props.typeData, props.nameData)
                let data: any = await getPersonalizedUrlDataFirebase(props.leyendAttr.url, true, true);
                verifiedData = verifiedData[props.leyendAttr.id]
                console.log(props.typeMap, data)
                if (Array.isArray(data)) {
                    data = data.map((obj: any) => { return { id: obj.id, name: obj.name, value: obj.name } })
                    if (verifiedData != undefined) data = data.filter((obj: any) => verifiedData.find((obj2: any) => obj2 == obj.name) != undefined)
                }
                else if (!Array.isArray(data)) {
                    data = Object.entries(data).map(([key, value]: any) => { return { id: key, name: value, value: value } })
                    if (verifiedData != undefined) data = data.filter((obj: any) => verifiedData[obj.name] != undefined)
                }

                setSearchableData(data ? data : []);
            }
        }
        init();
    }, [])

    const renderInput = () => {
        return (
            <Input
                value={value}
                onChangeText={(text: string) => {
                    if (props.typeMap == 'searchable-map') {
                        const aux: any = {}
                        aux[keySerchableDropdown.id] = { id: keySerchableDropdown.id, name: keySerchableDropdown.name, value: props.leyendAttr.type != 'number' ? text : parseInt(text) }
                        props.onChangeText(aux)
                    }
                    else props.onChangeText(props.leyendAttr.type != 'number' ? text : parseInt(text))
                    setValue(text);
                }}
                numberOfLines={props.leyendAttr.type == 'long-string' ? 10 : 1}
                label={props.typeMap ? undefined : props.leyendAttr.name}
                keyboardType={props.leyendAttr.type == 'number' ? 'phone-pad' : undefined}
                inputContainerStyle={{ borderWidth: props.leyendAttr.type == 'long-string' ? 1 : 0, borderBottomWidth: props.typeMap ? 0 : 1, }}
            />
        )
    }

    const renderKeyMap = () => {
        return (
            <View style={{ width: props.typeMap != 'searchable-array' ? '50%' : '100%', height: 50, borderRightWidth: 1, alignItems: 'center', justifyContent: 'center' }}>
                {props.typeMap == 'map' && <Text style={{ textAlign: 'center' }}>{props.leyendAttr.name}</Text>}
                {(props.typeMap == 'searchable-map' || props.typeMap == 'searchable-array') &&
                    <TouchableOpacity style={{ width: '100%', height: '100%' }} onPress={() => {
                        setVisible(true)
                    }} >
                        <Text>{keySerchableDropdown.name}</Text>
                    </TouchableOpacity>
                }
            </View>
        )
    }

    return (
        <View style={{ width: '70%', flexDirection: 'row', alignItems: 'center' }}>
            {((props.typeMap != 'searchable-array' && props.typeMap != 'searchable-map') || searchableData.length > 0) &&
                <View style={props.typeMap ? { flexDirection: 'row', width: '100%' } : { width: '100%', flexDirection: 'row' }}>

                    <View style={props.typeMap ? styles.mapKey : { width: '100%' }}>
                        {props.typeMap && renderKeyMap()}
                        {props.typeMap != 'searchable-array' &&
                            <View style={props.typeMap ? styles.mapValue : {}}>
                                {renderInput()}
                            </View>
                        }
                    </View>
                    {props.leyendAttr.bitecoins > 0 &&
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text>{props.leyendAttr.bitecoins}</Text>
                            <Image source={BiteCoin} style={{ height: 25, width: 25 }} />
                        </View>
                    }
                    <ModalPlanNBite visible={visible} onPressCancel={() => setVisible(false)}>
                        <StyledSearchableDropdown data={searchableData} onItemSelect={(item) => {
                            console.log("item", item)
                            if (props.typeMap == 'searchable-array') {
                                props.onChangeText(item)
                            }
                            else setKeySerchableDropdown(item);
                            setVisible(false)
                        }} />
                    </ModalPlanNBite>
                </View>
            }
        </View>
    );
}



