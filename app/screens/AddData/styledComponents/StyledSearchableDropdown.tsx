import React, { useState, useContext, useEffect } from 'react';
import SearchableDropdown from '../../../components/SearchableDropdown';


type LeyendAttr = {
    id: string,
    name: string,
    bitecoins: number,
    type: string
}

type Item = {
    id: number,
    value: string,
    name: string
}

interface Props {
    data: any[],
    onItemSelect: (x: any) => void
};

export default (props: Props) => {
    // const initValue = props.value ? props.value : "";
    // const [value, setValue] = useState(initValue);

    return (
        <SearchableDropdown
            onItemSelect={(item: Item) => { console.log("item", item); props.onItemSelect(item) }}
            containerStyle={{ padding: 5 }}
            itemStyle={{
                padding: 10,
                marginTop: 2,
                backgroundColor: '#ddd',
                borderColor: '#bbb',
                borderWidth: 1,
                borderRadius: 5,
            }}
            itemTextStyle={{ color: 'black' }}
            itemsContainerStyle={{ height: 200, }}
            items={props.data}
        />

    );
}



