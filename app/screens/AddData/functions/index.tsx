import { getCostIngredient, getCurrentPlace, getLocalRecipe, getLocalRecipes, getRecipe } from "../../../storage/firebase";
import { addLocalRecipeFirebase, getDataFirebase, getEmailUserFirebase, getListaRecetasFirestore, getUploadDataFirebase, setObjectAsyncStorage, setUnverifiedDataFirebase } from "../../../storage/storage";
import { Context } from "../../../type/type";
import { unmergeDict } from "../../../Utils/utils";

export const onPressLocalAddData = async (context: Context, state: any, typeData: string, totalBitecoins: number) => {

    let senderData: any = {};
    Object.entries(state.data).forEach(([key, value]: any) => {

        const isMap = typeof value == typeof {};
        if (!isMap) {
            senderData[key] = value
        } else if (Array.isArray(value)) {
            senderData[key] = value.map((obj: any) => {
                const leyendObj = state.leyend.find((leyendAttr: any) => leyendAttr.id == key);
                const bitecoins = (leyendObj && leyendObj.bitecoins) ? leyendObj.bitecoins : 0
                return obj.value;
            })
        }
        else {
            senderData[key] = {};
            Object.entries(value).forEach(([keyMap, valueMap]: any) => {
                senderData[key][keyMap] = valueMap.value;
            })
        }
    })
    const id = state.data.nombre ? state.data.nombre.toLocaleLowerCase() : "";
    await addLocalRecipeFirebase(senderData, id, totalBitecoins);
    if (typeData === 'recipes') {
        let data = await getListaRecetasFirestore();
        let localData: any = await getLocalRecipe(id);
        data = data.filter((obj: any) => obj.id_receta != id);
        let currentPlace: any = await getCurrentPlace();
        currentPlace = (currentPlace && currentPlace.current_place) ? currentPlace.current_place : currentPlace;
        let totalCost = 0;
        await Promise.all(localData.lista_ingredientes.map(async (ingrediente: any) => {
            const id = ingrediente.id_ingrediente.toString()
            const cost = await getCostIngredient(id, currentPlace)
            totalCost += cost * ingrediente.cantidad
        }))
        totalCost = Math.floor(totalCost * 100) / 100
        localData = { ...localData, local: true, cost: totalCost }
        const result = [...data, localData]
        context.set_lista_recetas(context, result, true);
        await setObjectAsyncStorage("lista_recetas", result)

    }

}

export const onPressAddData = async (navigation: any, state: any, typeData: string, totalBitecoins: number) => {
    const email = await getEmailUserFirebase();
    let senderData: any = {};
    Object.entries(state.data).forEach(([key, value]: any) => {
        const isMap = typeof value == typeof {};
        if (!isMap) {

            const leyendObj = state.leyend.find((leyendAttr: any) => leyendAttr.id == key);
            const bitecoins = (leyendObj && leyendObj.bitecoins) ? leyendObj.bitecoins : 0
            senderData[key] = {
                uploader: email,
                bitecoins,
                data: value
            }
        } else if (Array.isArray(value)) {
            senderData[key] = value.map((obj) => {
                const leyendObj = state.leyend.find((leyendAttr: any) => leyendAttr.id == key);
                const bitecoins = (leyendObj && leyendObj.bitecoins) ? leyendObj.bitecoins : 0
                return {
                    uploader: email,
                    bitecoins: bitecoins,
                    data: obj.id
                }
            })
        }
        else {
            senderData[key] = {};
            Object.entries(value).forEach(([keyMap, valueMap]: any) => {
                const leyendObj = state.leyend.find((leyendAttr: any) => leyendAttr.id == key);
                let leyendObj2;
                let bitecoins = 0;
                if (leyendObj.data) {
                    leyendObj2 = leyendObj.data.find((leyendAttr: any) => leyendAttr.id == keyMap);
                    bitecoins = (leyendObj2 && leyendObj2.bitecoins) ? leyendObj2.bitecoins : 0
                }
                else {
                    bitecoins = (leyendObj && leyendObj.bitecoins) ? leyendObj.bitecoins : 0
                }

                senderData[key][keyMap] = {
                    uploader: email,
                    bitecoins: bitecoins,
                    data: valueMap.value
                }

            })
        }
    })
    console.log(typeData, senderData[state.id].data, senderData, totalBitecoins)
    setUnverifiedDataFirebase(typeData, senderData[state.id].data, senderData, totalBitecoins)
    navigation.navigate("FreeBitecoins")
}

export const onPressIntroduceNameData = async (state: any, setState: any, typeData: string) => {
    const id = state.leyend[0].id
    console.log("id", id)
    let name: string = state.data[id];
    name = name.toLowerCase();
    console.log("name", name)
    name = name.replace(' ', '');
    const verifiedData: any = await getUploadDataFirebase(typeData, name.toLowerCase());
    let newLeyend: any = [...state.leyend];
    Object.entries(verifiedData).forEach(([key, value]: any) => {
        const leyendObj = state.leyend.find((obj: any) => obj.id == key)
        if (leyendObj) {
            if (leyendObj.type == 'map') {
                Object.entries(value).forEach(([keyMap, valueMap]) => {
                    leyendObj["data"] = leyendObj["data"].filter((obj: any) => obj.id != keyMap)
                })
            }
            else if (leyendObj.type != 'searchable-map' && leyendObj.type != 'searchable-array') {
                newLeyend = newLeyend.filter((obj: any) => obj.id != key)
            }
            // console.log("newLeyend",newLeyend)
        }
    })
    newLeyend = newLeyend.filter((leyendAttr: any) => leyendAttr.id != state.leyend[0].id);
    if (newLeyend.length > 0) setState({ ...state, id, leyend: newLeyend, isNameIntroduced: true, error: "", name })
    else setState({ ...state, error: "el ingrediente escogido ya ha sido introducido" })
}

export const onChangeStyledInput = (state: any, setState: any, typeMap: string | undefined, text: any, leyendAttr: any, idMap?: any, position?: number) => {
    let newData: any = { ...state.data };
    if (typeMap == 'searchable-array' && position) {
        newData[idMap] = newData[idMap] ? newData[idMap] : []
        newData[idMap][position.toString()] = text;
        newData[idMap] = [...newData[idMap]]
    } else if (typeMap == 'searchable-map') {
        newData[idMap] = newData[idMap] ? newData[idMap] : {}
        newData[idMap] = { ...newData[idMap], ...text }
    } else if (typeMap == 'map') {
        newData[idMap] = newData[idMap] ? newData[idMap] : {}
        newData[idMap][leyendAttr.id] = text;
    } else {
        newData[leyendAttr.id] = text;
    }
    console.log("state", { ...state, data: newData, })
    setState({ ...state, data: newData, leyend: [...state.leyend] })
}

export const getNumBitecoins = (state: any) => {
    let bitecoins = 0;
    Object.entries(state.data).forEach(([key, value]: any) => {
        const obj: any = state.leyend.find((leyendAttr: any) => leyendAttr.id == key);
        const leyendBitecoins = (obj && obj.bitecoins) ? obj.bitecoins : 0
        console.log("value", value)
        if (obj && obj.data) {
            console.log(state.leyend, value, key)
            Object.keys(value).forEach((keyMap) => {
                const obj2 = obj.data.find((dataAttr: any) => dataAttr.id == keyMap);
                const leyendBitecoins = (obj2 && obj2.bitecoins) ? obj2.bitecoins : 0
                bitecoins += leyendBitecoins
            })
        }
        else if (typeof value == typeof {}) {
            Object.keys(value).forEach((keyMap) => {
                const leyendBitecoins = (obj && obj.bitecoins) ? obj.bitecoins : 0
                bitecoins += leyendBitecoins
            })
        }
        else bitecoins += leyendBitecoins
    })
    return bitecoins;
}