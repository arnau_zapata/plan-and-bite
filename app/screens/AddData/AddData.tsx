import React, { useState, useContext, useEffect } from 'react';
import { Image, Text, View, TextInput, TouchableOpacity, ImageBackground, ScrollView, Dimensions } from 'react-native';
var s = require('../../style');
import * as firebase from "@react-native-firebase/storage";
import SafeAreaView from 'react-native-safe-area-view';
import { NavigationInjectedProps } from 'react-navigation'
import moment from 'moment'
import CONSTANT from '../../local/constant'
import { useTranslation } from 'react-i18next'
import Loading from '../../components/Loading';
import { AppContext } from '../../provider/provider';
import { getDataFirebase, getEmailUserFirebase, getLeyendUploadDataFirebase, } from '../../storage/storage';
import StyledTextInput from './styledComponents/StyledTextInput';
import { Button } from 'react-native-elements';
import { getNumBitecoins, onChangeStyledInput, onPressAddData, onPressIntroduceNameData, onPressLocalAddData } from './functions';
interface Props extends NavigationInjectedProps { };

export default (props: Props) => {
  const context: any = useContext(AppContext)
  const [state, setState] = useState({
    leyend: [],
    data: [],
    isNameIntroduced: false,
    error: "",
    id: "",
    name: ""
  })
  console.log("state.data", state.data)
  const { t } = useTranslation();
  const typeData = props.navigation.getParam('type')
  const local = props.navigation.getParam('local')
  const nextScreen = props.navigation.getParam('nextScreen')
  console.log("state data", state.data)
  type LeyendAttr = {
    id: string,
    name: string,
    bitecoins: number,
    type: 'string' | 'number' | 'map' | any
    data: LeyendAttr[]
    value?: any,
  }

  const initialization = async () => {

    const leyend = await getLeyendUploadDataFirebase(typeData);
    setState({ ...state, leyend })
    console.log(leyend)
  }


  useEffect(() => {
    initialization()
  }, [])



  const renderStyledInput = (leyendAttr: LeyendAttr, typeMap?: string, idMap?: string, position?: number) => {
    return (
      <StyledTextInput
        key={idMap + "-" + leyendAttr.id + Math.random()}
        typeMap={typeMap}
        leyendAttr={leyendAttr}
        onChangeText={(text) => onChangeStyledInput(state, setState, typeMap, text, leyendAttr, idMap, position)}
        typeData={typeData}
        nameData={state.name}
      />
    )
  }
  console.log("state.data", state.data)
  const totalBitecoins = getNumBitecoins(state);

  const renderAttr = (leyendAttr: any) => {
    const aux: any = state.data[leyendAttr.id]
    const attrValue = aux ? aux : "";
    return (
      <>
        {(leyendAttr.type == 'string' || leyendAttr.type == 'long-string' || leyendAttr.type == 'number') && renderStyledInput({ ...leyendAttr, value: attrValue })}
        {leyendAttr.type == 'map' &&
          <View>
            <Text>{leyendAttr.name}</Text>
            {leyendAttr.data.map((obj: any) => {
              const aux = attrValue[obj.id] ? attrValue[obj.id] : "";
              return renderStyledInput({ ...obj, value: aux }, 'map', leyendAttr.id)
            })}
          </View>
        }
        {leyendAttr.type == 'searchable-map' &&
          <View>
            <Text>{leyendAttr.name}</Text>
            {(state.data[leyendAttr.id] ? Object.entries(state.data[leyendAttr.id]) : []).map(([key, value]: any) => {
              const obj: LeyendAttr = { ...leyendAttr, id: key, name: value.name, value: value.value, type: 'number' }

              return renderStyledInput(obj, 'searchable-map', leyendAttr.id)
            })}
            {renderStyledInput({ ...leyendAttr, name: '', type: 'number' }, 'searchable-map', leyendAttr.id)}
          </View>
        }

        {leyendAttr.type == 'searchable-array' &&
          <View>
            <Text>{leyendAttr.name}</Text>
            {(state.data[leyendAttr.id] ? Object.entries(state.data[leyendAttr.id]) : []).map(([key, value]: any, position) => {
              const obj: LeyendAttr = { ...leyendAttr, id: key, name: value.name, value: value.value, type: 'number' }

              return renderStyledInput(obj, 'searchable-array', leyendAttr.id, position)
            })}
            {renderStyledInput({ ...leyendAttr, name: '', type: 'number' }, 'searchable-array', leyendAttr.id, state.data[leyendAttr.id] ? Object.entries(state.data[leyendAttr.id]).length : 0)}
          </View>
        }

      </>
    )
  }

  return (
    <SafeAreaView style={[]}>
      <ScrollView style={{ height: Dimensions.get('window').height - 100 }}>
        {state.leyend.map((leyendAttr: LeyendAttr, index) => {
          if (state.isNameIntroduced == false) {
            if (index == 0) return (
              <View>
                <Text>introduzca el nombre de {typeData}</Text>
                <StyledTextInput
                  leyendAttr={leyendAttr}
                  onChangeText={(text) => onChangeStyledInput(state, setState, undefined, text, leyendAttr)}
                />
                <Text style={{ color: 'red' }}>{state.error}</Text>
              </View>
            )
            else return <View />
          }
          else return (
            <View style={{}}>
              {renderAttr(leyendAttr)}
            </View>
          )
        })}
      </ScrollView>
      {!state.isNameIntroduced && <Button title={`introducir`} onPress={() => onPressIntroduceNameData(state, setState, typeData)} />}
      {state.isNameIntroduced && local && <Button title={`Añadir Localmente`}
        onPress={async () => {
          await onPressLocalAddData(context, state, typeData, totalBitecoins);
          props.navigation.navigate(nextScreen)
        }

        } />}
      {state.isNameIntroduced && <Button title={`Añadir (${totalBitecoins} bitecoins)`} onPress={() => onPressAddData(props.navigation, state, typeData, totalBitecoins)} />}
    </SafeAreaView>
  );
}



