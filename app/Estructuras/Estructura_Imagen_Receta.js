import PorDefecto from '../assets/Screen_AcessStack_00.png';
import Image1 from '../assets/temporal_image_1.png';
import Image2 from '../assets/temporal_image_2.png';
import Image3 from '../assets/temporal_image_3.png';
import Image4 from '../assets/temporal_image_4.png';
import Image5 from '../assets/temporal_image_5.png';
import Image6 from '../assets/temporal_image_6.png';
import Image7 from '../assets/temporal_image_7.png';
import Image8 from '../assets/temporal_image_8.png';
import Image9 from '../assets/temporal_image_9.png';
import Image10 from '../assets/temporal_image_10.png';
import Image11 from '../assets/temporal_image_11.png';
import Image12 from '../assets/temporal_image_12.png';
import Image13 from '../assets/temporal_image_13.png';
import Image14 from '../assets/temporal_image_14.png';
import Image15 from '../assets/temporal_image_15.png';
import Image16 from '../assets/temporal_image_16.png';
import Image17 from '../assets/temporal_image_17.png';
import Image18 from '../assets/temporal_image_18.png';
import Image19 from '../assets/temporal_image_19.png';

import Tomate from '../assets/temporal_ingrediente_1.png';
import Huevo from '../assets/temporal_ingrediente_2.png';
import Cebolla from '../assets/temporal_ingrediente_3.png';


// import 
export const Lista_Imagen_Receta=()=>{
    return(
        {
            "defecto":PorDefecto,
            "Image1":Image1,
            "Image2":Image2,
            "Image3":Image3,
            "Image4":Image4,
            "Image5":Image5,
            "Image6":Image6,
            "Image7":Image7,
            "Image8":Image8,
            "Image9":Image9,
            "Image10":Image10,
            "Image11":Image11,
            "Image12":Image12,
            "Image13":Image13,
            "Image14":Image14,
            "Image15":Image15,
            "Image16":Image16,
            "Image17":Image17,
            "Image18":Image18,
            "Image19":Image19,
        }
    );
}

export const Lista_Imagen_Ingrediente=()=>{
    return(
        {
            "defecto":PorDefecto,
            "Tomate":Tomate,
            "Huevo":Huevo,
            "Cebolla":Cebolla,
        }
    );
}




