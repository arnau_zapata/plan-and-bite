
export type ValorNutricional = {
    kcal: number,
    proteina: number,
    hidratos: number,
    grasa: number,
    fibra: number,
    azucar: number,
}

export type Plato = {
    year: number,
    mes: number,
    dia: number,
    comida: number,
    plato: number,
    receta: number,
    quantity: number,
}

export type Menu = {
    comida: string,
    lista_platos: Plato[]
}

export type MenuDia = {
    dia: number,
    mes: number,
    year: number,
    valores_nutricionales: ValorNutricional[],
    cost_recipes: number
}



export type Nevera = {
    lista_ingredientes_en_nevera: Ingrediente[],
    lista_ingredientes_lista_compra: Ingrediente[],
    lista_ingredientes_auxiliar: Ingrediente[]
}

export type Ingrediente = {
    id_ingrediente: number,
    nombre?: string,
    cantidad: number,
    tipo_cantidad?: string,
    image?: string,
    conversion_ud_g: number,
    tipo_ingrediente?: any[] //habra que tocarlo
    valores_nutricionales: ValorNutricional
}

export type Place = {
    neighbourhood: string,
    district: string,
    city: string,
    province: string,
    region: string,
    country: string,
}

export type Coordinate = {
    latitude: number;
    longitude: number;
}

export type CoordinatesPlace = {
    neighbourhood: Coordinate,
    district: Coordinate,
    city: Coordinate,
    province: Coordinate,
    region: Coordinate,
    country: Coordinate,
}

export type Language = 'es' | 'en';

export type Planning = {
    ingredientes_a_comprar: Ingrediente[],
    lista_menus_del_dia: MenuDia[][],
    nevera_auxiliar: Nevera[]
}

export type Receta = {
    id: number,
    id_receta: number,
    nombre: string,
    lista_ingredientes: Ingrediente[],
    preparacion: string,
    valores_nutricionales: ValorNutricional[],
    solo_para_comida: string,
    solo_para_plato: number,
    imagen: string,
    imagen_url?: string,
    tiempo_preparacion: number,
    cost: number,
}


export type Global = {
    lista_ingredientes: Ingredientes[],
    ingrediente_seleccionado: string,
    receta_seleccionada: string,
    lista_recetas: Receta[],
    texto_introducido: string,
    ano_actual: number,
    mes_actual: number,
    dia_actual: number,
    dia_semana_actual: number,
    screen_Anadir_Receta: string,
    last_screen: string,
    ano_seleccionado: number,
    mes_seleccionado: number,
    dia_seleccionado: number,
    persona_seleccionada: string,
    comida_seleccionada: string,
    plato_seleccionado: number,
    pagina_actual: string,
    props: string,
    reset: boolean
}
export type Persona = {
    nombre: string,
    edad: number,
    gender: string,
    email?: string,
    altura: number,
    time: string,
    peso: number,
    objetivo: string,
    valores_nutricionales: ValorNutricional[],
    seguimiento: (number | string)[][],
    alergias: number[],
    selected?: boolean,
    noCome: string[],
    nivel_ejercicio: string,
    imported: boolean
}
export type Usuario = {
    nombre: string,
    contrasena: string,
    lista_personas: Persona[]
}

export type Context = {
    reset: boolean,
    set_reset: (context: any, x: boolean) => void,
    email: string,
    set_email: (context: any, x: string) => void,
    lista_ingredientes_nevera: Ingrediente[],
    set_lista_ingredientes_nevera: (context: any, x: Ingrediente[], y: Ingrediente[], z: Ingrediente[]) => void,
    lista_ingredientes_planificables: Ingrediente[],
    set_lista_ingredientes_planificablesyCompra: (context: any, x: Ingrediente[], y: Ingrediente[]) => void,
    lista_ingredientes_lista_compra: Ingrediente[],
    set_lista_ingredientes_lista_compra: (context: any, x: Ingrediente[]) => void,
    lista_planning: Plato[],
    set_lista_planning: (context: any, x: Plato[], y: MenuDia[], z: Ingrediente[], a: Ingrediente[]) => void,
    lista_planning_features: MenuDia[],
    set_lista_planning_features: (context: any, x: MenuDia[]) => void,
    lista_personas: Persona[],
    set_lista_personas: (context: any, x: Persona[]) => void,
    lista_recetas: Receta[],
    set_lista_recetas: (context: any, x: Receta[], reset:boolean) => void,
    lista_ingredientes: Ingrediente[],
    set_lista_ingredientes: (context: any, x: Ingrediente[]) => void,
    updateFromFirebase: any

}