import React, { createContext, useState } from 'react';
import { getListaRecetas, getListIngredients, getPersonas, getPlanning, getPlanningFeatures } from '../storage/firebase';

import {
    getEmailUserFirebase,
    get_Ingredientes_Nevera,
    get_lista_compra,
    get_Ingredientes_Planificables,
    getPlanningFirebase,
    getPlanningFeaturesFirebase,
    getListPersonas,
    get_Lista_Ingredientes_Firebase,
    setObjectAsyncStorage,
    getListIngredientsFirebase,
    getListaRecetasFirestore
} from '../storage/storage';



export default (props: any) => {



    const setLista_ingredientes_nevera = (context: any, lista_ingredientes_nevera: any, lista_ingredientes_planificables: any, lista_ingredientes_lista_compra: any) => {
        setState({ ...context, lista_ingredientes_nevera, lista_ingredientes_planificables, lista_ingredientes_lista_compra })
    }

    const setLista_ingredientes_planificablesyCompra = (context: any, lista_ingredientes_planificables: any, ingredientes_lista_compra: any) => {
        setState({ ...context, lista_ingredientes_planificables, ingredientes_lista_compra })
    }

    const setLista_ingredientes_lista_compra = (context: any, obj: any) => {
        setState({ ...context, lista_ingredientes_lista_compra: obj })
    }

    const setListaPlanning = (context: any, lista_planning: any, features: any, ingredientes_planificables: any, ingredientes_lista_compra: any) => {
        setState({ ...context, lista_planning, features, ingredientes_planificables, ingredientes_lista_compra, reset: true })
        console.log("context updated:", "planning")
    }


    const setEmail = (context: any, obj: any) => {
        setState({ ...context, email: obj })
    }

    const setListaPlanning_features = (context: any, obj: any,) => {
        setState({ ...context, lista_planning_features: obj })
    }

    const setListaPersonas = (context: any, obj: any,) => {
        setState({ ...context, lista_personas: obj })
    }
    const setListaIngredientes = (context: any, obj: any) => {
        setState({ ...context, lista_ingredientes: obj })
    }
    const setListaRecetas = (context: any, obj: any, reset?: boolean) => {
        setState({ ...context, lista_recetas: obj, reset })
    }

    const setReset = (context: any, reset: boolean) => {
        setState({ ...context, reset })
    }

    const updateFromFirebase = async (useCache: boolean, setComponentLoaded?: (x: string) => void) => {
        console.log("useCache", useCache)
        let email: any = undefined;
        let lista_ingredientes_nevera: any = undefined;
        let lista_ingredientes_planificables: any = undefined;
        let lista_ingredientes_lista_compra: any = undefined;
        let lista_personas: any = undefined;
        let lista_planning: any = undefined;
        let lista_planning_features: any = undefined;
        let lista_ingredientes: any = undefined;
        let lista_recetas: any = undefined;
        if (useCache) {

            setComponentLoaded && setComponentLoaded("cargando usuario")
            email = (await getEmailUserFirebase())

            setComponentLoaded && setComponentLoaded("cargando ingredientes");
            lista_ingredientes = await getListIngredientsFirebase();

            setComponentLoaded && setComponentLoaded("cargando recetas");
            lista_recetas = await getListaRecetasFirestore();
            setComponentLoaded && setComponentLoaded("cargando nevera")
            lista_ingredientes_nevera = (await get_Ingredientes_Nevera())
            lista_ingredientes_planificables = (await get_Ingredientes_Planificables())
            setComponentLoaded && setComponentLoaded("cargando lista de la compra")
            lista_ingredientes_lista_compra = (await get_lista_compra())
            setComponentLoaded && setComponentLoaded("cargando otros datos")
            lista_personas = (await getListPersonas())
            setComponentLoaded && setComponentLoaded("cargando plannificacion")
            lista_planning = (await getPlanningFirebase())
            lista_planning_features = (await getPlanningFeaturesFirebase())


            setState({
                ...state,
                email,
                lista_ingredientes_nevera,
                lista_ingredientes_planificables,
                lista_ingredientes_lista_compra,
                lista_planning,
                lista_planning_features,
                lista_personas,
                lista_ingredientes,
                lista_recetas
            })
        } else {

            setComponentLoaded && setComponentLoaded("cargando usuario")

            email = (await getEmailUserFirebase())
            setComponentLoaded && setComponentLoaded("cargando ingredientes");
            lista_ingredientes = await getListIngredients();
            setComponentLoaded && setComponentLoaded("cargando recetas");
            lista_recetas = await getListaRecetas(true);
            setComponentLoaded && setComponentLoaded("cargando nevera")

            lista_ingredientes_nevera = (await get_Lista_Ingredientes_Firebase("ingredientes_nevera"))
            lista_ingredientes_planificables = (await get_Lista_Ingredientes_Firebase("ingredientes_planificables"))

            setComponentLoaded && setComponentLoaded("cargando lista de la compra")
            lista_ingredientes_lista_compra = (await get_Lista_Ingredientes_Firebase("ingredientes_lista_compra"))

            lista_personas = await getPersonas();
            console.log("lista_personas", lista_personas)
            setComponentLoaded && setComponentLoaded("cargando otros datos")

            lista_planning = (await getPlanning())
            lista_planning_features = (await getPlanningFeatures())
            setComponentLoaded && setComponentLoaded("cargando plannificacion")

            await setObjectAsyncStorage("email", email)
            await setObjectAsyncStorage("ingredientes_nevera", lista_ingredientes_nevera)
            await setObjectAsyncStorage("ingredientes_planificables", lista_ingredientes_planificables)
            await setObjectAsyncStorage("ingredientes_lista_compra", lista_ingredientes_lista_compra)
            await setObjectAsyncStorage("planning", lista_planning)
            await setObjectAsyncStorage("planning_features", lista_planning_features)
            await setObjectAsyncStorage("personas", lista_personas)

            await setObjectAsyncStorage("lista-ingredientes", lista_ingredientes)
            await setObjectAsyncStorage("lista_recetas", lista_recetas)

            setState({
                ...state,
                email,
                lista_ingredientes_nevera,
                lista_ingredientes_planificables,
                lista_ingredientes_lista_compra,
                lista_planning,
                lista_planning_features,
                lista_personas,
                lista_ingredientes,
                lista_recetas
            })
        }
        console.log(`context updated: from firebase (useCache=${useCache})`)
        console.log({
            ...state,
            email,
            lista_ingredientes_nevera,
            lista_ingredientes_planificables,
            lista_ingredientes_lista_compra,
            lista_planning,
            lista_planning_features,
            lista_personas,
            lista_ingredientes,
            lista_recetas
        })
    }

    const [state, setState] = useState({
        reset: false,
        set_reset: setReset,
        email: "",
        set_email: setEmail,
        lista_ingredientes_nevera: [],
        set_lista_ingredientes_nevera: setLista_ingredientes_nevera,
        lista_ingredientes_planificables: [],
        set_lista_ingredientes_planificablesyCompra: setLista_ingredientes_planificablesyCompra,
        lista_ingredientes_lista_compra: [],
        set_lista_ingredientes_lista_compra: setLista_ingredientes_lista_compra,
        lista_planning: [],
        set_lista_planning: setListaPlanning,
        lista_planning_features: [],
        set_lista_planning_features: setListaPlanning_features,
        lista_personas: [],
        set_lista_personas: setListaPersonas,
        lista_ingredientes: [],
        set_lista_ingredientes: setListaIngredientes,
        lista_recetas: [],
        set_lista_recetas: setListaRecetas,
        updateFromFirebase,
    });

    return (
        <AppContext.Provider value={state}>
            {props.children}
        </AppContext.Provider>
    );
}

export const AppContext = createContext();